<footer>
	<div class="menu-footer">
		<div class="container">
			<div class="row">
				<ul class="list-inline">
					<li>
						<a href="#" title="">Quy chế Rông Bay</a><sup>New</sup>
					</li>
					<li>
						<a href="#" title="">Quy định chung</a>
					</li>
					<li>
						<a href="#" title="">Chính sách bảo mật</a>
					</li>
					<li>
						<a href="#" title="">Liên hệ quảng cáo</a>
					</li>
					<li>
						<a href="#" title="">Hỏi & Đáp</a>
					</li>
					<li>
						<a href="#" title="">Bảng Mobile</a>
					</li>
					<li>
						<a href="#" title="">Rss</a>
					</li>
					<li>
						<a href="#" title="">Site Map</a>
					</li>
					<li>
						<a href="#" title="">Google+</a>
					</li>
					<li class="fast-search">
						<a href="#" title="">Tìm kiếm nhanh</a>
					</li>
				</ul>
			</div>
			
		</div>
	</div>
	<div class="footer-content">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<p><strong>Rongbay.com</strong></p>
					<p>Tầng 17 Tòa nhà Center Building - Hapulico Complex, số 1 Nguyễn Huy Tưởng, Quận Thanh Xuân, TP Hà Nội.</p>
					<p><i class="fa fa-envelope" aria-hidden="true"></i>&ensp;hien@gmail.com</p>
					<p><i class="fa fa-volume-control-phone" aria-hidden="true"></i>&ensp;CSKH: 024 73095555 (Ext: 497 / 495 / 496). Fax: 024 39743413</p>
					<p>Kinh doanh: 0932 223 656</p>
					<p><img src="./libraries/images/interface/zalo.png" style="with:15px; height: 15px;" alt="">&ensp;0972-616-834</p>
					<p><i class="fa fa-facebook-official" aria-hidden="true"></i>&ensp;Hiền Nguyễn</p>
					<div class="logo-footer">
						<div class="row">
							<div class="logo-footer1">
								<img src="./libraries/images/interface/logo-footer.png" alt="">
								<div class="text">
									<a href="#" class="a1" title="">Rồng Bay</a><br>
									<a href="#" class="a2 xzoom" title=""><i class="fa fa-facebook-official" aria-hidden="true"></i>&ensp;Thích trang</a><span>72K lượt thích</span>
									
								</div>
							</div>
							
						</div>	
					</div></div>
					<div class="col-md-6 col-lg-6">
						<div class="row image-logo">
							<div class="col-lg-6">
								<p>ĐƯỢC VẬN HÀNH BỞI</p>
								<div class="image-right">
									<img src="./libraries/images/interface/logo-footer2.png" class="img-responsive" alt="">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="image-left">
									<img src="./libraries/images/interface/logo-footer3.png" alt="">
								</div>
							</div>
							
						</div>
						<p>Copyright © 2006 - RongBay.com Công ty Cổ phần VCCorp</p>
						<p>Giấy đăng ký kinh doanh số: 0101871229 do Sở Kế hoạch và Đầu tư cấp ngày 27/8/2015</p>
						<p>Email: info@vccorp.vn</p>
						<img src="./libraries/images/interface/logo-footer4.png" alt="">
					</div>
				</div>
			</div>
		</div>
	</footer>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" ></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" ></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" ></script>	
	<script src="./libraries/js/jquery.validate.min.js" ></script>	
	<script src="./libraries/js/style.js"></script>
	<script src="./libraries/js/xzoom.min.js"></script>
	<script src="./libraries/js/validation.js"></script>
	<script>
		(function ($) {
			$(document).ready(function() {
				$('.xzoom, .xzoom-gallery').xzoom({position: 'lens', lensShape: 'circle',zoomWidth:200,zoomHeight:200, sourceClass: 'xzoom-hidden'});
			});
		})(jQuery);
	</script>
</body>
</html>