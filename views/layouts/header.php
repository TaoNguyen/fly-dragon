<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Rong Bay</title>
	<link rel="stylesheet" href="">
	<link rel="stylesheet" href="./libraries/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="./libraries/css/style.css">
	<link rel="stylesheet" type="text/css" href="./libraries/css/xzoom.css">
</head>
<body>
	<header>
		<div class="header-top">
			<div class="container">
				<div class="row">
					<div class="col-md-8 offset-md-4 col-lg-6 offset-lg-6">
						<ul class="list-inline menu_header">
							<li class="news-top">
								<a href="#">Đăng tin ưu tiên</a>&ensp;<i class="fa fa-sort-desc" aria-hidden="true"></i>
								<ul class="menu-top-child">
									<li>
										<div class="row top-child">
											<div class="col-6 col-sm-6 col-md-4 col-lg-4 top-child1">
												<p>Các dịch vụ ưu tiên trên Rông Bay</p>
												<a href="#">Mua lượt trên trang 1</a><br>
												<a href="#">Tin trọn đời</a><br>
												<a href="#">Tin VIP</a><br>
												<a href="#">Bảng giá các dịc vụ</a><br>
											</div>
											<div class="col-6 col-sm-6 col-md-4 col-lg-4 top-child2">
												<p>Các dịch vụ ưu tiên trên Rông Bay</p>
												<a href="#">Mua lượt trên trang 1</a><br>
												<a href="#">Tin trọn đời</a><br>
												<a href="#">Tin VIP</a><br>
												<a href="#">Bảng giá các dịc vụ</a><br>
											</div>
											<div class="col-md-4 col-lg-4 top-child3">
												<p>Các dịch vụ ưu tiên trên Rông Bay</p>
												<a href="#">Mua lượt trên trang 1</a><br>
												<a href="#">Tin trọn đời</a><br>
												<a href="#">Tin VIP</a><br>
												<a href="#">Bảng giá các dịc vụ</a><br>
											</div>
										</div>
										<div class="row top-child-left">
											<div class="bonus">
												<a href="#"><img src="./libraries/images/interface/icon_bonus.png" alt="">&ensp;Rồng bay mới ra chức năng mới: update qua ứng dụng Facebook Message</a>
											</div>
											
										</div>
									</li>

								</ul>
							</li>
							<li class="asks">
								<a href="#">Hỏi & Đáp</a>
							</li>
							<li class="help">
								<a href="#">Hỗ trợ</a>&ensp;<i class="fa fa-sort-desc" aria-hidden="true"></i>
								<ul class="menu-top-child-1">
									<li>
										<div class="row top-child-1">
											<p>Nếu bạn cần hỗ trợ vui lòng liên hệ</p>
										</div>
										<div class="row menu-top-contact">
											<div class="col-5 col-sm-5 col-md-5 col-lg-5">
												<div class="row">
													<p><img src="./libraries/images/interface/phone.png" alt="">&ensp;Điện thoại</p>
												</div>
												<div class="row">
													<p><img src="./libraries/images/interface/cellphone.png" alt="">&ensp;&ensp;Hotline</p>
												</div>
												<div class="row">
													<p><img src="./libraries/images/interface/email.png" alt="">&ensp;Email</p>
												</div>
												<div class="row">
													<p><img src="./libraries/images/interface/sky.png" alt="">&ensp;Sky</p>
												</div>
											</div>
											<div class="col-7 col-sm-7 col-md-7 col-lg-7">
												<div class="row">
													<p>:<strong>0972616834</strong>&ensp;<small>(0510333786)</small></p>
												</div>
												<div class="row">
													<p>:<strong>0972616834</strong></p>
												</div>
												<div class="row email">
													<p>:hien@gmail.com</p>
													
												</div>
												<div class="row sky">
													<p>:nguyenthihien</p>
												</div>
											</div>
										</div>
										
										<div class="row link-contact">
											<ul class="list-inline child-top">
												<li>
													<a href="#">Thông báo về việc chuyển văn phòng</a>
												</li>
												<li>
													<a href="#">Thông báo về việc chuyển văn phòng</a>
												</li>
												<li>
													<a href="#">Thông báo về việc chuyển văn phòng</a>
												</li>
											</ul>
										</div>
									</li>
								</ul>
							</li>
							<li class="adv">
								<a href="#">Mua quảng cáo</a>
							</li>
							<?php 
							if (!isset($_SESSION['u_info'])) {
								?>
								<li><a href="" class="modal-trigger lighten-2 registersite" data-toggle="modal" data-target="#modal-rg">Đăng ký</a></li>
								<li><a href="" class="yel low-text lighten-2 modal-trigger waves-effect waves-light loginsite" data-toggle="modal" data-target="#modal-rg">Đăng nhập</a></li>
								<?php
							} elseif (isset($_SESSION['u_info'])) {
								$show=$_SESSION['u_info'];
								?> 
								<li class="show_email"><a  href="/rongbayproject/customers/index.php?controller=customer&action=inforcus" ><?php echo $show['email']; ?></a></li>
								<li><a href="/rongbayproject/customers/index.php?controller=customer&action=logout" >Logout</a></li>
								<?php
							} 
							?>
							<li class="start">
								<i class="fa fa-star" aria-hidden="true"></i>
							</li>
						</ul>	
					</div>	
				</div>
			</div>
		</div>
		<div class="modal fade" id="modal-rg" tabindex="-1" role="dialog" aria-labelledby="modal-rg" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					
					<div class="modal-body">
						<div class="button-login-rg ">
							<ul class="nav nav-tabs">
								<li><a data-toggle="tab" class="active" href="#test-swipe-1">Đăng Nhập</a></li>
								<li><a data-toggle="tab" href="#test-swipe-2">Đăng Ký</a></li>
							</ul>
						</div>
						<div class="tab-content">
							<div id="test-swipe-2"  class="form-rigen tab-pane fade">
								<form action="/rongbayproject/customers/index.php?controller=customer&action=register" method="post" id="register-form">
									<div class="form-group input-form">
										<input id="name" type="text" name="full_name" class="form-control">
										<label for="name">Họ và tên</label>
									</div>
									<div class="form-group input-form-radio">
										<label for="gender">Giới tính</label>
										<br>
										<input name="gender" type="radio" id="test1" value="1" checked="" />
										<label for="test1">Nam</label>
										<input name="gender" type="radio" id="test2" value="2"/>
										<label for="test2">Nữ</label>
									</div>
									<div class="form-group input-form">
										<label for="email">Email</label>
										<input id="email" type="text" name="email" class="form-control">
										
									</div>
									<div class="form-group input-form">
										<label for="phone">Điện Thoại</label>
										<input id="Phone" type="text" name="phone" class="form-control">
										
									</div>
									<div class="form-group input-form">
										<label for="location">Địa chỉ</label>
										<input id="location" type="text" name="address" class="form-control">
										
									</div>
									<div class="form-group input-form">
										<label for="password">Mật khẩu</label>
										<input id="password" type="password" name="password" class="form-control">
										
									</div>
									<div class="form-group input-form">
										<label for="password-se">Xác Mật khẩu</label>
										<input id="password-se" type="password" name="repassword" class="form-control">
										
									</div>
									<div class="submit-login-rg">
										<button class="btn btn-success" type="submit" name="register_btn">Tạo Tài Khoản</button>
									</div>
									<div class="loginfb">
										<a href="https://www.facebook.com/v2.2/dialog/oauth?client_id=184755148827595&amp;state=262d9015425ea828719d9ad39f1c5d52&amp;response_type=code&amp;sdk=php-sdk-5.6.2&amp;redirect_uri=https%3A%2F%2Fdanang.land%2Fmember%2Fregister.html&amp;scope=email"><img style="width:218px;" src="assets/img/site/signup2069.png?1523588361" alt="" /></a>                    
									</div>
								</form>
							</div>
							<div id="test-swipe-1"  class="form-rigen tab-pane fade in show">
								<form action="/rongbayproject/customers/index.php?controller=customer&action=login" method="post" id="login-form">
									<div class="form-group input-form">
										<label for="email">Email đăng nhập</label>
										<input type="text" name="email" class="form-control">
										
									</div>
									<div class="form-group input-form">
										<label for="password">Mật khẩu</label>
										<input id="pasword" type="password" name="password" class="form-control">
										

									</div>
									<div class="submit-login-rg">
										<button class="btn btn-success" type="submit" name="submit">Đăng Nhập</button>
									</div>
									<div class="loginfb">
										<a href="https://www.facebook.com/v2.2/dialog/oauth?client_id=184755148827595&amp;state=262d9015425ea828719d9ad39f1c5d52&amp;response_type=code&amp;sdk=php-sdk-5.6.2&amp;redirect_uri=https%3A%2F%2Fdanang.land%2Fmember%2Flogin.html&amp;scope=email"><img style="width:218px;" src="assets/img/site/signindfc6.png?1523588370" alt="" /></a>                    
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="header-content">
			<div class="container">
				<div class="row">
					<?php include "controllers/templateController.php"; ?>
				</div>
			</div>
		</div>				
	</header><!-- /header -->