<div class="menu-left col-lg-3">
	<nav class="navbar navbar-expand-lg navbar-light">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-top" aria-controls="nav-top" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			<p>Lựa chọn chuyên mục</p>
		</button>
		<div class="collapse navbar-collapse" id="nav-top">
			<ul class="navbar-nav flex-column" >
				<li class="nav-item menu">
					<p>Lựa chọn chuyên mục</p>
				</li>
				<li class="nav-item menu1">
					<img src="./libraries/images/interface/icon_menu_1.png" alt="">&ensp;
					<a href="index.php?controller=posts&action=listpost&parent=1" title="">Mua bán nhà đất</a>
					<i class="fa fa-angle-right" aria-hidden="true"></i>
					<ul class="menu-left-child">
						<li>
							<div class="row child">
								<div class="col-md-5 col-lg-5 border-child">
									<div class="title-child">
										<a href="#" title="">MUA BÁN NHÀ ĐẤT</a>&ensp;
										<i class="fa fa-angle-right" aria-hidden="true"></i>
									</div>
									<ul class="child-child">
										<?php
										foreach ($c1 as $p1) {
											?>
											<li>
												<a href="index.php?controller=posts&action=listpost&id=<?= $p1['id']?>" title=""><?= $p1['category_name']?></a>
											</li>
											<?php
										}
										?>
									</ul>	
								</div>
								<div class="col-md-7 col-lg-7">
									<div class="child2 text-center">
										<a href="https://rongbay.com/Da-Nang"><img src="./libraries/images/interface/image-child.png" class="img-responsive" alt=""></a>
										<br><br>
										<span><strong>*</strong> Tìm kiếm dễ dàng với hơn 1010 dự án trên toàn quốc</span>
									</div>

								</div>
							</div>
						</li>
					</ul>
				</li>
				<li class="nav-item menu2">
					<img src="./libraries/images/interface/icon_menu_2.png" alt="">&ensp;
					<a href="index.php?controller=posts&action=listpost&parent=2" title="">Cho thuê nhà, văn phòng</a>
					<i class="fa fa-angle-right" aria-hidden="true"></i>
					<ul class="menu-left-child1">
						<li>
							<div class="row child">
								<div class="col-sm-6 col-md-4 col-lg-4 border-child1 ">
									<div class="title-child">
										<a href="#" title="">CHO THUÊ NHÀ, VĂN PHÒNG</a>&ensp;
										<i class="fa fa-angle-right" aria-hidden="true"></i>
									</div>
									<ul class="child-child1">
										<?php
										foreach ($c2 as $p2) {
											?>
											<li>
												<a href="index.php?controller=posts&action=listpost&id=<?= $p2['id']?>" title=""><?= $p2['category_name']?></a>
											</li>
											<?php
										}
										?>
									</ul>	
								</div>
								<div class="col-sm-6 col-md-4 col-lg-4">
									<div class="title-child">
										<a href="#" title="">CHO THUÊ NHÀ, VĂN PHÒNG</a>&ensp;
										<i class="fa fa-angle-right" aria-hidden="true"></i>
									</div>
									<ul class="child-child1">
										<?php
										foreach ($c2 as $p2) {
											?>
											<li>
												<a href="index.php?controller=posts&action=listpost&id=<?= $p2['id']?>" title=""><?= $p2['category_name']?></a>
											</li>
											<?php
										}
										?>

									</ul>
								</div>
								<div class="col-md-4 col-lg-4 last-child2">
									<div class="title-child">
										<a href="#" title="">CHO THUÊ NHÀ, VĂN PHÒNG</a>&ensp;
										<i class="fa fa-angle-right" aria-hidden="true"></i>
									</div>
									<ul class="child-child1"
									style="list-style-type: circle;">
									<?php
									foreach ($c2 as $p2) {
										?>
										<li>
											<a href="index.php?controller=posts&action=listpost&id=<?= $p2['id']?>" title=""><?= $p2['category_name']?></a>
										</li>
										<?php
									}
									?>
								</ul>
							</div>
						</div>
					</li>
				</ul>
			</li>
			<li class="nav-item menu3" >
				<img src="./libraries/images/interface/icon_menu_3.png" alt="">&ensp;
				<a href="index.php?controller=posts&action=listpost&parent=3" title="">Ô tô, xe máy</a>
				<i class="fa fa-angle-right" aria-hidden="true"></i>
				<ul class="menu-left-child">
					<li>
						<div class="row child">
							<div class="col-md-5 col-lg-5 border-child">
								<div class="title-child">
									<a href="#" title="">Ô TÔ, XE MÁY</a>&ensp;
									<i class="fa fa-angle-right" aria-hidden="true"></i>
								</div>
								<ul class="child-child">
									<?php
									foreach ($c3 as $p3) {
										?>
										<li>
											<a href="index.php?controller=posts&action=listpost&id=<?= $p3['id']?>" title=""><?= $p3['category_name']?></a>
										</li>
										<?php
									}
									?>
								</ul>	
							</div>
							<div class="col-md-7 col-lg-7">
								<div class="child2 text-center">
									<a href="https://rongbay.com/Da-Nang"><img src="./libraries/images/interface/image-child.png" class="img-responsive" alt=""></a>
									<br><br>
									<span><strong>*</strong> Tìm kiếm dễ dàng với hơn 1010 dự án trên toàn quốc</span>
								</div>

							</div>
						</div>
					</li>
				</ul>
			</li>
			<li class="nav-item menu4" >
				<img src="./libraries/images/interface/icon_menu_6.png" alt="">&ensp;
				<a href="index.php?controller=posts&action=listpost&parent=4" title="">Thời trang, Mẹ và Bé</a>
				<i class="fa fa-angle-right" aria-hidden="true"></i>
				<ul class="menu-left-child">
					<li>
						<div class="row child">
							<div class="col-md-5 col-lg-5 border-child">
								<div class="title-child">
									<a href="#" title="">THỜI TRANG MẸ & BÉ</a>&ensp;
									<i class="fa fa-angle-right" aria-hidden="true"></i>
								</div>
								<ul class="child-child">
									<?php
									foreach ($c4 as $p4) {
										?>
										<li>
											<a href="index.php?controller=posts&action=listpost&id=<?= $p4['id']?>" title=""><?= $p4['category_name']?></a>
										</li>
										<?php
									}
									?>
								</ul>	
							</div>
							<div class="col-md-7 col-lg-7">
								<div class="child2 text-center">
									<a href="https://rongbay.com/Da-Nang"><img src="./libraries/images/interface/image-child.png" class="img-responsive" alt=""></a>
									<br><br>
									<span><strong>*</strong> Tìm kiếm dễ dàng với hơn 1010 dự án trên toàn quốc</span>
								</div>

							</div>
						</div>
					</li>
				</ul>
			</li>
			<li class="nav-item menu5" >
				<img src="./libraries/images/interface/icon_menu_20.png" alt="">&ensp;
				<a href="index.php?controller=posts&action=listpost&parent=5" title="">Nhà và Vườn</a>
				<i class="fa fa-angle-right" aria-hidden="true"></i>
				<ul class="menu-left-child">
					<li>
						<div class="row child">
							<div class="col-md-5 col-lg-5 border-child">
								<div class="title-child">
									<a href="#" title="">NHÀ VÀ VƯỜN</a>&ensp;
									<i class="fa fa-angle-right" aria-hidden="true"></i>
								</div>
								<ul class="child-child">
									<?php
									foreach ($c5 as $p5) {
										?>
										<li>
											<a href="index.php?controller=posts&action=listpost&id=<?= $p5['id']?>" title=""><?= $p5['category_name']?></a>
										</li>
										<?php
									}
									?>
								</ul>	
							</div>
							<div class="col-md-7 col-lg-7">
								<div class="child2 text-center">
									<a href="https://rongbay.com/Da-Nang"><img src="./libraries/images/interface/image-child.png" class="img-responsive" alt=""></a>
									<br><br>
									<span><strong>*</strong> Tìm kiếm dễ dàng với hơn 1010 dự án trên toàn quốc</span>
								</div>

							</div>
						</div>
					</li>
				</ul>
			</li>
			<li class="nav-item menu6" >
				<img src="./libraries/images/interface/icon_menu_8.png" alt="">&ensp;
				<a href="index.php?controller=posts&action=listpost&parent=6" title="">Điện máy</a>
				<i class="fa fa-angle-right" aria-hidden="true"></i>
				<ul class="menu-left-child">
					<li>
						<div class="row child">
							<div class="col-md-5 col-lg-5 border-child">
								<div class="title-child">
									<a href="#" title="">ĐIỆN MÁY</a>&ensp;
									<i class="fa fa-angle-right" aria-hidden="true"></i>
								</div>
								<ul class="child-child">
									<?php
									foreach ($c6 as $p6) {
										?>
										<li>
											<a href="index.php?controller=posts&action=listpost&id=<?= $p6['id']?>" title=""><?= $p6['category_name']?></a>
										</li>
										<?php
									}
									?>
								</ul>	
							</div>
							<div class="col-md-7 col-lg-7">
								<div class="child2 text-center">
									<a href="https://rongbay.com/Da-Nang"><img src="./libraries/images/interface/image-child.png" class="img-responsive" alt=""></a>
									<br><br>
									<span><strong>*</strong> Tìm kiếm dễ dàng với hơn 1010 dự án trên toàn quốc</span>
								</div>

							</div>
						</div>
					</li>
				</ul>
			</li>
			<li class="nav-item menu7" >
				<img src="./libraries/images/interface/icon_menu_10.png" alt="">&ensp;
				<a href="index.php?controller=posts&action=listpost&parent=7" title="">Điện thoại, chợ sim</a>
				<i class="fa fa-angle-right" aria-hidden="true"></i>
				<ul class="menu-left-child">
					<li>
						<div class="row child">
							<div class="col-md-5 col-lg-5 border-child">
								<div class="title-child">
									<a href="#" title="">ĐIỆN THOẠI, CHỢ SIM</a>&ensp;
									<i class="fa fa-angle-right" aria-hidden="true"></i>
								</div>
								<ul class="child-child">
									<?php
									foreach ($c7 as $p7) {
										?>
										<li>
											<a href="index.php?controller=posts&action=listpost&id=<?= $p7['id']?>" title=""><?= $p7['category_name']?></a>
										</li>
										<?php
									}
									?>
								</ul>	
							</div>
							<div class="col-md-7 col-lg-7">
								<div class="child2 text-center">
									<a href="https://rongbay.com/Da-Nang"><img src="./libraries/images/interface/image-child.png" class="img-responsive" alt=""></a>
									<br><br>
									<span><strong>*</strong> Tìm kiếm dễ dàng với hơn 1010 dự án trên toàn quốc</span>
								</div>

							</div>
						</div>
					</li>
				</ul>
			</li>
			<li class="nav-item menu8" >
				<img src="./libraries/images/interface/icon_menu_4.png" alt="">&ensp;
				<a href="index.php?controller=posts&action=listpost&parent=8" title="">Việc làm</a>
				<i class="fa fa-angle-right" aria-hidden="true"></i>
				<ul class="menu-left-child">
					<li>
						<div class="row child">
							<div class="col-md-5 col-lg-5 border-child">
								<div class="title-child">
									<a href="#" title="">VIỆC LÀM</a>&ensp;
									<i class="fa fa-angle-right" aria-hidden="true"></i>
								</div>
								<ul class="child-child">
									<?php
									foreach ($c8 as $p8) {
										?>
										<li>
											<a href="index.php?controller=posts&action=listpost&id=<?= $p8['id']?>" title=""><?= $p8['category_name']?></a>
										</li>
										<?php
									}
									?>
								</ul>	
							</div>
							<div class="col-md-7 col-lg-7">
								<div class="child2 text-center">
									<a href="https://rongbay.com/Da-Nang"><img src="./libraries/images/interface/image-child.png" class="img-responsive" alt=""></a>
									<br><br>
									<span><strong>*</strong> Tìm kiếm dễ dàng với hơn 1010 dự án trên toàn quốc</span>
								</div>

							</div>
						</div>
					</li>
				</ul>
			</li>
			<li class="nav-item menu9" >
				<img src="./libraries/images/interface/icon_menu_5.png" alt="">&ensp;
				<a href="index.php?controller=posts&action=listpost&parent=9" title="">Du lịch</a>
				<i class="fa fa-angle-right" aria-hidden="true"></i>
				<ul class="menu-left-child">
					<li>
						<div class="row child">
							<div class="col-md-5 col-lg-5 border-child">
								<div class="title-child">
									<a href="#" title="">DU LỊCH</a>&ensp;
									<i class="fa fa-angle-right" aria-hidden="true"></i>
								</div>
								<ul class="child-child">
									<?php
									foreach ($c9 as $p9) {
										?>
										<li>
											<a href="index.php?controller=posts&action=listpost&id=<?= $p9['id']?>" title=""><?= $p9['category_name']?></a>
										</li>
										<?php
									}
									?>
								</ul>	
							</div>
							<div class="col-md-7 col-lg-7">
								<div class="child2 text-center">
									<a href="https://rongbay.com/Da-Nang"><img src="./libraries/images/interface/image-child.png" class="img-responsive" alt=""></a>
									<br><br>
									<span><strong>*</strong> Tìm kiếm dễ dàng với hơn 1010 dự án trên toàn quốc</span>
								</div>

							</div>
						</div>
					</li>
				</ul>
			</li>
			<li class="nav-item menu10">
				<img src="./libraries/images/interface/icon_menu_11.png" alt="">&ensp;
				<a href="index.php?controller=posts&action=listpost&parent=10" title="">Mua sắm, Du lịch, hợp tác</a>
				<i class="fa fa-angle-right" aria-hidden="true"></i>
				<ul class="menu-left-child">
					<li>
						<div class="row child">
							<div class="col-md-5 col-lg-5 border-child">
								<div class="title-child">
									<a href="#" title="">MUA SẮM, DU LỊCH, HỢP TÁC</a>&ensp;
									<i class="fa fa-angle-right" aria-hidden="true"></i>
								</div>
								<ul class="child-child">
									<?php
									foreach ($c10 as $p10) {
										?>
										<li>
											<a href="index.php?controller=posts&action=listpost&id=<?= $p10['id']?>" title=""><?= $p10['category_name']?></a>
										</li>
										<?php
									}
									?>
								</ul>	
							</div>
							<div class="col-md-7 col-lg-7">
								<div class="child2 text-center">
									<a href="https://rongbay.com/Da-Nang"><img src="./libraries/images/interface/image-child.png" class="img-responsive" alt=""></a>
									<br><br>
									<span><strong>*</strong> Tìm kiếm dễ dàng với hơn 1010 dự án trên toàn quốc</span>
								</div>

							</div>
						</div>
					</li>
				</ul>
			</li>

		</ul>
	</div>
</nav>		
</div>