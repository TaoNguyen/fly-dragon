<div class="col-md-5 col-lg-3">
	<div class="logo">
		<a href="index.php?controller=home"><img src="./libraries/images/interface/raovat_rongbay.png" class="logo_image" alt="logo"></a>	
	</div>		
</div>
<div class=" col-md-7 col-lg-6">
	<div class="row">
		<div class="col-3 col-sm-3 col-md-4 col-lg-3 main-dropdown">
			<div class="dropdown">
				<a href="#" class="city dropdown-toggle" data-toggle="dropdown" id="title"><?php if(isset($_SESSION['province_name'])){ echo $_SESSION['province_name'];}else {echo "Chọn tỉnh";}?></a>
				<div class="dropdown-menu">
					<div class="row">
						<?php 
						$run=1;
						if(isset($list_provinces)&&!empty($list_provinces)){
							while ($row_province=$list_provinces->fetch_array()) {
								if($run==1){
									echo '<div class="col-4 col-sm-4 col-md-4 col-lg-4">';
								}
								echo '<a class="dropdown-item province_header" href="#" id="'.$row_province['id'].'" name="'.$row_province['name'].'">'.$row_province['name'].'</a>';
								if($run%21==0){
									echo '</div>';
									if($run<$list_provinces->num_rows){
										echo '<div class="col-4 col-sm-4 col-md-4 col-lg-4">';
									}
								}
								$run++;
							}
						}
						?>
					</div>  
				</div>
			</div>
		</div>
		<div class="col-9 col-sm-9 col-md-8 col-lg-9 logo-left">
			<form action="index.php?controller=search" method = "GET">
				<div class="input-group">
					<div class="input-group-prepend">
						<button class="btn button dropdown-toggle" id="parent" name="parent_id" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" name="">Tất cả</button>
						<input type="hidden" name="controller" value="search">
						<input type="hidden" name="parent" value="">
						<div class="dropdown-menu drop-menu2">
							<?php
							foreach ($listparent as $list) {
								?>
								<a class="dropdown-item choose_category" href="#" parent_name= "<?= $list['category_name']?>" category_id="<?= $list['id']?>"><?= $list['category_name']?></a>

								<?php
							}
							?>	
						</div>
					</div>
					<input type="text" class="form-control search_word" name="keysearch" placeholder="Tìm kiếm" aria-label="hehe" aria-describedby="basic-addon2">
					<div class="input-group-append">
						<button class="btn search " type="submit">Tìm kiếm</button>
					</div>
				</div>
			</form>

		</div>
	</div>	
</div>
<div class="col-lg-2 offset-lg-1">
	<a href="index.php?controller=posts&action=post" title=""><button type="button" class="btn post">Đăng tin</button></a>		
</div>