<section>	
	<div class="container detail-post">	
		<div class="row title-detail">
			<div class="container">
				<p><?php echo $row['title']?></p>
			</div>	
		</div>
		<div class="row">
			<div class="col-5 col-sm-5 col-md-5 col-lg-6 description">
				<p><b>Tổng quan:</b></p>
				<p><b>Giá :</b> <?php echo $row['price']?> (<?php echo $row['unit']?>)</p>
				<p><b>Diện tích :</b> <?php echo $row['acreage']?> m2</p>
				<p><b>Hướng:</b> <?php echo $row['lay']?></p>
				<p><b>Thuận lợi:</b> <?php echo $row['balcony']?></p>
				<p><b>Giấy tờ pháp lý:</b> <?php echo $row['license']?></p>
				<p><b>Miêu tả:</b> <?php echo $row['description']?></p>
			</div>
			<div class="col-7 col-sm-7 col-md-7 col-lg-6 img-detail">
				<div class="row detail-contact">
					<div class = "box-contact">
						<p class="text-center">Liên hệ người bán</p>
						<i class="fa fa-address-card" aria-hidden="true"></i><a href="#" title=""><?php echo $row['fullname']?></a><br>
						<i class="fa fa-phone" aria-hidden="true"></i> <a href="#" title=""><?php echo $row['phone']?></a>
					</div>	
				</div>
				<div class="row text-center">
					<div id="gal1">
						<div class="list-imgzoom">
							<img class="xzoom" id="xzoom-default" src="<?php echo $row['image']?>" xoriginal="<?php echo $row['image']?>" />
							<div class="xzoom-thumbs list-thumb-view ">
								<?php
								while ($img = $images->fetch_assoc()) {
									?>
									<a href="<?php echo $img['image']?>">
										<img class="xzoom-gallery" width="70" src="<?php echo $img['image']?>"  xpreview="<?php echo $img['image']?>">
									</a>
									<?php
								}
								?>
							</div> 
							<div class="large-7 viewzoom"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row post1">
			<div class="col-5 col-sm-5 col-md-4 col-lg-3 text1">
				<span>THÔNG TIN BẠN QUAN TÂM</span>
			</div>
			<div class="col-7 col-sm-7 col-md-8 col-lg-9 text2">		
			</div>
		</div>
		<div class="container">
		<?php
		while ($category = $categories->fetch_assoc()){
			?>
			<div class="row">
				
					<div class="col-4 col-sm-5 col-md-3 col-lg-3 text-center">
						<div class="img_post">
							<img src="<?php echo $category['image']?>" alt="">
						</div>
					</div>
					<div class="col-8 col-sm-7 col-md-6 col-lg-6">
						<div class="title_post">
							<a href="index.php?controller=posts&action=detailpost&id=<?php echo $category['post_id']?>" title=""><?php echo $category['title']?></a>
						</div>
						<div class="info-post">
							<span><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;<b>Dự án,</b><?php echo $category['address']?></span><br>
							<span>Giá: <strong><?php echo $category['price']?> (<?php echo $category['unit']?>)</strong> - Diện tích: <?php echo $category['acreage']?> m2</span><br>
							<span>Hướng: <?php echo $category['lay']?>, <?php echo $category['balcony']?>, <?php echo $category['license']?></span><br>
							<span>Post date: <b><?php echo $category['created_at']?></b></span>
						</div>	
					</div>
					<div class="col-md-3 col-lg-3">
						<div class="avatar text-center">
							<img src="./libraries/images/interface/no_avatar.jpg" alt=""><br>
							<a href="#" title=""><?php echo $category['fullname']?></a>
						</div>
					</div>
				</div>
				
			
			<hr>
			<?php
		}	
		?>
		</div>
	</div>	
</section>