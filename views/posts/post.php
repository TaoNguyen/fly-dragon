<?php
	include_once ('libraries/tinyeditor.php');
?>
<section>		
	<div class="container posts">	
		<div class="row post1">
			<div class="col-4 col-sm-4 col-md-3 col-lg-2 text1">
				<span>THÔNG TIN CƠ BẢN</span>
			</div>
			<div class="col-8 col-sm-8 col-md-9 col-lg-10 text2">		
			</div>
		</div>
	 	<div class="container">
			<div class="row post2">
				<div class="col-lg-11 offset-lg-1">
					<form method="post" action="" enctype="multipart/form-data">
						<div class="post_title">
							<div class="form-group">
								<p>Vui lòng chọn chuyên mục</p>
								<select class="form-control category" name="category">
									<option value="">Vui lòng chọn chuyên mục</option>
									<?php
									foreach($categories as $category) {
										?>
										<option value="<?php echo $category ['id']?>" <?php if($category ['id']==$category_id) echo "selected='selected'";?>><?php echo  $category['category_name'];?></option>
										<?php
									}
									?>
								</select>
								<small id="title" class="form-text"><?php echo $category_error;?></small>
							</div>
						</div>
						<div class="post_form">
							<div class="form-group">
								<input type="text" class="form-control <?php echo (!empty($title_error)) ? 'has-error' : ''; ?>" name="title" placeholder="Tiêu đề" value="<?php echo $title; ?>">
								<small id="title" class="form-text"><?php echo $title_error;?></small>
							</div>
							<div class="row">
								<div class="col-md-8 col-lg-8">
									<div class="form-group">
										<p>Vui lòng chọn tỉnh/ thành phố</p>
										<select class="form-control provinces" name="province">
											<option value="">Vui lòng chọn tỉnh/ thành phố</option>
											<?php 
											foreach($results as $result) {
												?>
												<option value="<?php echo $result ['id']?>" <?php if($result['id']==$province_id) echo "selected='selected'";?>><?php echo  $result['name'];?></option>
												<?php
											}
											?>
										</select>
										<small id="title" class="form-text"><?php echo $provice_error;?></small>
									</div>
									<div class="form-group">
										<p>Quận/ Huyện</p>
										<div id="district_select">
											<select class="form-control" name="district">
												
												<?php
												if ($district!=""){
													foreach ($district as $list_district) {
														?>
														<option value="<?=$list_district['id']?>" <?php if($list_district['id']==$district_id) echo "selected='selected'";?>><?=$list_district['name']?></option>
														<?php
													}
												}else {
													?>
													<option value="">Vui lòng chọn Quận/huyện</option>
													<?php
												}
												?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<p>Địa chỉ</p>
										<input type="text" class="form-control <?php echo (!empty($address_error)) ? 'has-error' : ''; ?>" name="address" placeholder="Nhập địa chỉ" value="<?php echo $address; ?>">
										<label>* Nếu có số nhà cụ thể bạn hãy nhập đầy đủ thông tin</label>
										<small id="address" class="form-text"><?php echo $address_error;?></small>
									</div>
									<div class="form-group" id ="acreage" <?php if( $hide == 1){ echo "hidden";}?>>
										<p>Diện tích</p>
										<input type="text" id ="acreage" class="form-control <?php echo (!empty($acreage_error)) ? 'has-error' : ''; ?>" name="acreage" placeholder="Diện tích (m2)" value="<?php echo $acreage; ?>">
										<small id="acreage" class="form-text"><?php echo $acreage_error;?></small>
									</div>
									<div class="form-group">
										<p>Giá tiền:</p>
										<div class="row">
											<div class="col-lg-4">
												<input type="text" class="form-control <?php echo (!empty($price_error)) ? 'has-error' : ''; ?>" name="price" placeholder="Giá" value="<?php echo $price; ?>">
												<small id="acreage" class="form-text"><?php echo $price_error;?></small>
											</div>
											<div class="col-lg-4">
												<div class="form-group one">
													<select class="form-control" name="unit">
														<option value="Tỷ" <?php if ($unit == "Tỷ") echo "selected='selected'";?>>Tỷ</option>
														<option value="Triệu" <?php if ($unit == "Triệu") echo "selected='selected'"; ?>>Triệu</option>
														<option value="Triệu/m2" <?php if ($unit == "Triệu/m2") echo "selected='selected'"; ?>>Triệu/m2</option>
														<option value="Triệu/tháng" <?php if ($unit == "Triệu/tháng") echo "selected='selected'"; ?>>Triệu/tháng</option>
													</select>
													<small id="acreage" class="form-text"><?php echo $unit_error;?></small>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="concurrent">
													<div class="custom-control custom-checkbox my-1 mr-sm-2">
														<input type="checkbox" class="custom-control-input" id="customControlInline" name="concurrent" value="Thoa thuan">
														<label class="custom-control-label" for="customControlInline">Thỏa thuận</label>
													</div>
												</div>	
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 col-lg-4">
									<div class="upload-img-form">
										<p>Chọn ảnh UpLoad</p>
										<div class="input-upload">
											<input  id="file-input" type="file" style="display: none" name="photo[]" multiple="">
											<label for="file-input"> Bấm chọn ảnh</label>	
										</div>
										<div id="thumb-output"></div>
										<p class="ly-upload">Tin đăng có ảnh được khách xem gấp<br> 10 lần tin không ảnh.<br>
										Bạn nên đăng ảnh cho tin của mình.</p>
										<small id="acreage" class="form-text"><?php echo $image_blank;?></small>
									</div>

								</div>
							</div>
							<div class="form-group">
								<p>Mô tả:</p>
								<textarea class="form-control <?php echo (!empty($description_error)) ? 'has-error' : ''; ?>" rows="5" id="comment" name="description" value="<?php echo $description; ?>"></textarea>
								<small id="acreage" class="form-text"><?php echo $description_error;?></small>
							</div>
							<div class="row add-info" <?php if( $hide == 1){ echo "hidden";}?>>
								<div class="col-4 col-sm-4 col-md-3 col-lg-2 text1">
									<span>THÊM THÔNG TIN</span>
								</div>
								<div class="col-8 col-sm-8 col-md-9 col-lg-10 text2">		
								</div>
							</div>
							<div class="list-check-input" <?php if( $hide == 1){ echo "hidden";}?>>
								<p>Lợi thế</p>
								<div class="row list-radio">
									<label class="check-input">Gần trường học và khu dân cư
										<input type="radio" checked name="balcony" value="Gần trường học và khu dân cư">
										<span class="checkmark"></span>
									</label>
									<label class="check-input">Gần chợ/siêu thị
										<input type="radio" name="balcony" value="Gần chợ/siêu thị">
										<span class="checkmark"></span>
									</label>
									<label class="check-input">Gần bệnh viện
										<input type="radio" name="balcony" value="Gần bệnh viện">
										<span class="checkmark"></span>
									</label>
									<label class="check-input">Gần bến tàu/xe
										<input type="radio" name="balcony" value="Gần bến tàu/xe">
										<span class="checkmark"></span>
									</label>
								</div>	
							</div>
							<div class="list-check-input" <?php if( $hide == 1){ echo "hidden";}?>>
								<p>Hướng</p>
								<div class="row list-radio">
									<label class="check-input">Đông
										<input type="radio" name="lay" checked value="Đông">
										<span class="checkmark"></span>
									</label>
									<label class="check-input">Tây Bắc
										<input type="radio" name="lay" value="Tây Bắc">
										<span class="checkmark"></span>
									</label>
									<label class="check-input">Tây
										<input type="radio" name="lay" value="Tây">
										<span class="checkmark"></span>
									</label>
									<label class="check-input">Tây Nam
										<input type="radio" name="lay" value="Tây Nam">
										<span class="checkmark"></span>
									</label>
									<label class="check-input">Nam
										<input type="radio" name="lay" value="Nam">
										<span class="checkmark"></span>
									</label>
									<label class="check-input">Đông Bắc
										<input type="radio" name="lay" value="Đông Bắc">
										<span class="checkmark"></span>
									</label>
									<label class="check-input">Bắc
										<input type="radio" name="lay" value="Bắc">
										<span class="checkmark"></span>
									</label>
									<label class="check-input">Đông Nam
										<input type="radio" name="lay" value="Đông Nam">
										<span class="checkmark"></span>
									</label>
								</div>
							</div>
							<div class="list-check-input" <?php if( $hide == 1){ echo "hidden";}?>>
								<p>Giấy tờ pháp lí:</p>
								<div class="row list-radio">
									<label class="check-input">Khác
										<input type="radio" name="license" checked value="Khác">
										<span class="checkmark"></span>
									</label>
									<label class="check-input">Có sổ hồng
										<input type="radio" name="license" value="Có cửa sổ">
										<span class="checkmark"></span>
									</label>
									<label class="check-input">Hợp đồng mua bán
										<input type="radio" name="license" value="Chưa có cửa sổ">
										<span class="checkmark"></span>
									</label>
									<label class="check-input">Văn phòng chuyển nhượng
										<input type="radio" name="license" value="Chưa có cửa sổ">
										<span class="checkmark"></span>
									</label>
								</div>	
							</div>
							<div class="row post1">
								<div class="col-4 col-sm-4 col-md-3 col-lg-2 text1">
									<span>THÔNG TIN LIÊN HỆ</span>
								</div>
								<div class="col-8 col-sm-8 col-lg-10 text2">		
								</div>
							</div>
							<div class="row">
								<div class="col-md-9 col-lg-2">

								</div>
								<div class="col-lg-8">
									<div class="form-group">
										<input type="text" name="name" class="form-control <?php echo (!empty($name_error)) ? 'has-error' : ''; ?>"  placeholder="Tên" value="<?php echo $name; ?>">
										<small id="acreage" class="form-text"><?php echo $name_error;?></small>
									</div>
									<div class="form-group">
										<input type="text" name="phonenumber" class="form-control <?php echo (!empty($phonenumber_error)) ? 'has-error' : ''; ?>" placeholder="Di động" value="<?php echo $phonenumber; ?>">
										<small id="acreage" class="form-text"><?php echo $phonenumber_error;?></small>
									</div>
									<div class="form-group">
										<input type="text" name="email" class="form-control <?php echo (!empty($email_error)) ? 'has-error' : ''; ?>" placeholder="Email" value="<?php echo $email; ?>">
										<small id="acreage" class="form-text"><?php echo $email_error;?></small>
									</div>
								</div>
								<div class="col-lg-2">

								</div>
							</div>
							<div class="btn-center text-center">
								<button type="submit" name="sb_post" class="btn btn-warning">Đăng tin</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>