<section>
	<div class="container">
		<div class="row">
			<?php include_once('controllers/menuleftController.php'); ?>
		<div class="product col-lg-9">
			<div class="row">
				<div class="col-md-4 col-lg-4 picture">
					<a href="#" title=""><img src="./libraries/images/interface/banner-tin-tuong-tac.jpg" class="img-responsive" alt=""></a>
				</div>

				<div class="col-md-8 col-lg-8 slide-image">
					<div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
						<div class="carousel-inner">
							<div class="carousel-item active">
								<img class="img-responsive d-block w-100" src="./libraries/images/interface/12-06.png" alt="First slide">
							</div>
							<div class="carousel-item">
								<img class="img-responsive d-block w-100" src="./libraries/images/interface/ads_490x200.jpg" alt="Second slide">
							</div>
							<div class="carousel-item">
								<img class="img-responsive d-block w-100" src="./libraries/images/interface/AnBinh-City.png" alt="Third slide">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="title">
				<p>Mua bán rao vặt, trang mua bán rao vặt trên toàn quốc</p>
			</div>
			<div class="show-product" >
				<div class="row list_product">
					<?php

					foreach ($result as $row) {
						?>
						<div class="col-md-6 col-lg-4 show-product1">
							<div class="image-thumb">
								<img src="<?= $row['image']?>" class="img-responsive" alt="">
								<div class="news">
									<a href="index.php?controller=posts&action=listpost&id=<?= $row['category_id']?>" title=""><?= $row['name']?></a>
									<span><?= $row['number']?></span>
								</div>
							</div>	
						</div>
						<?php
					}
					?>
				</div>
			</div>	
		</div>
	</div>
</div>
</section>