	<section>
		<div class="container">
			<div class="row">
			<?php include_once('controllers/menuleftController.php'); ?>
			<div class="col-lg-9">
				<div class="container list-post list-product">
					<div class="title_list">
						<span class="uppcase">Chất lượng của bài đăng tạo nên thương hiệu</span><br>
						<span>Hiện tại có <strong><?=$count?></strong> bài đăng trên Rồng Bay</span>
					</div>
					<hr>
					<?php
					while ($row = $result->fetch_array()){
						?>
						<div class="row">
							<div class="col-4 col-sm-5 col-md-3 col-lg-3">
								<div class="img_post">
									<img src="<?php echo $row['image']?>" alt="">
								</div>
							</div>
							<div class="col-8 col-sm-7 col-md-6 col-lg-6">
								<div class="title_post">
									<a href="index.php?controller=posts&action=detailpost&id=<?php echo $row['post_id']?>" title=""><?php echo $row['title']?></a>
								</div>
								<div class="info-post">
									<span><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;<b>Dự án,</b><?php echo $row['address']?></span><br>
									<span>Giá: <strong><?php echo $row['price']?> (<?php echo $row['unit']?>)</strong></span><span <?php if($row['acreage']== 0){echo "hidden";}?>>- Diện tích: <?php echo $row['acreage']?> m2</span><br>
									<span <?php if($row['balcony']==""){echo "hidden";}?>>Hướng: <?php echo $row['lay']?>, <?php echo $row['balcony']?>, <?php echo $row['license']?></span><br>
									<span>Post date: <b><?php echo $row['created_at']?></b></span>
								</div>

							</div>
							<div class="col-md-3 col-lg-3">
								<div class="avatar text-center">
									<img src="./libraries/images/interface/no_avatar.jpg" alt=""><br>
									<a href="#" title=""><?php echo $row['fullname']?></a>
								</div>
							</div>
						</div>
						<hr>
						<?php
					}
					?>
					<div class="row">
						<div class="col-8 offset-4 ol-sm-8 offset-sm-4 col-md-8 offset-md-4 col-lg-8 offset-lg-4">
							<div class="page">
								<nav aria-label="Page navigation example">
									<ul class="pagination">
										<li class="page-item">
											<a class="page-link" href="<?=
											(
												(!isset($page)||$page==''||$page==1)?
												'index.php?controller=search&parent='.$parent_id.'&keysearch='.$keysearch:
												'index.php?controller=search&parent='.$parent_id.'&keysearch='.$keysearch.'&page='.($page-1)
											)
											?>" aria-label="Previous">
											<span aria-hidden="true">&laquo;</span>
											<span class="sr-only">Previous</span>
										</a>
									</li>
									<?php 
									$total_page=ceil($count_all_post/5);
									for ($i=1; $i <=$total_page ; $i++) { 
										?>
										<li class="page-item"><a class="page-link" href="index.php?controller=search&parent=<?=$parent_id?>&keysearch=<?=$keysearch?>&page=<?=$i?>"><?=$i?></a></li>
										<?php
									}
									?>
									<li class="page-item">
										<a class="page-link" href="<?=
										((!isset($page)||$page=='')?
										'index.php?controller=search&page=2':
										(
											($page<$total_page)?
											'index.php?controller=search&parent='.$parent_id.'&keysearch='.$keysearch.'&page='.($page+1):
											'index.php?controller=search&parent='.$parent_id.'&keysearch='.$keysearch.'&page='.$total_page
										)
										)?>" aria-label="Next">
										<span aria-hidden="true">&raquo;</span>
										<span class="sr-only">Next</span>
									</a>
								</li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</section>