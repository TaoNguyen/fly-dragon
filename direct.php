<?php 
if(!empty($_POST)){
include_once('libraries/connect.php');
include_once('libraries/__autoload.php');
}
if(isset($_GET['controller'])){
	switch($_GET['controller']){
		case 'posts': include_once('controllers/posts/PostController.php');
		break;
		case 'home': include_once('controllers/home/HomeController.php');
		break;
		case 'search': include_once('controllers/search/SearchController.php');
		break;
		default:
			include_once('controllers/home/HomeController.php');
			break;
	}
} else {
	header('location: index.php?controller=home');
}
 ?>