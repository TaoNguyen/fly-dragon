			<div class="menu-inforcus">

				<div class="logo-cusinfo">
					<img style="width: 177px;height: 80px;" src="../libraries/images/userCus/default/raovat_rongbay.png" class="img-responsive" alt="Image">
				</div>
				<div class="sidebar">
					<ul class="nav flex-column ">
						<li class ="nav-item ">
							<a class ="nav-link hr hro btn-block" >Danh Mục</a>
						</li>
						<li class="nav-item ">
							<span><a class=" hr btn-block" href="../index.php?controller=home">Trang chủ</a></span>
						</li>
						<li class ="nav-item ">
							<a class ="nav-link hr btn-block active" href="index.php?controller=customer&action=inforcus">Hồ sơ của bạn</a>
						</li>
						<li class ="nav-item ">
							<a class  ="nav-link hr md" href="#myModal">Thay đổi mật khẩu</a>

							<div class="modal face" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"aria-hidden="true" >
								<div class="modal-changePass">
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<img src="assets/img/changePass.png" class="img-responsive" alt="Image">
											</div>
											<div class="modal-content">
												<div class="modal-body">
													<div class="container">
														<div class="row">
															<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
																<div class="form-pass">
																	<form action="index.php?controller=customer&action=changepass" method="post" id="register-form">
																		<div class="input-field input-form">
																			<input type="password" name="old_pass" class="">
																			<label for="name">Mật khẩu củ</label>
																		</div>
																		<div class="input-field input-form">
																			<input id="password" type="password" name="new_pass">
																			<label for="password">Mật khẩu mới</label>
																		</div>
																		<div class="input-field input-form">
																			<input id="password-se" type="password" name="confirm_pass" class="">
																			<label for="password-se">Nhập lại mật khẩu</label>
																		</div>
																		<div class="submit-login-rg">
																			<button class="btn-change-pass light-blue accent-3 btn" type="submit" name="change_pass_btn">Thay Đổi</button>
																		</div>
																	</form>
																</div>
															</div>			
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>	
							</div>
						</li>
						<li class ="nav-item  ">
							<a class  ="nav-link hr btn-block" href="index.php?controller=customer&action=history">Dòng thời gian</a>
						</li>
						<li class ="nav-item  ">
							<a class  ="nav-link hr btn-block" href="index.php?controller=customer&action=logout">Đăng xuất</a>
						</li>
					</ul> 
				</div>
			</div>