<?php  
$show=$_SESSION['u_info'];
?>

<div class="container">
	<div class="row">
		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
			<?php 
			include('menuLeft.php');
			?>
		</div>
		<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
			<div class="inforcus">
				<?php include('headerProfile.php'); ?>
				<div class="content">
					<div class="form-inforcus">
						<div class="row">
							<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 offset-md-1 offset-lg-1">
								<form action="" id="" method="POST" role="form" enctype="multipart/form-data">

									<div class="form-group row align">
										<label for="inputName" class="col-sm-5 col-lg-5 col-md-5 col-form-label">Họ Và Tên:</label>
										<div class="col-sm-6 col-lg-6 col-md-6">
											<input type="text" class="form-control-plaintext" name="name_cus" id="inputName" value="<?php echo $show['fullname']; ?>">
										</div>
										<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
											<i title="Được phép chỉnh sửa" class="fa fa-pencil-square-o" aria-hidden="true"></i>
										</div>
									</div>

									<div class="form-group row align">
										<label for="inputGender" class="col-sm-5 col-lg-5 col-md-5 col-form-label">Giới Tính:</label>
										<div class="col-sm-7 col-lg-7 col-md-7">
											<input type="text" readonly class="form-control-plaintext" name="gender_cus" id="inputGender" value="<?php
											if($show['gender'] == 1 || $show['gender'] == 'Nam') {
												echo "Nam"; 
												} if($show['gender'] == 2 || $show['gender'] == 'Nữ') {
													echo "Nữ";
												}
												?>" title="Không được phép chỉnh sửa">
											</div>
										</div>

										<div class="form-group row align">
											<label for="staticEmail" class="col-sm-5 col-lg-5 col-md-5 col-form-label">Email:</label>
											<div class="col-sm-7 col-lg-7 col-md-7">
												<input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?php echo $show['email']; ?> " title="Không được phép chỉnh sửa">
											</div>
										</div>

										<div class="form-group row align">
											<label for="inputPhone" class="col-sm-5 col-lg-5 col-md-5 col-form-label">Điện Thoại:</label>
											<div class="col-sm-6 col-lg-6 col-md-6">
												<input type="number"  class="form-control-plaintext" name="phone_cus" id="inputPhone" value="<?php echo $show['phone']; ?>">
											</div>
											<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
												<i title="Được phép chỉnh sửa" class="fa fa-pencil-square-o" aria-hidden="true"></i>
											</div>								
										</div>

										<div class="form-group row align">
											<label for="inputAddress" class="col-sm-5 col-lg-5 col-md-5 col-form-label">Địa Chỉ:</label>
											<div class="col-sm-6 col-lg-6 col-md-6">
												<input type="text"  class="form-control-plaintext" name="address_cus" id="inputAddress" value="<?php echo $show['address']; ?>">
												<input type="hidden" name="ID_cus" value="<?=$_GET['id']; echo $show['id']; ?>">
											</div>
											<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
												<i title="Được phép chỉnh sửa" class="fa fa-pencil-square-o" aria-hidden="true"></i>
											</div>
										</div>


										<div class="form-group row align">
											<label for="inputName" class="col-sm-5 col-lg-5 col-md-5 col-form-label label-last">Avatar:</label>
											<div class="col-sm-7 col-lg-7 col-md-7">
												<div id="image_preview">
													<img type="	" class="avatar" id="previewing" src="../<?php echo $show['img']; ?>" />
												</div>
												<div id="selectImage">
													<input type="file" name="myimage" id="file" class="" />
												</div>

												<?php 
												if (isset($_SESSION['u_info'])) {
													$img2 = $show['img'];
													?>
													<input type="hidden" name="img_cus" id="file" value="<?=$img2?>" />
													<?php
												}
												?>
											</div>

										</div>

										<div class="form-group ">
											<div class="text-center">
												<button type="submit" name="update_btn" class="btn btn-success">Cập Nhật</button>
											</div>
										</div>

									</form>
								</div>

								<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
							</div>
						</div>
						<div class="border3"></div>
					</div>
				</div>
			</div>
		</div>
	</div>