
<div class="header-profile profile">
	<div class="bd">
		<div class="border1"></div>
	</div>
	<div class="inforcus-avatar text-center ">
		<div id="image_preview">
			
			<?php 
			if (isset($_SESSION['u_info'])) {
				$show=$_SESSION['u_info'];
				?>
				<img src="../<?php echo $show['img']; ?>" class="avatar img-responsive" alt="Image">
				<?php
			} 
			?>
		</div>
	</div>
	<div class="title-cus">
		<h5 class="text-center"><?=$show['fullname'];?></h5>
	</div>
	<div class="border2"></div>
</div>
