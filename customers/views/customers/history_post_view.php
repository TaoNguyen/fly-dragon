

<div class="container">
	<div class="row">
		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
			<?php 
			include('menuLeft.php');
			?>
		</div>

		<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
			<div class="inforcus">
				<?php include('headerProfile.php'); ?>
			</div>

			<?php 
			if (mysqli_num_rows($post_history) > 0) {
				while ($row = mysqli_fetch_assoc($post_history)) {
					?>
					<div class="history-post">
						<div class="row img-thumbnail-G">
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<div class="">
									<div class="info-left">
										<div class="form-group">
											<?php 
											$cutTitle = $row['title'];

											$count     = strlen($cutTitle);
											if($count < 34) {
												$show  = ucfirst(trim($cutTitle));

											}
											if ($count >= 34) {

												$stTitle = substr($cutTitle ,0, 38);
												// $more      = '...';
												$show  = ucfirst(trim($stTitle));
											}
											?>
											<input type="text" readonly class="form-control-plaintext" value="Danh Mục: <?=$show;?>">
											<span class="id-cus-post">Mã bài: <?=$row['id']; ?></span>
										</div>
										<div class="form-group">
											<input type="text" readonly class="form-control-plaintext" value="Thành Phố: <?=$row['province_name'];?> ">
											<span class="time"><?=date('d/m/Y', strtotime($row['created_at'])); ?></span>
										</div>
										<div class="form-group">
											<input type="text" readonly class="form-control-plaintext" value="Quận: <?=$row['district_name'];?> ">
										</div>
										<div class="form-group">
											<input type="text" readonly class="form-control-plaintext" value="Diện tích: <?=$row['acreage'];?>m² ">
										</div>
										<div class="form-group">
											<input type="text" readonly class="form-control-plaintext" value="Giá: <?=$row['price'];?> <?=$row['unit'];?>">
										</div>
									</div>
									<div class="form-group">
										<?php 
										$cutString = $row['description'];
										$more      = '..';
										$count     = strlen($cutString);
										if($count < 58) {
											$print  = ucfirst(trim($cutString));
										}
										if ($count > 58) {
											$string = substr($cutString ,0, 71);
											$print  = ucfirst(trim($string));
										}
										?>
										<input type="text" readonly class="form-control-plaintext" value="Mô Tả: <?=$print;?><?=$more;?>">
										
									</div>
								</div>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
								<div class="img-post">
									<a href="" title="">
										<img
										src="../<?php echo $row['post_img']; ?>"

										class="img-responsive img-thumbnail" alt="Image">
										<span class="icon-hot"></span>
									</a>
								</div>
							</div>
							<div class="action-item">
								<div class="icon-item">
									<div class="edit-item">
										<a href="index.php?controller=customer&action=edit&id=<?php echo $row['id'];?>" title="Chỉnh sửa bài đăng"><img src="assets/img/site/edit.png" width="20" height="20" class="img-responsive" alt="Image"></a>
									</div>

									<div class="delete-item">
										<a class='delete' href="index.php?controller=customer&action=del&id=<?php echo $row['id'];?>" title="Xóa bài đăng "><img src="assets/img/site/deleteItem.ico" width="20" height="20" class="img-responsive" alt="Image"></a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php
				}
			}
			?>
		</div>
	</div>
</div>
