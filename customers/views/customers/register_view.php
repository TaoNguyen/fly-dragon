<div class="container" style="margin-top:1px">
	<div class="form">
		<div class="row">
			<div class="col-md-8 col-lg-8 offset-md-2 offset-lg-2">
				<div class="panel-body-form-register">
					<div class="header">
						<h3 class="text-center">Đăng Ký</h3>
					</div>
					<br>
					<form action="index.php?controller=customer&action=register" method="POST" role="form" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-10 col-lg-10 offset-lg-1 offset-md-1">
								
								<div class="form-group">
									<label for="name">Họ Tên:</label>
									<div class="input-group"> 
										<input class="form-control" placeholder="" name="full_name" type="text" required="">
									</div>
								</div>
								<div class="form-group">
									<label for="gender">Giới Tính:</label>
									<select id="select2-1" name="gender" class="form-control" aria-hidden="true">
										<option value="none" ></option>
										<option value="male">Male</option>
										<option value="femail">Female</option>
									</select>
								</div>
								<div class="form-group">
									<label for="email">Email:</label>
									<div class="input-group"> 
										<input class="form-control" placeholder="" name="email" type="email" required="">
									</div>
								</div>
								<div class="form-group">
									<label for="phone">Điện Thoại:</label> 
									<div class="input-group">
										<input class="form-control" placeholder="" name="phone" type="number" required="">
									</div>
								</div>
								<div class="form-group">
									<label for="address">Địa Chỉ:</label>
									<div class="input-group"> 

										<input class="form-control" placeholder="" name="address" type="text" required="">
									</div>
								</div>
								<div class="form-group">
									<label for="password">Mật khẩu:</label>
									<div class="input-group"> 
										<input class="form-control" placeholder="" name="password" type="password" required="">
									</div>
									<input type="hidden" name="ID_user" value="<?=$_GET['id']?>">
								</div>

								<div class="form-group">
									<label for="password">Nhập mật khẩu:</label>
									<div class="input-group"> 
										<input class="form-control" placeholder="" name="repassword" type="password" required="">
									</div>
									<input type="hidden" name="ID_user" value="<?=$_GET['id']?>">
								</div>

								<div class="form-group">
									<div class="text-center">
										<input type="submit" style="background: #12b1eb;" class="btn btn-block login-btn" name ="register_btn" value="Đăng Ký" >
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
