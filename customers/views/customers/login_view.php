<nav class="navfeature nav-top menu-top">
	<div class="nav-wrapper container">
		<ul class="right hide-on-med-and-down">
			<li><a class='dropdown-button dropdown-button-top' data-activates='dropdown1'>Đăng tin ưu tiên</a>
			</li>
			<li><a href="#">Hỏi & đáp</a></li>
			<li><a class='dropdown-button dropdown-button-top' href='#' data-activates='dropdown1'>Hổ trợ</a>
			</li>
			<li><a href="#">Mua quảng cáo</a></li>
			<?php 
			if (!isset($_SESSION['u_info'])) {
				?>
				<li><a href="#modal-rg" class="modal-trigger lighten-2 registersite">Đăng ký</a></li>
				<li><a href="#modal-rg" class="yel low-text lighten-2 modal-trigger waves-effect waves-light loginsite">Đăng nhập</a></li>
				<?php
			} elseif (isset($_SESSION['u_info'])) {
				$show=$_SESSION['u_info'];
				?> 

				<li><a href="index.php?controller=customer&action=inforcus" ><?php echo $show['email']; ?></a></li>
				<li><a href="index.php?controller=customer&action=logout" >Logout</a></li>
				<?php
			} 
			?>
		</ul>
	</div>
</nav>
<div id="modal-rg" class="modal modal-login">
	<div class="modal-conten ">
		<div class="button-login-rg ">
			<ul class="tabs">
				<li class="tab "><a href="#test-swipe-1">Đăng Nhập</a></li>
				<li class="tab"><a href="#test-swipe-2">Đăng Ký</a></li>
			</ul>
		</div>
		<div id="test-swipe-2"  class="form-rigen">
			<form action="index.php?controller=customer&action=register" method="post" id="register-form">
				<div class="input-field input-form">
					<input id="name" type="text" name="full_name" class="">
					<label for="name">Họ và tên</label>
				</div>
				<div class="input-field input-form-radio">
					<label for="gender">Giới tính</label>
					<br>
					<input name="gender" type="radio" id="test1" value="1" checked=""/>
					<label for="test1">Nam</label>
					<input name="gender" type="radio" id="test2" value="2"/>
					<label for="test2">Nữ</label>
				</div>
				<div class="input-field input-form">
					<input id="email" type="text" name="email">
					<label for="email">Email</label>
				</div>
				<div class="input-field input-form">
					<input id="Phone" type="text" name="phone" class="">
					<label for="phone">Điện Thoại</label>
				</div>
				<div class="input-field input-form">
					<input id="location" type="text" name="address" class="">
					<label for="location">Địa chỉ</label>
				</div>
				<div class="input-field input-form">
					<input id="password" type="password" name="password">
					<label for="password">Mật khẩu</label>
				</div>
				<div class="input-field input-form">
					<input id="password-se" type="password" name="repassword">
					<label for="password-se">Xác Mật khẩu</label>
				</div>
				<div class="submit-login-rg">
					<button class="light-blue accent-3 btn" type="submit" name="register_btn">Tạo Tài Khoản</button>
				</div>
				<div class="loginfb">
					<a href="https://www.facebook.com/v2.2/dialog/oauth?client_id=184755148827595&amp;state=262d9015425ea828719d9ad39f1c5d52&amp;response_type=code&amp;sdk=php-sdk-5.6.2&amp;redirect_uri=https%3A%2F%2Fdanang.land%2Fmember%2Fregister.html&amp;scope=email"><img style="width:218px;" src="assets/img/site/signup2069.png?1523588361" alt="" /></a>                    
				</div>
			</form>
		</div>
		<div id="test-swipe-1"  class="form-rigen">
			<form action="index.php?controller=customer&action=login" method="post" id="login-form">
				<div class="input-field input-form">
					<input type="text" name="email" class="">
					<label for="email">Email đăng nhập</label>
				</div>
				<div class="input-field input-form">
					<input id="pasword" type="password" name="password" class="">
					<label for="password">Mật khẩu</label>
					
				</div>
				<div class="submit-login-rg">
					<button class=" light-blue accent-4 btn" type="submit" name="submit">Đăng Nhập</button>
				</div>
				<div class="loginfb">
					<a href="https://www.facebook.com/v2.2/dialog/oauth?client_id=184755148827595&amp;state=262d9015425ea828719d9ad39f1c5d52&amp;response_type=code&amp;sdk=php-sdk-5.6.2&amp;redirect_uri=https%3A%2F%2Fdanang.land%2Fmember%2Flogin.html&amp;scope=email"><img style="width:218px;" src="assets/img/site/signindfc6.png?1523588370" alt="" /></a>                    
				</div>
			</form>
		</div>
	</div>
</div>
