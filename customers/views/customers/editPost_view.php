<?php if (isset($_SESSION['u_info'])) {
	// if (isset($_GET['id'])) {
	// 	$row = $result->fetch_assoc();
	// 	$list_district = $model->getListDistrict($row['city_id']); 
	// 	$list_province = $model->getListProvince();
	// }
	?>

	<div class="container">
		<div class="row">
			<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
				<?php 
				include('menuLeft.php');
				?>
			</div>

			<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
				<div class="bgr-post-infor">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="header-post-infor">
								<div class="title-post-infor">
									<div class="row">
										<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
											<p>CHỈNH SỬA</p>
										</div>
										<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
											<div class="border-post-infor"></div>
										</div>
									</div>
								</div>
								<div class="container">
									<div class="header-second">
										<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<div class="post-form">
													<form action="" method="POST"  role="form"  enctype="multipart/form-data">

														<div class="add-info">
															<div class="form-group">
																<!-- <p> Mã Bài Đăng: <?=$row['id'];?></p>  -->
															</div>
															<!-- name post -->
															<div class="form-group">
																<input type="text" class="form-control" value="<?=$row['title']; ?>" name="title_p" id="" placeholder="Tiêu đề">
																<input type="hidden" name="post_ID" value="<?= $_GET['id'] ?>"/>
															</div>

															<div class="row">
																<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

																	<!-- city -->
																	<div class="form-group">
																		<select class="form-control" id="city" name="city_p">
																			<option value="none" selected="selected">Tỉnh/Thành</option>
																			<?php
																			while ($row_list_province = $list_province->fetch_array()) {
																				?>
																				<option value="<?= $row_list_province['id'] ?>" <?= ($row['city_id'] == $row_list_province['id']) ? ' selected="selected"' : ''; ?>><?= $row_list_province['name'] ?></option>;
																				<?php
																			}
																			?>
																		</select>
																	</div>

																	<!-- district -->
																	<div class="form-group">
																		<select class="form-control" id="district" name="district_p">
																			<!-- <option value="none" selected="selected">Quận/Huyện</option> -->
																			<?php
																			while ($row_list_district = $list_district->fetch_array()) {
																				?>
																				<option value="<?= $row_list_district['id'] ?>" <?= ($row['district_id'] == $row_list_district['id']) ? ' selected="selected"' : ''; ?>><?= $row_list_district['name'] ?></option>
																				<?php
																			}
																			?>
																		</select>
																	</div>

																	<!-- address -->
																	<div class="form-group">
																		<input type="text" class="form-control" name="address_p" value="<?=$row['address'];?>" id="exampleFormControlInput1" placeholder="Địa chỉ">
																		<label class="more-detail" for="exampleFormControlSelect1">* Nếu có số nhà cụ thể bạn hãy nhập đầy đủ thông tin</label>
																	</div>

																	<div class="form-group">
																		<div class="row">
																			<!-- acreage -->
																			<?php 
																			if (($cat_parent['parent'] == 1) || ($cat_parent['parent'] == 2) || ($cat_parent['parent'] == 5) ) {
																				?>
																				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" id="acreage">
																					<label class="text-l">Diện tích</label>
																					<input type="text" class="form-control" name="acreage_P" value="<?=$row['acreage'];?>"  placeholder="Diện tích">
																				</div>
																				<?php
																			} else {
																				?>
																				<div style="display: none;">
																					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" id="acreage">
																						<label class="text-l">Diện tích</label>
																						<input type="text" class="form-control" name="acreage_P" value="<?=$row['acreage'];?>"  placeholder="Diện tích">
																					</div>
																				</div>
																				<?php
																			}
																			?>
																			<!-- price -->
																			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
																				<label class="text-l">Giá</label>
																				<input type="text" class="form-control" name="price_p" value="<?=$row['price']; ?>"  placeholder="Giá">
																			</div>
																			<!-- unit -->
																			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
																				<label class="text-l">Đơn vị</label>
																				<div class="form-group one">
																					<select class="form-control" id="unit" name="unit_p">
																						<option value="Tỷ"<?php echo ($row['unit'] == 'Tỷ') ? ' selected="selected"' : ''; ?>>Tỷ</option>
																						<option value="Triệu"<?php echo ($row['unit'] == 'Triệu') ? ' selected="selected"' : ''; ?>>Triệu</option>
																						<option value="Triệu/m2"<?php echo ($row['unit'] == 'Triệu/m2') ? ' selected="selected"' : ''; ?>>Triệu/m2</option>
																						<option value="Triệu/tháng"<?php echo ($row['unit'] == 'Triệu/tháng') ? ' selected="selected"' : ''; ?>>Triệu/tháng</option>
																					</select>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="form-group">
																<textarea style="height: 125px;" autofocus class="form-control" placeholder="Vui lòng thêm mô tả mới cho bài đăng của bạn" name="description_p" value="<?=$row['description'];?>"  id="editor1" ></textarea>
															</div>

															<div class="form-group">
																<div class="col-sm-10">


																	<div class="picture">
																		<div class="row">
																			<?php
																			while ($a = $countImg->fetch_array()) {
																				?>
																				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
																					<div class="img-ed">
																						<img src="../<?php echo $a['image'] ?>" >
																					</div>
																				</div>

																			<?php }
																			?>
																		</div>
																	</div>



																</div>
															</div>

															<div class="form-group">
																<div class="col-sm-10">
																	<input type="file" class="form-control" id="file" name="file[]" multiple="">
																</div>
															</div>

														</div>
														<?php 
														if (($cat_parent['parent'] == 1) || ($cat_parent['parent'] == 2) || ($cat_parent['parent'] == 5) )
														{
															?>
															<div class="infor_edit">
																<div class="row post1">
																	<div class="col-lg-4 text1">
																		<span>CHỈNH SỬA</span>
																	</div>
																	<div class="col-lg-8 text2">		
																	</div>
																</div>

																<div class="checkbox">
																	<p>Lợi thế:</p>
																	<div class="row">
																		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
																			<div class="form-check form-check-inline">
																				<input type="checkbox" class="form-check-input" id="materialInline1" name="balcony" value="Gần trường"<?php echo ($row['balcony'] == 'Gần trường') ? ' checked="checked"' : ''; ?>>
																				<label class="form-check-label" for="materialInline1">Gần trường</label>
																			</div>
																		</div>
																		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

																			<div class="form-check form-check-inline">
																				<input type="checkbox" class="form-check-input" id="materialInline2" name="balcony" value="Gần chợ/siêu thị"<?php echo ($row['balcony'] == 'Gần chợ/siêu thị') ? ' checked="checked"' : ''; ?>>
																				<label class="form-check-label" for="materialInline2">Gần chợ/siêu thị</label>
																			</div>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

																			<div class="form-check form-check-inline">
																				<input type="checkbox" class="form-check-input" id="materialInline3" name="balcony" value="Gần bệnh viện"<?php echo ($row['balcony'] == 'Gần bệnh viện') ? ' checked="checked"' : ''; ?>>
																				<label class="form-check-label" for="materialInline3">Gần bệnh viện</label>
																			</div>
																		</div>
																		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
																			<div class="form-check form-check-inline">
																				<input type="checkbox" class="form-check-input" id="materialInline4" name="balcony" value="Gần bến tàu/xe"<?php echo ($row['balcony'] == 'Gần bến tàu/xe') ? ' checked="checked"' : ''; ?>>
																				<label class="form-check-label" for="materialInline4">Gần bến tàu/xe</label>
																			</div>
																		</div>
																	</div>
																</div>

																<div class="checkbox">
																	<p>Hướng:</p>
																	<div class="row">

																		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
																			<div class="custom-control custom-radio custom-control-inline">
																				<input type="radio" class="custom-control-input" id="defaultInline1" name="lay"  value="Đông"<?php echo ($row['lay'] == 'Đông') ? ' checked="checked"' : ''; ?>>
																				<label class="custom-control-label" for="defaultInline1">Đông</label>
																			</div>
																		</div>
																		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
																			<div class="custom-control custom-radio custom-control-inline">
																				<input type="radio" class="custom-control-input" id="defaultInline2" name="lay" value="Tây Bắc"<?php echo ($row['lay'] == 'Tây Bắc') ? ' checked="checked"' : ''; ?>>
																				<label class="custom-control-label" for="defaultInline2">Tây Bắc</label>
																			</div>
																		</div>
																		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
																			<div class="custom-control custom-radio custom-control-inline">
																				<input type="radio" class="custom-control-input" id="defaultInline3" name="lay" value="Tây"<?php echo ($row['lay'] == 'Tây') ? ' checked="checked"' : ''; ?>>
																				<label class="custom-control-label" for="defaultInline3">Tây</label>
																			</div>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
																			<div class="custom-control custom-radio custom-control-inline">
																				<input type="radio" class="custom-control-input" id="defaultInline4"  name="lay" value="Tây Nam"<?php echo ($row['lay'] == 'Tây Nam') ? ' checked="checked"' : ''; ?>>
																				<label class="custom-control-label" for="defaultInline4">Tây Nam</label>
																			</div>
																		</div>
																		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
																			<div class="custom-control custom-radio custom-control-inline">
																				<input type="radio" class="custom-control-input" id="defaultInline5" name="lay" value="Nam"<?php echo ($row['lay'] == 'Nam') ? ' checked="checked"' : ''; ?>>
																				<label class="custom-control-label" for="defaultInline5">Nam</label>
																			</div>
																		</div>
																		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
																			<div class="custom-control custom-radio custom-control-inline">
																				<input type="radio" class="custom-control-input" id="defaultInline6" name="lay" value="Đông Bắc"<?php echo ($row['lay'] == 'Đông Bắc') ? ' checked="checked"' : ''; ?>>
																				<label class="custom-control-label" for="defaultInline6">Đông Bắc</label>
																			</div>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
																			<div class="custom-control custom-radio custom-control-inline">
																				<input type="radio" class="custom-control-input" id="defaultInline7" name="lay" value="Bắc"<?php echo ($row['lay'] == 'Bắc') ? ' checked="checked"' : ''; ?>>
																				<label class="custom-control-label" for="defaultInline7">Bắc</label>
																			</div>
																		</div>
																		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
																			<div class="custom-control custom-radio custom-control-inline">
																				<input type="radio" class="custom-control-input" id="defaultInline8" name="lay" value="Đông Nam"<?php echo ($row['lay'] == 'Đông Nam') ? ' checked="checked"' : ''; ?>>
																				<label class="custom-control-label" for="defaultInline8">Đông Nam</label>
																			</div>
																		</div>
																	</div>
																</div>

																<div class="checkbox">
																	<p>Giấy tờ pháp lý:</p>
																	<div class="row">
																		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
																			<div class="form-check">
																				<input type="radio" class="form-check-input" id="materialGroupExample2" name="license" value="Có sổ hồng"<?php echo ($row['license'] == 'Có sổ hồng') ? ' checked="checked"' : ''; ?>>
																				<label class="form-check-label" for="materialGroupExample2">Có sổ hồng</label>
																			</div>
																		</div>
																		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">														
																			<div class="form-check">
																				<input type="radio" class="form-check-input" id="materialGroupExample1" name="license" value="Hợp đồng mua bán"<?php echo ($row['license'] == 'Hợp đồng mua bán') ? ' checked="checked"' : ''; ?>>
																				<label class="form-check-label" for="materialGroupExample1">Hợp đồng mua bán</label>
																			</div>
																		</div>
																		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
																			<div class="form-check">
																				<input type="radio" class="form-check-input" id="materialGroupExample3" name="license" value="Văn phòng chuyển nhượng"<?php echo ($row['license'] == 'Văn phòng chuyển nhượng') ? ' checked="checked"' : ''; ?>>
																				<label class="form-check-label" for="materialGroupExample3">Văn phòng chuyển nhượng</label>
																			</div>
																		</div>
																		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
																			<div class="form-check">
																				<input type="radio" class="form-check-input" id="materialGroupExample4" name="license" value="Khác"<?php echo ($row['license'] == 'Khác') ? ' checked="checked"' : ''; ?>>
																				<label class="form-check-label" for="materialGroupExample4">Khác</label>
																			</div>
																		</div>
																	</div>
																</div>
															</div>

															<?php
														} else {
															?> 
															<div class="infor_edit " style="display: none;">
																<div class="row post1">
																	<div class="col-lg-4 text1">
																		<span>CHỈNH SỬA</span>
																	</div>
																	<div class="col-lg-8 text2">		
																	</div>
																</div>

																<div class="checkbox">
																	<p>Lợi thế:</p>
																	<div class="row">
																		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
																			<div class="form-check form-check-inline">
																				<input type="checkbox" class="form-check-input" id="materialInline1" name="balcony" value="Gần trường"<?php echo ($row['balcony'] == 'Gần trường') ? ' checked="checked"' : ''; ?>>
																				<label class="form-check-label" for="materialInline1">Gần trường</label>
																			</div>
																		</div>
																		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

																			<div class="form-check form-check-inline">
																				<input type="checkbox" class="form-check-input" id="materialInline2" name="balcony" value="Gần chợ/siêu thị"<?php echo ($row['balcony'] == 'Gần chợ/siêu thị') ? ' checked="checked"' : ''; ?>>
																				<label class="form-check-label" for="materialInline2">Gần chợ/siêu thị</label>
																			</div>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

																			<div class="form-check form-check-inline">
																				<input type="checkbox" class="form-check-input" id="materialInline3" name="balcony" value="Gần bệnh viện"<?php echo ($row['balcony'] == 'Gần bệnh viện') ? ' checked="checked"' : ''; ?>>
																				<label class="form-check-label" for="materialInline3">Gần bệnh viện</label>
																			</div>
																		</div>
																		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
																			<div class="form-check form-check-inline">
																				<input type="checkbox" class="form-check-input" id="materialInline4" name="balcony" value="Gần bến tàu/xe"<?php echo ($row['balcony'] == 'Gần bến tàu/xe') ? ' checked="checked"' : ''; ?>>
																				<label class="form-check-label" for="materialInline4">Gần bến tàu/xe</label>
																			</div>
																		</div>
																	</div>
																</div>

																<div class="checkbox">
																	<p>Hướng:</p>
																	<div class="row">

																		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
																			<div class="custom-control custom-radio custom-control-inline">
																				<input type="radio" class="custom-control-input" id="defaultInline1" name="lay"  value="Đông"<?php echo ($row['lay'] == 'Đông') ? ' checked="checked"' : ''; ?>>
																				<label class="custom-control-label" for="defaultInline1">Đông</label>
																			</div>
																		</div>
																		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
																			<div class="custom-control custom-radio custom-control-inline">
																				<input type="radio" class="custom-control-input" id="defaultInline2" name="lay" value="Tây Bắc"<?php echo ($row['lay'] == 'Tây Bắc') ? ' checked="checked"' : ''; ?>>
																				<label class="custom-control-label" for="defaultInline2">Tây Bắc</label>
																			</div>
																		</div>
																		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
																			<div class="custom-control custom-radio custom-control-inline">
																				<input type="radio" class="custom-control-input" id="defaultInline3" name="lay" value="Tây"<?php echo ($row['lay'] == 'Tây') ? ' checked="checked"' : ''; ?>>
																				<label class="custom-control-label" for="defaultInline3">Tây</label>
																			</div>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
																			<div class="custom-control custom-radio custom-control-inline">
																				<input type="radio" class="custom-control-input" id="defaultInline4"  name="lay" value="Tây Nam"<?php echo ($row['lay'] == 'Tây Nam') ? ' checked="checked"' : ''; ?>>
																				<label class="custom-control-label" for="defaultInline4">Tây Nam</label>
																			</div>
																		</div>
																		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
																			<div class="custom-control custom-radio custom-control-inline">
																				<input type="radio" class="custom-control-input" id="defaultInline5" name="lay" value="Nam"<?php echo ($row['lay'] == 'Nam') ? ' checked="checked"' : ''; ?>>
																				<label class="custom-control-label" for="defaultInline5">Nam</label>
																			</div>
																		</div>
																		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
																			<div class="custom-control custom-radio custom-control-inline">
																				<input type="radio" class="custom-control-input" id="defaultInline6" name="lay" value="Đông Bắc"<?php echo ($row['lay'] == 'Đông Bắc') ? ' checked="checked"' : ''; ?>>
																				<label class="custom-control-label" for="defaultInline6">Đông Bắc</label>
																			</div>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
																			<div class="custom-control custom-radio custom-control-inline">
																				<input type="radio" class="custom-control-input" id="defaultInline7" name="lay" value="Bắc"<?php echo ($row['lay'] == 'Bắc') ? ' checked="checked"' : ''; ?>>
																				<label class="custom-control-label" for="defaultInline7">Bắc</label>
																			</div>
																		</div>
																		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
																			<div class="custom-control custom-radio custom-control-inline">
																				<input type="radio" class="custom-control-input" id="defaultInline8" name="lay" value="Đông Nam"<?php echo ($row['lay'] == 'Đông Nam') ? ' checked="checked"' : ''; ?>>
																				<label class="custom-control-label" for="defaultInline8">Đông Nam</label>
																			</div>
																		</div>
																	</div>
																</div>

																<div class="checkbox">
																	<p>Giấy tờ pháp lý:</p>
																	<div class="row">
																		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
																			<div class="form-check">
																				<input type="radio" class="form-check-input" id="materialGroupExample2" name="license" value="Có sổ hồng"<?php echo ($row['license'] == 'Có sổ hồng') ? ' checked="checked"' : ''; ?>>
																				<label class="form-check-label" for="materialGroupExample2">Có sổ hồng</label>
																			</div>
																		</div>
																		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">														
																			<div class="form-check">
																				<input type="radio" class="form-check-input" id="materialGroupExample1" name="license" value="Hợp đồng mua bán"<?php echo ($row['license'] == 'Hợp đồng mua bán') ? ' checked="checked"' : ''; ?>>
																				<label class="form-check-label" for="materialGroupExample1">Hợp đồng mua bán</label>
																			</div>
																		</div>
																		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
																			<div class="form-check">
																				<input type="radio" class="form-check-input" id="materialGroupExample3" name="license" value="Văn phòng chuyển nhượng"<?php echo ($row['license'] == 'Văn phòng chuyển nhượng') ? ' checked="checked"' : ''; ?>>
																				<label class="form-check-label" for="materialGroupExample3">Văn phòng chuyển nhượng</label>
																			</div>
																		</div>
																		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
																			<div class="form-check">
																				<input type="radio" class="form-check-input" id="materialGroupExample4" name="license" value="Khác"<?php echo ($row['license'] == 'Khác') ? ' checked="checked"' : ''; ?>>
																				<label class="form-check-label" for="materialGroupExample4">Khác</label>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<?php
														}
														?>
														

														<div class="row">
															<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"></div>
															<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 ">
																<div class="update-btn">
																	<button type="submit" name="update" class="btn btn-primary">Cập Nhật Thông Tin</button>
																</div>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
} ?>

