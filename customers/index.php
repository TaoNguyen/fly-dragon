<?php 
ob_start();
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Rong_Bay_ANT</title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Open+Sans:600'>

	<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/icon?family=Material+Icons" />
	<link type="text/css" rel="stylesheet" href="assets/css/xzoom2b5f.css?1500089656" />
	<link type="text/css" rel="stylesheet" href="assets/css/materialize.min61f7.css?1523933551" />
	<link type="text/css" rel="stylesheet" href="assets/css/index-style489c.css?1524196718" />
	<link rel="stylesheet" href="assets/css/stype.css">
</head>
<body>
	
	<?php
	include('../siteroot.php');
	// include('views/home/header_view.php');
	require_once('direct.php');
	// include('views/home/footer_view.php');
	?>

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
	<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
	<script src="https://code.jquery.com/jquery-3.3.1.js" type="text/javascript" charset="utf-8" async defer></script>
	
	<script type="text/javascript" src="assets/js/ckeditor/ckeditora608.js?1498992246"></script>
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js"></script>
	<script type="text/javascript" src="assets/js/xzoom.min2b5f.js?1500089656"></script>
	<!-- Hien -->
	<script type="text/javascript" src="assets/js/jquery.min4fb1.js?1495425930"></script>
	<script type="text/javascript" src="assets/js/jquery.validate.min90ec.js?1498789065"></script>
	<script type="text/javascript" src="assets/js/materialize.min5802.js?1497452712"></script>
	
	<script type="text/javascript" src="assets/js/style-index999f.js?1501905051"></script>
	<script data-cfasync="false" src="cdn-cgi/scripts/f2bf09f8/cloudflare-static/email-decode.min.js"></script>
	<script type="text/javascript" src="assets/js/validation.js"></script>
	<!-- end -->
	<script src="assets/js/script.js" type="text/javascript" charset="utf-8" async defer></script>

	<script src="assets/js/loadImageEdit.js" type="text/javascript" charset="utf-8" async defer></script>

</body>
</html>