<?php
if (isset($_SESSION['u_info'])) {
	if (isset($_SESSION['u_info'])) {
		$id_cus   = $_SESSION['u_info'];

		$show_cus = new Customer();
		$show     = $show_cus->getCus_byid($id_cus['id']);

		if (isset($_POST["update_btn"])) {


			$id_cus['id']       = $id_cus['info_id'];

			// $id_cus['']; die;
			$id_cus['fullname'] = $_POST['name_cus'];
			$id_cus['phone']    = $_POST['phone_cus'];
			$id_cus['address']  = $_POST['address_cus'];
			$id_cus['img']      = $_FILES["myimage"]["name"];
			
			$u_id               = $id_cus['id'];
			// $u_id               = $_SESSION['id_nguoidung'];
			$u_name             = $id_cus['fullname'];
			$u_phone            = $id_cus['phone'];
			$u_address          = $id_cus['address'];
			$u_img              = $id_cus['img'];
			$u_email            = $id_cus['email'];
			$u_gender           = $id_cus['gender'];

			$target_dir = SITE_ROOT . "/assets/img/";

			if ($u_phone == '') {
				echo "
				<script>
				alert('Điện thoại không được để trống, vui lòng cập nhập số điện thoại!')
				</script>
				";
			} elseif ($u_name == '') {
				echo "
				<script>
				alert('Họ và tên không được để trống!');
				</script>
				";
			} elseif ($u_address == '') {
				echo "
				<script>
				alert('Địa chỉ không được để trống, vui lòng cập nhật địa chỉ!');
				</script>
				";
			} elseif (isset($id_cus['id'])) {


				if (!empty($u_img)) {

					if (!is_dir("../libraries/images/userCus"))
						mkdir("../libraries/images/userCus", 0777, true);
					if (isset($_FILES["myimage"])) {
						$allowed  = array(
							"jpg"  => "image/jpg",
							"png"  => "image/png",
							"jpeg" => "image/jpeg",
							"gif"  => "image/gif",
						);
						$filename = $_FILES["myimage"]["name"];
						$filetype = $_FILES["myimage"]["type"];
						$filesize = $_FILES["myimage"]["size"];
						$ext      = pathinfo($filename, PATHINFO_EXTENSION);

						if (!array_key_exists($ext, $allowed)) {
							$image_error = "Vui lòng chọn đúng định dạng file";
						}
						if ($filesize > 5242880) {
							$image_error = "Kích thước file lớn hơn giới hạn cho phép";
						}
						if (in_array($filetype, $allowed)) {
							$year       = date('Y');
							$month      = date('m');
							$day        = date('d');
							$sub_folder = '/' . $year . '/' . $month . '/' . $day;
							if (!is_dir("../libraries/images/userCus" . $sub_folder))
								if (!mkdir("../libraries/images/userCus" . $sub_folder, 0777, true))
									return FALSE;
								$upload_url = SITE_ROOT . '/libraries/images/userCus' . $sub_folder;
								$img        = $upload_url . '/' . $filename;
								$image      = 'libraries/images/userCus' . $sub_folder . '/' . $filename;
								move_uploaded_file($_FILES["myimage"]["tmp_name"], $img);
							} else {
								echo "Lỗi : Có vấn đề xảy ra khi upload file";
							}
						}


						move_uploaded_file($_FILES["myimage"]["tmp_name"], $img);

						$cus_id              = $id_cus['id'];
						$update              = new Customer();
						$update_cus          = $update->UpdateCus_byid($cus_id, $u_phone, $u_name, $u_address, $image);
						
						$u_infor['id']       = $cus_id;
						$u_infor['phone']    = $u_phone;
						$u_infor['fullname'] = $u_name;
						$u_infor['address']  = $u_address;
						$u_infor['img']      = $image;
						$u_infor['email']    = $u_email;
						$u_infor['gender']   = $u_gender;
						
						$_SESSION['u_info']  = $u_infor;

						

						echo "
						<script>
						alert('Update information success!');'
						</script>
						";
					}
					if (empty($id_cus['img'])) {

						$img_cus             = $_POST['img_cus'];
						
						$cus_id              = $id_cus['id'];
						$update              = new Customer();
						$update_cus          = $update->UpdateCus_none($cus_id, $u_phone, $u_name, $u_address);
						
						$u_infor['id']       = $cus_id;
						$u_infor['phone']    = $u_phone;
						$u_infor['fullname'] = $u_name;
						$u_infor['address']  = $u_address;
						$u_infor['email']    = $u_email;
						$u_infor['gender']   = $u_gender;
						$u_infor['img']      = $img_cus;
						
						$_SESSION['u_info']  = $u_infor;


						echo "
						<script>
						alert('Update information success!');'
						</script>
						";
					}

				} else {
					echo "
					<script>
					alert('Update fail, Please check again!')
					</script>
					";
				}
			}
		}
	}

	include('views/customers/infor_cus_view.php');
	?>