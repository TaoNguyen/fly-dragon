<?php
if (isset($_POST['register_btn'])) {
	$name       = $_POST['full_name'];
	$gender     = $_POST['gender'];
	$email      = $_POST['email'];
	$phone      = $_POST['phone'];
	$address    = $_POST['address'];
	$password   = $_POST['password'];
	$repassword = $_POST['repassword'];
	
	$role       = 0;
	$facebook   = '01245484146';
	
	$img        = '../libraries/images/avatar.png';
	// $img        = 'library/userCus/default/avatar.png';
	
	$created_at = (new DateTime('now'))->format('Y-m-d');
	$updated_at = '';    

    // check some element
	$check_name   = "/^[a-zA-Z ]+$/";
	$check_email  = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9]+(\.[a-z]{2,4})$/";
	$check_number = "/^[0-9]+$/";

	if (isset($email)) {
		$status   = 0;
	} 
	if (!isset($email)) {
		$status   = 1;
	}elseif (empty($name)) {
		echo "
		<script>
		alert('Vui lòng nhập họ tên!');window.location='../index.php'
		</script>
		";
		exit();
	} elseif (empty($gender)) {
		echo "
		<script>
		alert('Vui lòng không để trống trường giới tính!');window.location='../index.php'
		</script>
		";
		exit();
	} elseif (empty($email)) {
		echo "
		<script>
		alert('Vui lòng nhập email!');window.location='../index.php'
		</script>
		";
		exit();
	} elseif (empty($phone)) {
		echo "
		<script>
		alert('Vui lòng nhập số điện thoại!');window.location='../index.php'
		</script>
		";
		exit();
	} elseif (empty($address)) {
		echo "
		<script>
		alert('Vui lòng nhập địa chỉ!');window.location='../index.php'
		</script>
		";
		exit();
	} elseif (empty($password)) {
		echo "
		<script>
		alert('Mật khẩu không được để trống, vui lòng nhập mật khẩu!');window.location='../index.php'
		</script>
		";
		exit();
	} else {
		if (!preg_match($check_email, $email)) {
			echo "
			<script>
			alert('Email không hợp lệ, vui lòng kiểm tra lại!');window.location='../index.php'
			</script>
			";
			exit();
		}
		if (strlen($password) < 8) {
			echo "
			<script>
			alert('Mật khẩu chưa đủ mạnh!');window.location='../index.php'
			</script>
			";
			exit();
		}
		if ($password != $repassword) {
			echo "
			<script>
			alert('Mật khẩu không trùng khớp!');window.location='../index.php'
			</script>
			";
			exit();
		}
		if (!preg_match($check_number, $phone)) {
			echo "
			<script>
			alert('Số điện thoại $phone không hợp lệ!');window.location='../index.php'
			</script>
			";
			exit();
		}
		if ((strlen($phone) < 10) || (strlen($phone) > 11)) {
			echo "
			<script>
			alert('Số điện thoại $phone >= 10 hoăc <= 11 ký tự!');window.location='../index.php'
			</script>
			";
			exit();
		} else {
			$password = md5($_POST["password"]);
			$password = ($password);
			if ($gender == 1) {
				$gender2 = "Nam";

				$add_user = new Customer();
				$user_id  = $add_user->AddUser($email, $role, $password, $facebook, $status); 

				$add_infor = new Customer();
				$infor     = $add_infor->AddInfor($phone, $gender2, $name, $address, $user_id, $img);
			} if ($gender == 2 ) {
				$gender2 = 'Nữ';
				$add_user = new Customer();
				$user_id  = $add_user->AddUser($email, $role, $password, $facebook, $status); 
				
				$add_infor = new Customer();
				$infor     = $add_infor->AddInfor($phone, $gender2, $name, $address, $user_id, $img);
			}


			echo "
			<script>
			alert('Đăng ký thành công!');window.location='../index.php?controller=home'
			</script>
			";
		}
	}
}
include('views/customers/register_view.php');
?>