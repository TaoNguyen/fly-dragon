<?php
if (isset($_SESSION['u_info'])) {

	$idcus   = $_SESSION['u_info'];
	$user_id = $idcus['id'];
	$email   = $idcus['email'];
	// $id_cus  = (int) implode('', $user_id); // chuyen doi kieu mang sang kieu int
	if (isset($_POST['change_pass_btn'])) {
		$old_pass     = $_POST['old_pass'];
		$new_pass     = $_POST['new_pass'];
		$confirm_pass = $_POST['confirm_pass'];

		if (!empty($old_pass) && !empty($new_pass) && !empty($confirm_pass)) {

			$check      = new Customer();
			$check_pass = $check->checkChangePass($user_id, $old_pass);

			if ($check_pass->num_rows =1 ) {
				
				if ($new_pass === $confirm_pass) {
					$update       = new Customer();
					$update_pass  = $update->updatePass($user_id, $new_pass, $email);

					echo "
					<script>
					alert('Thay đổi mật khẩu thành công, Vui lòng đăng nhập lại');window.location='index.php?controller=customer&action=logout'
					</script>
					";
					die;
				} else {
					echo "
					<script>
					alert('Mật khẩu không trùng khớp, Vui lòng nhập lại');window.location='../index.php'
					</script>
					";
					die;
				}
			} else {
				echo "
				<script>
				alert('Vui lòng nhập đúng mật khẩu củ');window.location='../index.php'
				</script>
				";
				die;
			}
		} else {
			echo "
			<script>
			alert('Các trường không được trống!');window.location='../index.php'
			</script>
			";die;
		}

	}
} 
?>