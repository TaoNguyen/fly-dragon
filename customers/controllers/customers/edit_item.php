<?php 

$model = new Customer();

if (isset($_SESSION['u_info'])) {
	if (isset($_GET['id'])) {
		$post_id = $_GET['id'];

		$edit = new customer();
		$result   = $edit->getPostById($post_id);
		$countImg = $edit->getImg($post_id);
		$row = $result->fetch_assoc();
		$parent_cat=$edit->getId_categories($row['category_id']);
		$cat_parent=$parent_cat->fetch_assoc();
	}
}

if (isset($_POST['update'])) {
	$_POST['id'] = $_GET['id'];
	if (isset($_POST['id'])) {
		$post_id = $_GET['id'];
		$name = $_POST['title_p'];
		$city = $_POST['city_p'];
		$district = $_POST['district_p'];
		$address = $_POST['address_p'];
		// $acreage = $_POST['acreage_P'];
		$price = $_POST['price_p'];
		$unit = $_POST['unit_p'];
		$description = $_POST['description_p'];
		// $balcony = $_POST['balcony'];
		// $lay = $_POST['lay'];
		// $license = $_POST['license'];

		if($cat_parent['parent'] != 1 && $cat_parent['parent'] != 2 && $cat_parent['parent'] != 5){
			$lay = "" ;
			$balcony = "" ;
			$license = "" ;
			$acreage = 0 ;
		} else {
			$acreage     = $_POST['acreage_P'];
			$lay         = $_POST['lay'];
			$balcony     = $_POST['balcony'];
			$license     = $_POST['license'];
			if ($acreage == '') {
				echo 'Vui lòng nhập trường Diện Tích!';
				$error = 1;
			} elseif (!is_numeric($acreage)) {
				echo 'Vui lòng nhập số, không nhập chữ cái tại trường Diện Tích!';
				$error = 1;
			}else{
				$acreage = $_POST['acreage_P'];
			}
		}

		if ($name == '') {
			echo "
			<script>
			alert('Vui lòng nhập tên bài đăng!')
			</script>
			";
		} 
	 elseif (!is_numeric($acreage)) {
			echo "
			<script>
			alert('Vui lòng nhập số $acreage, không nhập chữ cái tại trường Diện Tích!')
			</script>
			";
		} 
		if ($city == '') {
			echo "
			<script>
			alert('Vui lòng chọn trường Thành Phố!')
			</script>
			";
		} 
		if ($district == '') {
			echo "
			<script>
			alert('Vui lòng nhập trường Quận/Huyện!')
			</script>
			";
		} 
		if ($price == '') {
			echo "
			<script>
			alert('Vui lòng nhập trường Giá!')
			</script>
			";
		} elseif (!is_numeric($price)) {
			echo "
			<script>
			alert('Vui lòng nhập số, không nhập chữ cái tại trường Giá thành!')
			</script>
			";
		} 
		if ($description == '') {
			echo "
			<script>
			alert('Vui lòng nhập trường Mô tả!')
			</script>
			";
		} 
		// if ($lay == '') {
		// 	echo "
		// 	<script>
		// 	alert('Vui lòng nhập trường Hướng Chính!')
		// 	</script>
		// 	";
		// } 
		// if ($balcony == '') {
		// 	echo "
		// 	<script>
		// 	alert('Vui lòng nhập trường Hướng Ban Công!')
		// 	</script>
		// 	";
		// } 
		// if ($license == '') {
		// 	echo "
		// 	<script>
		// 	alert('Vui lòng nhập trường Giấy Phép!')
		// 	</script>
		// 	";
		// } 
		if ($address == '') {
			echo "
			<script>
			alert('Vui lòng nhập trường Địa chỉ!')
			</script>
			";
		} 
		if (isset($_FILES["file"]['name'][0]) && !empty($_FILES["file"]['name'][0])) {
			$groupFile = $_FILES["file"];

			$sub_folder = '/' . date('Y') . '/' . date('m') . '/' . date('d');
			$update = new Customer();
			$result0 = $update->updatePost($post_id, $address, $name, $acreage, $city, $district, $price, $description, $lay, $balcony, $license,$unit);
			// print_r($result0)->fetch_assoc();die;
			$result1 = $update->deleteImg($post_id);

			for ($i = 0; $i < count($groupFile['name']); $i++) { 

				 $duoi = explode('.', $groupFile['name'][$i]); // tách chuỗi khi gặp dấu .
				  $duoi = $duoi[(count($duoi) - 1)]; //lấy ra đuôi file

				  if ($duoi === 'jpg' || $duoi === 'png' || $duoi === 'gif' || $duoi === 'jpeg') {

				  	if (!is_dir("../libraries/images/posts" . $sub_folder)) 
				  		if (!mkdir("../libraries/images/posts" . $sub_folder, 0777, true))
				  			return FALSE;
				  		$upload_url = SITE_ROOT . '/libraries/images/posts' . $sub_folder;

				  		$img = $upload_url . '/' . $groupFile['name'][$i];
				  		$image = 'libraries/images/posts' . $sub_folder . '/' . $groupFile['name'][$i];
				  		$check = move_uploaded_file($groupFile['tmp_name'][$i], $img);
				  
				  		if ($check) {
				  			$result3 = $update->addImg($post_id, $image);

				  		} else {
				  			echo 0;
				  			die;

				  		} 
				  	}
				  } 

				} else { 
					$results = $model->updatePost($post_id, $address, $name, $acreage, $city, $district, $price, $description, $lay, $balcony, $license, $unit);
				} 
			} 
		}





		
		$list_province = $model->getListProvince();
		$list_district = $model->getListDistrict($row['city_id']); 

		include('views/customers/editPost_view.php');
		?>