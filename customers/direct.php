<?php 
include('config/connect.php');
include('../libraries/__autoload.php');

if (isset($_GET['controller'])) {
	switch ($_GET['controller']) {
		case 'home': include('controllers/home/controller.php');
		break;
		case 'customer': include('controllers/customers/controller.php');
		break;
	}
} else {
	header('location:index.php?controller=home&action=home');
}

?>