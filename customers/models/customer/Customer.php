<?php 
class Customer
{
	public $conn;
	public function __construct()
	{
		$conn = $GLOBALS['conn'];
	}

// login 
	public function LoginCus($email, $password)
	{
		$sql = "SELECT users.id,users.email,user_infos.phone,users.password,user_infos.gender,user_infos.fullname,user_infos.address,user_infos.id as info_id,user_infos.img  FROM users JOIN user_infos ON users.id=user_infos.user_id WHERE users.email = '$email' AND users.password = '".md5($password)."' LIMIT 1";
		$login = $GLOBALS['conn']->query($sql);
		$result=$login->fetch_assoc();
		// $result['password']='';
		return $result;
	}

// add user in register from
	public function AddUser($email, $role, $password, $facebook, $status)
	{
		$sql = "INSERT INTO `users` (`id`, `email`, `role`, `password`, `facebook`, `status`) VALUES (NULL, '$email', '$role', '$password', '$facebook', '$status')";
		$adduser = $GLOBALS['conn']->query($sql);
		if($adduser){
			$user_id=$GLOBALS['conn']->insert_id;
			return $user_id;
		}else{
			return false;
		}
	}

// add imformation form register from
	public function AddInfor($phone, $gender, $name, $address, $user_id, $img)
	{
		$sql = "INSERT INTO user_infos (phone, gender, fullname, address, user_id, img, created_at) VALUES ('$phone', '$gender', '$name', '$address', $user_id, '$img', now())";
		$addinfor = $GLOBALS['conn']->query($sql);
		return $addinfor;
	}

// list and update information of user
	public function getCus_byid($id_cus)
	{
		$sql = "SELECT user_id FROM users AS us JOIN user_infos AS us_info ON us.id = us_info.user_id WHERE us.id = $id_cus";
		$show = $GLOBALS['conn']->query($sql);
		return $show;	
	}
	// update with image
	public function UpdateCus_byid($cus_id, $u_phone, $u_name, $u_address, $image)
	{
		$sql = "UPDATE user_infos SET phone ='$u_phone', fullname = ' $u_name', address = '$u_address', img = '$image', updated_at = now() WHERE id = $cus_id";
		// echo $sql; die;
		$update = $GLOBALS['conn']->query($sql);
		return $update;
	}
	// update none image
	public function UpdateCus_none($cus_id, $u_phone, $u_name, $u_address)
	{
		$sql = "UPDATE user_infos SET phone ='$u_phone', fullname = '$u_name', address = '$u_address', updated_at = now() WHERE id = $cus_id";
		$update_none_img = $GLOBALS['conn']->query($sql);
		return $update_none_img;
	}

// check email validate register
	public function check_email($email='')
	{
		if($email!=''){
			$sql="SELECT * FROM users WHERE email='$email' LIMIT 1";
			$result=$GLOBALS['conn']->query($sql);
			if($result->num_rows==0){
				return 'true';
			}else{
				return 'false';
			}
		}else{
			return 'false';
		}
	}

// change password
	public function checkChangePass($user_id, $password)
	{
		$sql = "SELECT * FROM users WHERE password = '".md5($password)."' WHERE id=$user_id";
		$redirect = $GLOBALS['conn']->query($sql);
		return $redirect;
	}

	public function updatePass($user_id, $password, $email) {
		$sql = "UPDATE users SET password =  '".md5($password)."' WHERE id = $user_id";
		$update_pass = $GLOBALS['conn']->query($sql);
		return $update_pass;
	}

// Post history
	public function history($user_id=0)
	{
		$sql = "
		SELECT  posts.id, posts.title, posts.created_at , provinces.name AS province_name, districts.name AS district_name, posts.acreage, posts.price, posts.description, images.image AS post_img, post_contacts.user_id, posts.unit
		FROM posts 
		INNER JOIN provinces ON posts.city_id     = provinces.id
		INNER JOIN districts ON posts.district_id = districts.id
		INNER JOIN images ON posts.id             = images.post_id
		INNER JOIN post_contacts ON posts.id      = post_contacts.post_id
		WHERE post_contacts.user_id = $user_id
		GROUP BY id
		ORDER BY id DESC
		LIMIT 5";
		$history1 = $GLOBALS['conn']->query($sql);

		return $history1;

	}

// Delete item
	public function DelCotacts($post_id=0)
	{
		$sql = "DELETE FROM post_contacts WHERE post_id = $post_id";
		$delelte = $GLOBALS['conn']->query($sql);
		return $delelte;
	}

	public function DelImage($post_id)
	{
		$sql = "DELETE FROM images WHERE post_id = $post_id";
		$deleteImg = $GLOBALS['conn']->query($sql);
		return $deleteImg;
	}

	public function DelPost($post_id)
	{
		$sql = "DELETE FROM posts WHERE id = $post_id";
		$deletePost = $GLOBALS['conn']->query($sql);
		return $deletePost;
	}

// show news by id cus or id post
	public function editNews_byId($id_post)
	{
		$sql = "
		SELECT posts.id, posts.title, provinces.name AS province_name, districts.name AS district_name, posts.address, posts.price, posts.unit, posts.acreage, 
		posts.description, images.image AS post_img, posts.lay, posts.balcony, posts.license, categories.category_name
		FROM posts
		INNER JOIN provinces ON posts.city_id = provinces.id
		INNER JOIN districts ON posts.district_id = districts.id
		INNER JOIN images ON posts.id = images.post_id
		INNER JOIN categories ON posts.category_id = categories.id
		INNER JOIN post_contacts ON posts.id = post_contacts.post_id

		WHERE posts.id = $id_post

		";
		$result = $GLOBALS['conn']->query($sql);
		return $result;

	}

// show img
	public function getImg($post_id) {
		$sql = "SELECT image FROM images WHERE post_id = $post_id";
		$result = $GLOBALS['conn']->query($sql);
		return $result;
	}

// Get id Post
	public function getPostById($post_ID) {
		$sql = "
		SELECT * FROM posts AS p 
		LEFT JOIN images i ON p.id = i.post_id 
		LEFT JOIN provinces as pro ON p.city_id = pro.id 
		WHERE p.id = $post_ID";
		$result = $GLOBALS['conn']->query($sql);
		return $result;
	}

// Save the information after editing
	public function updatePost($post_id, $address, $name, $acreage, $city, $district, $price, $description, $lay, $balcony, $license, $unit) {
		$sql = "
		UPDATE posts 
		SET title = '$name', acreage = '$acreage', city_id ='$city', district_id = '$district', address = '$address', price = '$price', 
		unit = '$unit', description = '$description', lay = '$lay', balcony = '$balcony', license ='$license'
		WHERE id = $post_id";
		$result = $GLOBALS['conn']->query($sql);
		return $result;
	}

// Save image after editing
	public function addImg($post_ID, $image) {
		$sql = "INSERT INTO images (image, post_id)
		VALUES ('$image', $post_ID)";
		$result = $GLOBALS['conn']->query($sql);
		return $result;
	}

// Show some information before edit news
	public function getAPost($post_ID) {
		$sql = "
		SELECT p.id,p.title,p.unit,p.description,p.acreage,p.address,p.price,p.lay,p.balcony,p.license,p.created_at AS date,c.category_name,p_contact.email,p_contact.fullname,p_contact.phone,pro.name AS pname,dis.name AS disname 
		FROM posts AS p 
		LEFT JOIN categories AS c ON p.category_id = c.id 
		LEFT JOIN post_contacts as p_contact ON p.id = p_contact.post_id 
		LEFT JOIN images i ON p.id = i.post_id 
		LEFT JOIN provinces as pro ON p.city_id = pro.id 
		LEFT JOIN districts as dis ON p.district_id = dis.id 
		WHERE p.id = $post_ID";
		$result = $GLOBALS['conn']->query($sql);
		return $result;
	}

// List provinces
	public function getListProvince() {
		$sql = 'SELECT id,name FROM provinces ORDER BY name ASC';
		$result = $GLOBALS['conn']->query($sql);
		return $result;
	}

// load district
	public function getDistrict($id) {
		$sql = "SELECT id, name FROM districts WHERE province_id = $id ORDER BY name ASC";
		$result = $GLOBALS['conn']->query($sql);
		return $result;
	}

// List districts
	public function getListDistrict($province_id) {
		$sql = "SELECT id, name FROM districts WHERE id = $province_id ORDER BY name ASC";
		$result = $GLOBALS['conn']->query($sql);
		return $result;
	}

// delete Img
	public function deleteImg($post_ID) {
		$sql = "DELETE FROM images WHERE post_id = $post_ID";
		$result = $GLOBALS['conn']->query($sql);
		return $result;
	}

// Show id category 
	public function getId_categories($cat) {
		$sql = "SELECT * FROM categories WHERE id=$cat";
		$result=$GLOBALS['conn']->query($sql);
		return $result;

	}
}
?>
