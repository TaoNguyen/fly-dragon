-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th7 09, 2018 lúc 07:57 AM
-- Phiên bản máy phục vụ: 10.1.32-MariaDB
-- Phiên bản PHP: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `rongbay`
DROP DATABASE rongbay;
CREATE DATABASE rongbay;
USE rongbay;	
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `category_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL,
  `img` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `parent`, `img`) VALUES
(1, 'Mua bán nhà đất', 0, 'admin/images/111111111111111111111.jpg'),
(2, 'Mua bán chung cư', 1, 'admin/images/nhadat.jpg'),
(3, 'Cho thuê phòng trọ', 1, 'admin/images/2.jpg'),
(4, 'Xe', 0, 'admin/images/6.jpg'),
(5, 'Mua bán xe máy cũ', 4, 'admin/images/3.jpg'),
(6, 'Mua bán xe ô tô 7 chỗ', 4, 'admin/images/3.jpg'),
(7, 'Cho thuê chung cư', 1, 'admin/images/Capture.PNG'),
(8, 'Cho thuê nhà nguyên căn', 1, 'admin/images/8.jpg'),
(9, 'Cho mượn xe máy giá rẻ', 4, 'admin/images/28279794_1745069702222760_4697422490597588992_n.jpg'),
(10, 'Điện thoại', 0, 'admin/images/5.jpg'),
(11, 'Thời Trang Nam', 0, 'admin/images/Softech.jpg'),
(12, 'Thời Trang Nữ', 0, 'admin/images/nhadat.jpg'),
(13, 'Đồ Dùng ĐIện', 0, 'admin/images/nhadat.jpg');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `districts`
--

CREATE TABLE `districts` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `province_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `districts`
--

INSERT INTO `districts` (`id`, `name`, `location`, `province_id`, `created_at`, `updated_at`) VALUES
(1, 'Ba Đình', '21 02 08N, 105 49 38E', 1, NULL, NULL),
(2, 'Hoàn Kiếm', '21 01 53N, 105 51 09E', 1, NULL, NULL),
(3, 'Tây Hồ', '21 04 10N, 105 49 07E', 1, NULL, NULL),
(4, 'Long Biên', '21 02 21N, 105 53 07E', 1, NULL, NULL),
(5, 'Cầu Giấy', '21 01 52N, 105 47 20E', 1, NULL, NULL),
(6, 'Đống Đa', '21 00 56N, 105 49 06E', 1, NULL, NULL),
(7, 'Hai Bà Trưng', '21 00 27N, 105 51 35E', 1, NULL, NULL),
(8, 'Hoàng Mai', '20 58 33N, 105 51 22E', 1, NULL, NULL),
(9, 'Thanh Xuân', '20 59 44N, 105 48 56E', 1, NULL, NULL),
(16, 'Sóc Sơn', '21 16 55N, 105 49 46E', 1, NULL, NULL),
(17, 'Đông Anh', '21 08 16N, 105 49 38E', 1, NULL, NULL),
(18, 'Gia Lâm', '21 01 28N, 105 56 54E', 1, NULL, NULL),
(19, 'Từ Liêm', '21 02 39N, 105 45 32E', 1, NULL, NULL),
(20, 'Thanh Trì', '20 56 32N, 105 50 55E', 1, NULL, NULL),
(24, 'Hà Giang', '22 46 23N, 105 02 39E', 2, NULL, NULL),
(26, 'Đồng Văn', '23 14 34N, 105 15 48E', 2, NULL, NULL),
(27, 'Mèo Vạc', '23 09 10N, 105 26 38E', 2, NULL, NULL),
(28, 'Yên Minh', '23 04 20N, 105 10 13E', 2, NULL, NULL),
(29, 'Quản Bạ', '23 04 03N, 104 58 05E', 2, NULL, NULL),
(30, 'Vị Xuyên', '22 45 50N, 104 56 26E', 2, NULL, NULL),
(31, 'Bắc Mê', '22 45 48N, 105 16 26E', 2, NULL, NULL),
(32, 'Hoàng Su Phì', '22 41 37N, 104 40 06E', 2, NULL, NULL),
(33, 'Xín Mần', '22 38 05N, 104 28 35E', 2, NULL, NULL),
(34, 'Bắc Quang', '22 23 42N, 104 55 06E', 2, NULL, NULL),
(35, 'Quang Bình', '22 23 07N, 104 37 11E', 2, NULL, NULL),
(40, 'Cao Bằng', '22 39 20N, 106 15 20E', 4, NULL, NULL),
(42, 'Bảo Lâm', '22 52 37N, 105 27 28E', 4, NULL, NULL),
(43, 'Bảo Lạc', '22 52 31N, 105 42 41E', 4, NULL, NULL),
(44, 'Thông Nông', '22 49 09N, 105 57 05E', 4, NULL, NULL),
(45, 'Hà Quảng', '22 53 42N, 106 06 32E', 4, NULL, NULL),
(46, 'Trà Lĩnh', '22 48 14N, 106 19 47E', 4, NULL, NULL),
(47, 'Trùng Khánh', '22 49 31N, 106 33 58E', 4, NULL, NULL),
(48, 'Hạ Lang', '22 42 37N, 106 41 42E', 4, NULL, NULL),
(49, 'Quảng Uyên', '22 40 15N, 106 27 42E', 4, NULL, NULL),
(50, 'Phục Hoà', '22 33 52N, 106 30 02E', 4, NULL, NULL),
(51, 'Hoà An', '22 41 20N, 106 02 05E', 4, NULL, NULL),
(52, 'Nguyên Bình', '22 38 52N, 105 57 02E', 4, NULL, NULL),
(53, 'Thạch An', '22 28 51N, 106 19 51E', 4, NULL, NULL),
(58, 'Bắc Kạn', '22 08 00N, 105 51 10E', 6, NULL, NULL),
(60, 'Pác Nặm', '22 35 46N, 105 40 25E', 6, NULL, NULL),
(61, 'Ba Bể', '22 23 54N, 105 43 30E', 6, NULL, NULL),
(62, 'Ngân Sơn', '22 25 50N, 106 01 00E', 6, NULL, NULL),
(63, 'Bạch Thông', '22 12 04N, 105 51 01E', 6, NULL, NULL),
(64, 'Chợ Đồn', '22 11 18N, 105 34 43E', 6, NULL, NULL),
(65, 'Chợ Mới', '21 57 56N, 105 51 29E', 6, NULL, NULL),
(66, 'Na Rì', '22 09 48N, 106 05 09E', 6, NULL, NULL),
(70, 'Tuyên Quang', '21 49 40N, 105 13 12E', 8, NULL, NULL),
(72, 'Nà Hang', '22 28 34N, 105 21 03E', 8, NULL, NULL),
(73, 'Chiêm Hóa', '22 12 49N, 105 14 32E', 8, NULL, NULL),
(74, 'Hàm Yên', '22 05 46N, 105 00 13E', 8, NULL, NULL),
(75, 'Yên Sơn', '21 51 53N, 105 18 14E', 8, NULL, NULL),
(76, 'Sơn Dương', '21 40 22N, 105 22 57E', 8, NULL, NULL),
(80, 'Lào Cai', '22 25 07N, 103 58 43E', 10, NULL, NULL),
(82, 'Bát Xát', '22 35 50N, 103 44 49E', 10, NULL, NULL),
(83, 'Mường Khương', '22 41 05N, 104 08 26E', 10, NULL, NULL),
(84, 'Si Ma Cai', '22 39 46N, 104 16 05E', 10, NULL, NULL),
(85, 'Bắc Hà', '22 30 08N, 104 18 54E', 10, NULL, NULL),
(86, 'Bảo Thắng', '22 22 47N, 104 10 00E', 10, NULL, NULL),
(87, 'Bảo Yên', '22 17 38N, 104 25 02E', 10, NULL, NULL),
(88, 'Sa Pa', '22 18 54N, 103 54 18E', 10, NULL, NULL),
(89, 'Văn Bàn', '22 03 48N, 104 10 59E', 10, NULL, NULL),
(94, 'Điện Biên Phủ', '21 24 52N, 103 02 31E', 11, NULL, NULL),
(95, 'Mường Lay', '22 01 47N, 103 07 10E', 11, NULL, NULL),
(96, 'Mường Nhé', '22 06 11N, 102 30 54E', 11, NULL, NULL),
(97, 'Mường Chà', '21 50 31N, 103 03 15E', 11, NULL, NULL),
(98, 'Tủa Chùa', '21 58 19N, 103 23 01E', 11, NULL, NULL),
(99, 'Tuần Giáo', '21 38 03N, 103 21 06E', 11, NULL, NULL),
(100, 'Điện Biên', '21 15 19N, 103 03 19E', 11, NULL, NULL),
(101, 'Điện Biên Đông', '21 14 07N, 103 15 19E', 11, NULL, NULL),
(102, 'Mường Ảng', '', 11, NULL, NULL),
(104, 'Lai Châu', '22 23 15N, 103 24 22E', 12, NULL, NULL),
(106, 'Tam Đường', '22 20 16N, 103 32 53E', 12, NULL, NULL),
(107, 'Mường Tè', '22 24 16N, 102 43 11E', 12, NULL, NULL),
(108, 'Sìn Hồ', '22 17 21N, 103 18 12E', 12, NULL, NULL),
(109, 'Phong Thổ', '22 38 24N, 103 22 38E', 12, NULL, NULL),
(110, 'Than Uyên', '21 59 35N, 103 45 30E', 12, NULL, NULL),
(111, 'Tân Uyên', '', 12, NULL, NULL),
(116, 'Sơn La', '21 20 39N, 103 54 52E', 14, NULL, NULL),
(118, 'Quỳnh Nhai', '21 46 34N, 103 39 02E', 14, NULL, NULL),
(119, 'Thuận Châu', '21 24 54N, 103 39 46E', 14, NULL, NULL),
(120, 'Mường La', '21 31 38N, 104 02 48E', 14, NULL, NULL),
(121, 'Bắc Yên', '21 13 23N, 104 22 09E', 14, NULL, NULL),
(122, 'Phù Yên', '21 13 33N, 104 41 51E', 14, NULL, NULL),
(123, 'Mộc Châu', '20 49 21N, 104 43 10E', 14, NULL, NULL),
(124, 'Yên Châu', '20 59 33N, 104 19 51E', 14, NULL, NULL),
(125, 'Mai Sơn', '21 10 08N, 103 59 36E', 14, NULL, NULL),
(126, 'Sông Mã', '21 06 04N, 103 43 56E', 14, NULL, NULL),
(127, 'Sốp Cộp', '20 52 46N, 103 30 38E', 14, NULL, NULL),
(132, 'Yên Bái', '21 44 28N, 104 53 42E', 15, NULL, NULL),
(133, 'Nghĩa Lộ', '21 35 58N, 104 29 22E', 15, NULL, NULL),
(135, 'Lục Yên', '22 06 44N, 104 43 12E', 15, NULL, NULL),
(136, 'Văn Yên', '21 55 55N, 104 33 51E', 15, NULL, NULL),
(137, 'Mù Cang Chải', '21 48 22N, 104 09 01E', 15, NULL, NULL),
(138, 'Trấn Yên', '21 42 20N, 104 48 56E', 15, NULL, NULL),
(139, 'Trạm Tấu', '21 30 40N, 104 28 03E', 15, NULL, NULL),
(140, 'Văn Chấn', '21 34 15N, 104 35 19E', 15, NULL, NULL),
(141, 'Yên Bình', '21 52 24N, 104 55 16E', 15, NULL, NULL),
(148, 'Hòa Bình', '20 50 36N, 105 19 20E', 17, NULL, NULL),
(150, 'Đà Bắc', '20 55 58N, 105 04 52E', 17, NULL, NULL),
(151, 'Kỳ Sơn', '20 54 06N, 105 23 18E', 17, NULL, NULL),
(152, 'Lương Sơn', '20 53 16N, 105 30 55E', 17, NULL, NULL),
(153, 'Kim Bôi', '20 40 34N, 105 32 15E', 17, NULL, NULL),
(154, 'Cao Phong', '20 41 23N, 105 17 48E', 17, NULL, NULL),
(155, 'Tân Lạc', '20 36 44N, 105 15 03E', 17, NULL, NULL),
(156, 'Mai Châu', '20 40 56N, 104 59 46E', 17, NULL, NULL),
(157, 'Lạc Sơn', '20 29 59N, 105 24 57E', 17, NULL, NULL),
(158, 'Yên Thủy', '20 25 42N, 105 37 59E', 17, NULL, NULL),
(159, 'Lạc Thủy', '20 29 12N, 105 44 06E', 17, NULL, NULL),
(164, 'Thái Nguyên', '21 33 20N, 105 48 26E', 19, NULL, NULL),
(165, 'Sông Công', '21 29 14N, 105 48 47E', 19, NULL, NULL),
(167, 'Định Hóa', '21 53 50N, 105 38 01E', 19, NULL, NULL),
(168, 'Phú Lương', '21 45 57N, 105 43 22E', 19, NULL, NULL),
(169, 'Đồng Hỷ', '21 41 10N, 105 55 43E', 19, NULL, NULL),
(170, 'Võ Nhai', '21 47 14N, 106 02 29E', 19, NULL, NULL),
(171, 'Đại Từ', '21 36 17N, 105 37 28E', 19, NULL, NULL),
(172, 'Phổ Yên', '21 27 08N, 105 45 19E', 19, NULL, NULL),
(173, 'Phú Bình', '21 29 36N, 105 57 42E', 19, NULL, NULL),
(178, 'Lạng Sơn', '21 51 19N, 106 44 50E', 20, NULL, NULL),
(180, 'Tràng Định', '22 18 09N, 106 26 32E', 20, NULL, NULL),
(181, 'Bình Gia', '22 03 56N, 106 19 01E', 20, NULL, NULL),
(182, 'Văn Lãng', '22 01 59N, 106 34 17E', 20, NULL, NULL),
(183, 'Cao Lộc', '21 53 05N, 106 50 34E', 20, NULL, NULL),
(184, 'Văn Quan', '21 51 04N, 106 33 04E', 20, NULL, NULL),
(185, 'Bắc Sơn', '21 48 57N, 106 15 28E', 20, NULL, NULL),
(186, 'Hữu Lũng', '21 34 33N, 106 20 33E', 20, NULL, NULL),
(187, 'Chi Lăng', '21 40 05N, 106 37 24E', 20, NULL, NULL),
(188, 'Lộc Bình', '21 40 41N, 106 58 12E', 20, NULL, NULL),
(189, 'Đình Lập', '21 32 07N, 107 07 25E', 20, NULL, NULL),
(193, 'Hạ Long', '20 52 24N, 107 05 23E', 22, NULL, NULL),
(194, 'Móng Cái', '21 26 31N, 107 55 09E', 22, NULL, NULL),
(195, 'Cẩm Phả', '21 03 42N, 107 17 22E', 22, NULL, NULL),
(196, 'Uông Bí', '21 04 33N, 106 45 07E', 22, NULL, NULL),
(198, 'Bình Liêu', '21 33 07N, 107 26 24E', 22, NULL, NULL),
(199, 'Tiên Yên', '21 22 24N, 107 22 50E', 22, NULL, NULL),
(200, 'Đầm Hà', '21 21 23N, 107 34 32E', 22, NULL, NULL),
(201, 'Hải Hà', '21 25 50N, 107 41 26E', 22, NULL, NULL),
(202, 'Ba Chẽ', '21 15 40N, 107 09 52E', 22, NULL, NULL),
(203, 'Vân Đồn', '20 56 17N, 107 28 02E', 22, NULL, NULL),
(204, 'Hoành Bồ', '21 06 30N, 107 02 43E', 22, NULL, NULL),
(205, 'Đông Triều', '21 07 10N, 106 34 36E', 22, NULL, NULL),
(206, 'Yên Hưng', '20 55 40N, 106 51 05E', 22, NULL, NULL),
(207, 'Cô Tô', '21 05 01N, 107 49 10E', 22, NULL, NULL),
(213, 'Bắc Giang', '21 17 36N, 106 11 18E', 24, NULL, NULL),
(215, 'Yên Thế', '21 31 29N, 106 09 27E', 24, NULL, NULL),
(216, 'Tân Yên', '21 23 23N, 106 05 55E', 24, NULL, NULL),
(217, 'Lạng Giang', '21 21 58N, 106 15 21E', 24, NULL, NULL),
(218, 'Lục Nam', '21 18 16N, 106 29 24E', 24, NULL, NULL),
(219, 'Lục Ngạn', '21 26 15N, 106 39 09E', 24, NULL, NULL),
(220, 'Sơn Động', '21 19 42N, 106 51 09E', 24, NULL, NULL),
(221, 'Yên Dũng', '21 12 22N, 106 14 12E', 24, NULL, NULL),
(222, 'Việt Yên', '21 16 16N, 106 04 59E', 24, NULL, NULL),
(223, 'Hiệp Hòa', '21 15 51N, 105 57 30E', 24, NULL, NULL),
(227, 'Việt Trì', '21 19 01N, 105 23 35E', 25, NULL, NULL),
(228, 'Phú Thọ', '21 24 54N, 105 13 46E', 25, NULL, NULL),
(230, 'Đoan Hùng', '21 36 56N, 105 08 42E', 25, NULL, NULL),
(231, 'Hạ Hoà', '21 35 13N, 105 00 22E', 25, NULL, NULL),
(232, 'Thanh Ba', '21 27 04N, 105 09 10E', 25, NULL, NULL),
(233, 'Phù Ninh', '21 26 59N, 105 18 13E', 25, NULL, NULL),
(234, 'Yên Lập', '21 22 21N, 105 01 25E', 25, NULL, NULL),
(235, 'Cẩm Khê', '21 23 04N, 105 05 25E', 25, NULL, NULL),
(236, 'Tam Nông', '21 18 24N, 105 14 59E', 25, NULL, NULL),
(237, 'Lâm Thao', '21 19 37N, 105 18 09E', 25, NULL, NULL),
(238, 'Thanh Sơn', '21 08 32N, 105 04 40E', 25, NULL, NULL),
(239, 'Thanh Thuỷ', '21 07 26N, 105 17 18E', 25, NULL, NULL),
(240, 'Tân Sơn', '', 25, NULL, NULL),
(243, 'Vĩnh Yên', '21 18 26N, 105 35 33E', 26, NULL, NULL),
(244, 'Phúc Yên', '21 18 55N, 105 43 54E', 26, NULL, NULL),
(246, 'Lập Thạch', '21 24 45N, 105 25 39E', 26, NULL, NULL),
(247, 'Tam Dương', '21 21 40N, 105 33 36E', 26, NULL, NULL),
(248, 'Tam Đảo', '21 27 34N, 105 35 09E', 26, NULL, NULL),
(249, 'Bình Xuyên', '21 19 48N, 105 39 43E', 26, NULL, NULL),
(250, 'Mê Linh', '21 10 53N, 105 42 05E', 1, NULL, NULL),
(251, 'Yên Lạc', '21 13 17N, 105 34 44E', 26, NULL, NULL),
(252, 'Vĩnh Tường', '21 14 58N, 105 29 37E', 26, NULL, NULL),
(253, 'Sông Lô', '', 26, NULL, NULL),
(256, 'Bắc Ninh', '21 10 48N, 106 03 58E', 27, NULL, NULL),
(258, 'Yên Phong', '21 12 40N, 105 59 36E', 27, NULL, NULL),
(259, 'Quế Võ', '21 08 44N, 106 11 06E', 27, NULL, NULL),
(260, 'Tiên Du', '21 07 37N, 106 02 19E', 27, NULL, NULL),
(261, 'Từ Sơn', '21 07 12N, 105 57 26E', 27, NULL, NULL),
(262, 'Thuận Thành', '21 02 24N, 106 04 10E', 27, NULL, NULL),
(263, 'Gia Bình', '21 03 55N, 106 12 53E', 27, NULL, NULL),
(264, 'Lương Tài', '21 01 33N, 106 13 28E', 27, NULL, NULL),
(268, 'Hà Đông', '20 57 25N, 105 45 21E', 1, NULL, NULL),
(269, 'Sơn Tây', '21 05 51N, 105 28 27E', 1, NULL, NULL),
(271, 'Ba Vì', '21 09 40N, 105 22 43E', 1, NULL, NULL),
(272, 'Phúc Thọ', '21 06 32N, 105 34 52E', 1, NULL, NULL),
(273, 'Đan Phượng', '21 07 13N, 105 40 49E', 1, NULL, NULL),
(274, 'Hoài Đức', '21 01 25N, 105 42 03E', 1, NULL, NULL),
(275, 'Quốc Oai', '20 58 45N, 105 36 43E', 1, NULL, NULL),
(276, 'Thạch Thất', '21 02 17N, 105 33 05E', 1, NULL, NULL),
(277, 'Chương Mỹ', '20 52 46N, 105 39 01E', 1, NULL, NULL),
(278, 'Thanh Oai', '20 51 44N, 105 46 18E', 1, NULL, NULL),
(279, 'Thường Tín', '20 49 59N, 105 22 19E', 1, NULL, NULL),
(280, 'Phú Xuyên', '20 43 37N, 105 53 43E', 1, NULL, NULL),
(281, 'Ứng Hòa', '20 42 41N, 105 47 58E', 1, NULL, NULL),
(282, 'Mỹ Đức', '20 41 53N, 105 43 31E', 1, NULL, NULL),
(288, 'Hải Dương', '20 56 14N, 106 18 21E', 30, NULL, NULL),
(290, 'Chí Linh', '21 07 47N, 106 23 46E', 30, NULL, NULL),
(291, 'Nam Sách', '21 00 15N, 106 20 26E', 30, NULL, NULL),
(292, 'Kinh Môn', '21 00 04N, 106 30 23E', 30, NULL, NULL),
(293, 'Kim Thành', '20 55 40N, 106 30 33E', 30, NULL, NULL),
(294, 'Thanh Hà', '20 53 19N, 106 25 43E', 30, NULL, NULL),
(295, 'Cẩm Giàng', '20 57 05N, 106 12 29E', 30, NULL, NULL),
(296, 'Bình Giang', '20 52 36N, 106 11 24E', 30, NULL, NULL),
(297, 'Gia Lộc', '20 51 01N, 106 17 34E', 30, NULL, NULL),
(298, 'Tứ Kỳ', '20 49 05N, 106 24 05E', 30, NULL, NULL),
(299, 'Ninh Giang', '20 45 13N, 106 20 09E', 30, NULL, NULL),
(300, 'Thanh Miện', '20 46 02N, 106 12 26E', 30, NULL, NULL),
(303, 'Hồng Bàng', '20 52 37N, 106 38 32E', 31, NULL, NULL),
(304, 'Ngô Quyền', '20 51 13N, 106 41 57E', 31, NULL, NULL),
(305, 'Lê Chân', '20 50 12N, 106 40 30E', 31, NULL, NULL),
(306, 'Hải An', '20 49 38N, 106 45 57E', 31, NULL, NULL),
(307, 'Kiến An', '20 48 26N, 106 38 03E', 31, NULL, NULL),
(308, 'Đồ Sơn', '20 42 41N, 106 44 54E', 31, NULL, NULL),
(309, 'Kinh Dương', '', 31, NULL, NULL),
(311, 'Thuỷ Nguyên', '20 56 36N, 106 39 38E', 31, NULL, NULL),
(312, 'An Dương', '20 53 06N, 106 35 36E', 31, NULL, NULL),
(313, 'An Lão', '20 47 54N, 106 33 19E', 31, NULL, NULL),
(314, 'Kiến Thụy', '20 45 13N, 106 41 47E', 31, NULL, NULL),
(315, 'Tiên Lãng', '20 42 23N, 106 36 03E', 31, NULL, NULL),
(316, 'Vĩnh Bảo', '20 40 56N, 106 29 57E', 31, NULL, NULL),
(317, 'Cát Hải', '20 47 09N, 106 58 07E', 31, NULL, NULL),
(318, 'Bạch Long Vĩ', '20 08 41N, 107 42 51E', 31, NULL, NULL),
(323, 'Hưng Yên', '20 39 38N, 106 03 08E', 33, NULL, NULL),
(325, 'Văn Lâm', '20 58 31N, 106 02 51E', 33, NULL, NULL),
(326, 'Văn Giang', '20 55 51N, 105 57 14E', 33, NULL, NULL),
(327, 'Yên Mỹ', '20 53 45N, 106 01 21E', 33, NULL, NULL),
(328, 'Mỹ Hào', '20 55 35N, 106 05 34E', 33, NULL, NULL),
(329, 'Ân Thi', '20 48 49N, 106 05 30E', 33, NULL, NULL),
(330, 'Khoái Châu', '20 49 53N, 105 58 28E', 33, NULL, NULL),
(331, 'Kim Động', '20 44 47N, 106 01 47E', 33, NULL, NULL),
(332, 'Tiên Lữ', '20 40 05N, 106 07 59E', 33, NULL, NULL),
(333, 'Phù Cừ', '20 42 51N, 106 11 30E', 33, NULL, NULL),
(336, 'Thái Bình', '20 26 45N, 106 19 56E', 34, NULL, NULL),
(338, 'Quỳnh Phụ', '20 38 57N, 106 21 16E', 34, NULL, NULL),
(339, 'Hưng Hà', '20 35 38N, 106 12 42E', 34, NULL, NULL),
(340, 'Đông Hưng', '20 32 50N, 106 20 15E', 34, NULL, NULL),
(341, 'Thái Thụy', '20 32 33N, 106 32 32E', 34, NULL, NULL),
(342, 'Tiền Hải', '20 21 05N, 106 32 45E', 34, NULL, NULL),
(343, 'Kiến Xương', '20 23 52N, 106 25 22E', 34, NULL, NULL),
(344, 'Vũ Thư', '20 25 29N, 106 16 43E', 34, NULL, NULL),
(347, 'Phủ Lý', '20 32 19N, 105 54 55E', 35, NULL, NULL),
(349, 'Duy Tiên', '20 37 33N, 105 58 01E', 35, NULL, NULL),
(350, 'Kim Bảng', '20 34 19N, 105 50 41E', 35, NULL, NULL),
(351, 'Thanh Liêm', '20 27 31N, 105 55 09E', 35, NULL, NULL),
(352, 'Bình Lục', '20 29 23N, 106 02 52E', 35, NULL, NULL),
(353, 'Lý Nhân', '20 32 55N, 106 04 48E', 35, NULL, NULL),
(356, 'Nam Định', '20 25 07N, 106 09 54E', 36, NULL, NULL),
(358, 'Mỹ Lộc', '20 27 21N, 106 07 56E', 36, NULL, NULL),
(359, 'Vụ Bản', '20 22 57N, 106 05 35E', 36, NULL, NULL),
(360, 'Ý Yên', '20 19 48N, 106 01 55E', 36, NULL, NULL),
(361, 'Nghĩa Hưng', '20 05 37N, 106 08 59E', 36, NULL, NULL),
(362, 'Nam Trực', '20 20 08N, 106 12 54E', 36, NULL, NULL),
(363, 'Trực Ninh', '20 14 42N, 106 12 45E', 36, NULL, NULL),
(364, 'Xuân Trường', '20 17 53N, 106 21 43E', 36, NULL, NULL),
(365, 'Giao Thủy', '20 14 45N, 106 28 39E', 36, NULL, NULL),
(366, 'Hải Hậu', '20 06 26N, 106 16 29E', 36, NULL, NULL),
(369, 'Ninh Bình', '20 14 45N, 105 58 24E', 37, NULL, NULL),
(370, 'Tam Điệp', '20 09 42N, 103 52 43E', 37, NULL, NULL),
(372, 'Nho Quan', '20 18 46N, 105 42 48E', 37, NULL, NULL),
(373, 'Gia Viễn', '20 19 50N, 105 52 20E', 37, NULL, NULL),
(374, 'Hoa Lư', '20 15 04N, 105 55 52E', 37, NULL, NULL),
(375, 'Yên Khánh', '20 11 26N, 106 04 33E', 37, NULL, NULL),
(376, 'Kim Sơn', '20 02 07N, 106 05 27E', 37, NULL, NULL),
(377, 'Yên Mô', '20 07 45N, 105 59 45E', 37, NULL, NULL),
(380, 'Thanh Hóa', '19 48 26N, 105 47 37E', 38, NULL, NULL),
(381, 'Bỉm Sơn', '20 05 21N, 105 51 48E', 38, NULL, NULL),
(382, 'Sầm Sơn', '19 45 11N, 105 54 03E', 38, NULL, NULL),
(384, 'Mường Lát', '20 30 42N, 104 37 27E', 38, NULL, NULL),
(385, 'Quan Hóa', '20 29 15N, 104 56 35E', 38, NULL, NULL),
(386, 'Bá Thước', '20 22 48N, 105 14 50E', 38, NULL, NULL),
(387, 'Quan Sơn', '20 15 17N, 104 51 58E', 38, NULL, NULL),
(388, 'Lang Chánh', '20 08 58N, 105 07 40E', 38, NULL, NULL),
(389, 'Ngọc Lặc', '20 04 08N, 105 22 39E', 38, NULL, NULL),
(390, 'Cẩm Thủy', '20 12 20N, 105 27 22E', 38, NULL, NULL),
(391, 'Thạch Thành', '21 12 41N, 105 36 38E', 38, NULL, NULL),
(392, 'Hà Trung', '20 03 20N, 105 51 20E', 38, NULL, NULL),
(393, 'Vĩnh Lộc', '20 02 29N, 105 39 28E', 38, NULL, NULL),
(394, 'Yên Định', '20 00 31N, 105 37 44E', 38, NULL, NULL),
(395, 'Thọ Xuân', '19 55 39N, 105 29 14E', 38, NULL, NULL),
(396, 'Thường Xuân', '19 54 55N, 105 10 46E', 38, NULL, NULL),
(397, 'Triệu Sơn', '19 48 11N, 105 34 03E', 38, NULL, NULL),
(398, 'Thiệu Hoá', '19 53 56N, 105 40 57E', 38, NULL, NULL),
(399, 'Hoằng Hóa', '19 51 59N, 105 51 34E', 38, NULL, NULL),
(400, 'Hậu Lộc', '19 56 02N, 105 53 19E', 38, NULL, NULL),
(401, 'Nga Sơn', '20 00 16N, 105 59 26E', 38, NULL, NULL),
(402, 'Như Xuân', '19 5 55N, 105 20 22E', 38, NULL, NULL),
(403, 'Như Thanh', '19 35 19N, 105 33 09E', 38, NULL, NULL),
(404, 'Nông Cống', '19 36 58N, 105 40 54E', 38, NULL, NULL),
(405, 'Đông Sơn', '19 47 44N, 105 42 19E', 38, NULL, NULL),
(406, 'Quảng Xương', '19 40 53N, 105 48 01E', 38, NULL, NULL),
(407, 'Tĩnh Gia', '19 27 13N, 105 43 38E', 38, NULL, NULL),
(412, 'Vinh', '18 41 06N, 105 42 05E', 40, NULL, NULL),
(413, 'Cửa Lò', '18 47 26N, 105 43 31E', 40, NULL, NULL),
(414, 'Thái Hoà', '', 40, NULL, NULL),
(415, 'Quế Phong', '19 42 25N, 104 54 21E', 40, NULL, NULL),
(416, 'Quỳ Châu', '19 32 16N, 105 03 18E', 40, NULL, NULL),
(417, 'Kỳ Sơn', '19 24 36N, 104 09 45E', 40, NULL, NULL),
(418, 'Tương Dương', '19 19 15N, 104 35 41E', 40, NULL, NULL),
(419, 'Nghĩa Đàn', '19 21 19N, 105 26 33E', 40, NULL, NULL),
(420, 'Quỳ Hợp', '19 19 24N, 105 09 12E', 40, NULL, NULL),
(421, 'Quỳnh Lưu', '19 14 01N, 105 37 00E', 40, NULL, NULL),
(422, 'Con Cuông', '19 03 50N, 107 47 15E', 40, NULL, NULL),
(423, 'Tân Kỳ', '19 06 11N, 105 14 14E', 40, NULL, NULL),
(424, 'Anh Sơn', '18 58 04N, 105 04 30E', 40, NULL, NULL),
(425, 'Diễn Châu', '19 01 20N, 105 34 13E', 40, NULL, NULL),
(426, 'Yên Thành', '19 01 25N, 105 26 17E', 40, NULL, NULL),
(427, 'Đô Lương', '18 55 00N, 105 21 03E', 40, NULL, NULL),
(428, 'Thanh Chương', '18 44 11N, 105 12 59E', 40, NULL, NULL),
(429, 'Nghi Lộc', '18 47 41N, 105 31 30E', 40, NULL, NULL),
(430, 'Nam Đàn', '18 40 28N, 105 30 32E', 40, NULL, NULL),
(431, 'Hưng Nguyên', '18 41 13N, 105 37 41E', 40, NULL, NULL),
(436, 'Hà Tĩnh', '18 21 20N, 105 54 00E', 42, NULL, NULL),
(437, 'Hồng Lĩnh', '18 32 05N, 105 42 40E', 42, NULL, NULL),
(439, 'Hương Sơn', '18 26 47N, 105 19 36E', 42, NULL, NULL),
(440, 'Đức Thọ', '18 29 23N, 105 36 39E', 42, NULL, NULL),
(441, 'Vũ Quang', '18 19 30N, 105 26 38E', 42, NULL, NULL),
(442, 'Nghi Xuân', '18 38 46N, 105 46 17E', 42, NULL, NULL),
(443, 'Can Lộc', '18 26 39N, 105 46 13E', 42, NULL, NULL),
(444, 'Hương Khê', '18 11 36N, 105 41 24E', 42, NULL, NULL),
(445, 'Thạch Hà', '18 19 29N, 105 52 43E', 42, NULL, NULL),
(446, 'Cẩm Xuyên', '18 11 32N, 106 00 04E', 42, NULL, NULL),
(447, 'Kỳ Anh', '18 05 15N, 106 15 49E', 42, NULL, NULL),
(448, 'Lộc Hà', '', 42, NULL, NULL),
(450, 'Đồng Hới', '17 26 53N, 106 35 15E', 44, NULL, NULL),
(452, 'Minh Hóa', '17 44 03N, 105 51 45E', 44, NULL, NULL),
(453, 'Tuyên Hóa', '17 54 04N, 105 58 17E', 44, NULL, NULL),
(454, 'Quảng Trạch', '17 50 04N, 106 22 24E', 44, NULL, NULL),
(455, 'Bố Trạch', '17 29 13N, 106 06 54E', 44, NULL, NULL),
(456, 'Quảng Ninh', '17 15 15N, 106 32 31E', 44, NULL, NULL),
(457, 'Lệ Thủy', '17 07 35N, 106 41 47E', 44, NULL, NULL),
(461, 'Đông Hà', '16 48 12N, 107 05 12E', 45, NULL, NULL),
(462, 'Quảng Trị', '16 44 37N, 107 11 20E', 45, NULL, NULL),
(464, 'Vĩnh Linh', '17 01 35N, 106 53 49E', 45, NULL, NULL),
(465, 'Hướng Hóa', '16 42 19N, 106 40 14E', 45, NULL, NULL),
(466, 'Gio Linh', '16 54 49N, 106 56 16E', 45, NULL, NULL),
(467, 'Đa Krông', '16 33 58N, 106 55 49E', 45, NULL, NULL),
(468, 'Cam Lộ', '16 47 09N, 106 57 52E', 45, NULL, NULL),
(469, 'Triệu Phong', '16 46 32N, 107 09 12E', 45, NULL, NULL),
(470, 'Hải Lăng', '16 41 07N, 107 13 34E', 45, NULL, NULL),
(471, 'Cồn Cỏ', '19 09 39N, 107 19 58E', 45, NULL, NULL),
(474, 'Huế', '16 27 16N, 107 34 29E', 46, NULL, NULL),
(476, 'Phong Điền', '16 32 42N, 106 16 37E', 46, NULL, NULL),
(477, 'Quảng Điền', '16 35 21N, 107 29 31E', 46, NULL, NULL),
(478, 'Phú Vang', '16 27 20N, 107 42 28E', 46, NULL, NULL),
(479, 'Hương Thủy', '16 19 27N, 107 37 26E', 46, NULL, NULL),
(480, 'Hương Trà', '16 25 49N, 107 28 37E', 46, NULL, NULL),
(481, 'A Lưới', '16 13 59N, 107 16 12E', 46, NULL, NULL),
(482, 'Phú Lộc', '16 17 16N, 107 55 25E', 46, NULL, NULL),
(483, 'Nam Đông', '16 07 11N, 107 41 25E', 46, NULL, NULL),
(490, 'Liên Chiểu', '16 07 26N, 108 07 04E', 48, NULL, NULL),
(491, 'Thanh Khê', '16 03 28N, 108 11 00E', 48, NULL, NULL),
(492, 'Hải Châu', '16 03 38N, 108 11 46E', 48, NULL, NULL),
(493, 'Sơn Trà', '16 06 13N, 108 16 26E', 48, NULL, NULL),
(494, 'Ngũ Hành Sơn', '16 00 30N, 108 15 09E', 48, NULL, NULL),
(495, 'Cẩm Lệ', '', 48, NULL, NULL),
(497, 'Hoà Vang', '16 03 59N, 108 01 57E', 48, NULL, NULL),
(498, 'Hoàng Sa', '16 21 36N, 111 57 01E', 48, NULL, NULL),
(502, 'Tam Kỳ', '15 34 37N, 108 29 52E', 49, NULL, NULL),
(503, 'Hội An', '15 53 20N, 108 20 42E', 49, NULL, NULL),
(504, 'Tây Giang', '15 53 46N, 107 25 52E', 49, NULL, NULL),
(505, 'Đông Giang', '15 54 44N, 107 47 06E', 49, NULL, NULL),
(506, 'Đại Lộc', '15 50 10N, 107 58 27E', 49, NULL, NULL),
(507, 'Điện Bàn', '15 54 15N, 108 13 38E', 49, NULL, NULL),
(508, 'Duy Xuyên', '15 47 58N, 108 13 27E', 49, NULL, NULL),
(509, 'Quế Sơn', '15 41 03N, 108 05 34E', 49, NULL, NULL),
(510, 'Nam Giang', '15 36 37N, 107 33 52E', 49, NULL, NULL),
(511, 'Phước Sơn', '15 23 17N, 107 50 22E', 49, NULL, NULL),
(512, 'Hiệp Đức', '15 31 09N, 108 05 56E', 49, NULL, NULL),
(513, 'Thăng Bình', '15 42 29N, 108 22 04E', 49, NULL, NULL),
(514, 'Tiên Phước', '15 29 30N, 108 15 28E', 49, NULL, NULL),
(515, 'Bắc Trà My', '15 08 00N, 108 05 32E', 49, NULL, NULL),
(516, 'Nam Trà My', '15 16 40N, 108 12 15E', 49, NULL, NULL),
(517, 'Núi Thành', '15 26 53N, 108 34 49E', 49, NULL, NULL),
(518, 'Phú Ninh', '15 30 43N, 108 24 43E', 49, NULL, NULL),
(519, 'Nông Sơn', '', 49, NULL, NULL),
(522, 'Quảng Ngãi', '15 07 17N, 108 48 24E', 51, NULL, NULL),
(524, 'Bình Sơn', '15 18 45N, 108 45 35E', 51, NULL, NULL),
(525, 'Trà Bồng', '15 13 30N, 108 29 57E', 51, NULL, NULL),
(526, 'Tây Trà', '15 11 13N, 108 22 23E', 51, NULL, NULL),
(527, 'Sơn Tịnh', '15 11 49N, 108 45 03E', 51, NULL, NULL),
(528, 'Tư Nghĩa', '15 05 25N, 108 45 23E', 51, NULL, NULL),
(529, 'Sơn Hà', '14 58 35N, 108 29 09E', 51, NULL, NULL),
(530, 'Sơn Tây', '14 58 11N, 108 21 22E', 51, NULL, NULL),
(531, 'Minh Long', '14 56 49N, 108 40 19E', 51, NULL, NULL),
(532, 'Nghĩa Hành', '14 58 06N, 108 46 05E', 51, NULL, NULL),
(533, 'Mộ Đức', '11 59 13N, 108 52 21E', 51, NULL, NULL),
(534, 'Đức Phổ', '14 44 59N, 108 56 58E', 51, NULL, NULL),
(535, 'Ba Tơ', '14 42 52N, 108 43 44E', 51, NULL, NULL),
(536, 'Lý Sơn', '15 22 50N, 109 06 56E', 51, NULL, NULL),
(540, 'Qui Nhơn', '13 47 15N, 109 12 48E', 52, NULL, NULL),
(542, 'An Lão', '14 32 10N, 108 47 52E', 52, NULL, NULL),
(543, 'Hoài Nhơn', '14 30 56N, 109 01 56E', 52, NULL, NULL),
(544, 'Hoài Ân', '14 20 51N, 108 54 04E', 52, NULL, NULL),
(545, 'Phù Mỹ', '14 14 41N, 109 05 43E', 52, NULL, NULL),
(546, 'Vĩnh Thạnh', '14 14 26N, 108 44 08E', 52, NULL, NULL),
(547, 'Tây Sơn', '13 56 47N, 108 53 06E', 52, NULL, NULL),
(548, 'Phù Cát', '14 03 48N, 109 03 57E', 52, NULL, NULL),
(549, 'An Nhơn', '13 51 28N, 109 04 02E', 52, NULL, NULL),
(550, 'Tuy Phước', '13 46 30N, 109 05 38E', 52, NULL, NULL),
(551, 'Vân Canh', '13 40 35N, 108 57 52E', 52, NULL, NULL),
(555, 'Tuy Hòa', '13 05 42N, 109 15 50E', 54, NULL, NULL),
(557, 'Sông Cầu', '13 31 40N, 109 12 39E', 54, NULL, NULL),
(558, 'Đồng Xuân', '13 24 59N, 108 56 46E', 54, NULL, NULL),
(559, 'Tuy An', '13 15 19N, 109 12 21E', 54, NULL, NULL),
(560, 'Sơn Hòa', '13 12 16N, 108 57 17E', 54, NULL, NULL),
(561, 'Sông Hinh', '12 54 19N, 108 53 38E', 54, NULL, NULL),
(562, 'Tây Hoà', '', 54, NULL, NULL),
(563, 'Phú Hoà', '13 04 07N, 109 11 24E', 54, NULL, NULL),
(564, 'Đông Hoà', '', 54, NULL, NULL),
(568, 'Nha Trang', '12 15 40N, 109 10 40E', 56, NULL, NULL),
(569, 'Cam Ranh', '11 59 05N, 108 08 17E', 56, NULL, NULL),
(570, 'Cam Lâm', '', 56, NULL, NULL),
(571, 'Vạn Ninh', '12 43 10N, 109 11 18E', 56, NULL, NULL),
(572, 'Ninh Hòa', '12 32 54N, 109 06 11E', 56, NULL, NULL),
(573, 'Khánh Vĩnh', '12 17 50N, 108 51 56E', 56, NULL, NULL),
(574, 'Diên Khánh', '12 13 19N, 109 02 16E', 56, NULL, NULL),
(575, 'Khánh Sơn', '12 02 17N, 108 53 47E', 56, NULL, NULL),
(576, 'Trường Sa', '9 07 27N, 114 15 00E', 56, NULL, NULL),
(582, 'Phan Rang-Tháp Chàm', '11 36 11N, 108 58 34E', 58, NULL, NULL),
(584, 'Bác Ái', '11 54 45N, 108 51 29E', 58, NULL, NULL),
(585, 'Ninh Sơn', '11 42 36N, 108 44 55E', 58, NULL, NULL),
(586, 'Ninh Hải', '11 42 46N, 109 05 41E', 58, NULL, NULL),
(587, 'Ninh Phước', '11 28 56N, 108 50 34E', 58, NULL, NULL),
(588, 'Thuận Bắc', '', 58, NULL, NULL),
(589, 'Thuận Nam', '', 58, NULL, NULL),
(593, 'Phan Thiết', '10 54 16N, 108 03 44E', 60, NULL, NULL),
(594, 'La Gi', '', 60, NULL, NULL),
(595, 'Tuy Phong', '11 20 26N, 108 41 15E', 60, NULL, NULL),
(596, 'Bắc Bình', '11 15 52N, 108 21 33E', 60, NULL, NULL),
(597, 'Hàm Thuận Bắc', '11 09 18N, 108 03 07E', 60, NULL, NULL),
(598, 'Hàm Thuận Nam', '10 56 20N, 107 54 38E', 60, NULL, NULL),
(599, 'Tánh Linh', '11 08 26N, 107 41 22E', 60, NULL, NULL),
(600, 'Đức Linh', '11 11 43N, 107 31 34E', 60, NULL, NULL),
(601, 'Hàm Tân', '10 44 41N, 107 41 33E', 60, NULL, NULL),
(602, 'Phú Quí', '10 33 13N, 108 57 46E', 60, NULL, NULL),
(608, 'Kon Tum', '14 20 32N, 107 58 04E', 62, NULL, NULL),
(610, 'Đắk Glei', '15 08 13N, 107 44 03E', 62, NULL, NULL),
(611, 'Ngọc Hồi', '14 44 23N, 107 38 49E', 62, NULL, NULL),
(612, 'Đắk Tô', '14 46 49N, 107 55 36E', 62, NULL, NULL),
(613, 'Kon Plông', '14 48 13N, 108 20 05E', 62, NULL, NULL),
(614, 'Kon Rẫy', '14 32 56N, 108 13 09E', 62, NULL, NULL),
(615, 'Đắk Hà', '14 36 50N, 107 59 55E', 62, NULL, NULL),
(616, 'Sa Thầy', '14 16 02N, 107 36 30E', 62, NULL, NULL),
(617, 'Tu Mơ Rông', '', 62, NULL, NULL),
(622, 'Pleiku', '13 57 42N, 107 58 03E', 64, NULL, NULL),
(623, 'An Khê', '14 01 24N, 108 41 29E', 64, NULL, NULL),
(624, 'Ayun Pa', '', 64, NULL, NULL),
(625, 'Kbang', '14 18 08N, 108 29 17E', 64, NULL, NULL),
(626, 'Đăk Đoa', '14 07 02N, 108 09 36E', 64, NULL, NULL),
(627, 'Chư Păh', '14 13 30N, 107 56 33E', 64, NULL, NULL),
(628, 'Ia Grai', '13 59 25N, 107 43 16E', 64, NULL, NULL),
(629, 'Mang Yang', '13 57 26N, 108 18 37E', 64, NULL, NULL),
(630, 'Kông Chro', '13 45 47N, 108 36 04E', 64, NULL, NULL),
(631, 'Đức Cơ', '13 46 16N, 107 38 26E', 64, NULL, NULL),
(632, 'Chư Prông', '13 35 39N, 107 47 36E', 64, NULL, NULL),
(633, 'Chư Sê', '13 37 04N, 108 06 56E', 64, NULL, NULL),
(634, 'Đăk Pơ', '13 55 47N, 108 36 16E', 64, NULL, NULL),
(635, 'Ia Pa', '13 31 37N, 108 30 34E', 64, NULL, NULL),
(637, 'Krông Pa', '13 14 13N, 108 39 12E', 64, NULL, NULL),
(638, 'Phú Thiện', '', 64, NULL, NULL),
(639, 'Chư Pưh', '', 64, NULL, NULL),
(643, 'Buôn Ma Thuột', '12 39 43N, 108 10 40E', 66, NULL, NULL),
(644, 'Buôn Hồ', '', 66, NULL, NULL),
(645, 'Ea H\'leo', '13 13 52N, 108 12 30E', 66, NULL, NULL),
(646, 'Ea Súp', '13 10 59N, 107 46 49E', 66, NULL, NULL),
(647, 'Buôn Đôn', '12 52 45N, 107 45 20E', 66, NULL, NULL),
(648, 'Cư M\'gar', '12 53 47N, 108 04 16E', 66, NULL, NULL),
(649, 'Krông Búk', '12 56 27N, 108 13 54E', 66, NULL, NULL),
(650, 'Krông Năng', '12 59 41N, 108 23 42E', 66, NULL, NULL),
(651, 'Ea Kar', '12 48 17N, 108 32 42E', 66, NULL, NULL),
(652, 'M\'đrắk', '12 42 14N, 108 47 09E', 66, NULL, NULL),
(653, 'Krông Bông', '12 27 08N, 108 27 01E', 66, NULL, NULL),
(654, 'Krông Pắc', '12 41 20N, 108 18 42E', 66, NULL, NULL),
(655, 'Krông A Na', '12 31 51N, 108 05 03E', 66, NULL, NULL),
(656, 'Lắk', '12 19 20N, 108 12 17E', 66, NULL, NULL),
(657, 'Cư Kuin', '', 66, NULL, NULL),
(660, 'Gia Nghĩa', '', 67, NULL, NULL),
(661, 'Đắk Glong', '12 01 53N, 107 50 37E', 67, NULL, NULL),
(662, 'Cư Jút', '12 40 56N, 107 44 44E', 67, NULL, NULL),
(663, 'Đắk Mil', '12 31 08N, 107 42 24E', 67, NULL, NULL),
(664, 'Krông Nô', '12 22 16N, 107 53 49E', 67, NULL, NULL),
(665, 'Đắk Song', '12 14 04N, 107 36 30E', 67, NULL, NULL),
(666, 'Đắk R\'lấp', '12 02 30N, 107 25 59E', 67, NULL, NULL),
(667, 'Tuy Đức', '', 67, NULL, NULL),
(672, 'Đà Lạt', '11 54 33N, 108 27 08E', 68, NULL, NULL),
(673, 'Bảo Lộc', '11 32 48N, 107 47 37E', 68, NULL, NULL),
(674, 'Đam Rông', '12 02 35N, 108 10 26E', 68, NULL, NULL),
(675, 'Lạc Dương', '12 08 30N, 108 27 48E', 68, NULL, NULL),
(676, 'Lâm Hà', '11 55 26N, 108 11 31E', 68, NULL, NULL),
(677, 'Đơn Dương', '11 48 26N, 108 32 48E', 68, NULL, NULL),
(678, 'Đức Trọng', '11 41 50N, 108 18 58E', 68, NULL, NULL),
(679, 'Di Linh', '11 31 10N, 108 05 23E', 68, NULL, NULL),
(680, 'Bảo Lâm', '11 38 31N, 107 43 25E', 68, NULL, NULL),
(681, 'Đạ Huoai', '11 27 11N, 107 38 08E', 68, NULL, NULL),
(682, 'Đạ Tẻh', '11 33 46N, 107 32 00E', 68, NULL, NULL),
(683, 'Cát Tiên', '11 39 38N, 107 23 27E', 68, NULL, NULL),
(688, 'Phước Long', '', 70, NULL, NULL),
(689, 'Đồng Xoài', '11 31 01N, 106 50 21E', 70, NULL, NULL),
(690, 'Bình Long', '', 70, NULL, NULL),
(691, 'Bù Gia Mập', '11 56 57N, 106 59 21E', 70, NULL, NULL),
(692, 'Lộc Ninh', '11 49 28N, 106 35 11E', 70, NULL, NULL),
(693, 'Bù Đốp', '11 59 08N, 106 49 54E', 70, NULL, NULL),
(694, 'Hớn Quản', '11 37 37N, 106 36 02E', 70, NULL, NULL),
(695, 'Đồng Phù', '11 28 45N, 106 57 07E', 70, NULL, NULL),
(696, 'Bù Đăng', '11 46 09N, 107 14 14E', 70, NULL, NULL),
(697, 'Chơn Thành', '11 28 45N, 106 39 26E', 70, NULL, NULL),
(703, 'Tây Ninh', '11 21 59N, 106 07 47E', 72, NULL, NULL),
(705, 'Tân Biên', '11 35 14N, 105 57 53E', 72, NULL, NULL),
(706, 'Tân Châu', '11 34 49N, 106 17 48E', 72, NULL, NULL),
(707, 'Dương Minh Châu', '11 22 04N, 106 16 58E', 72, NULL, NULL),
(708, 'Châu Thành', '11 19 02N, 106 00 15E', 72, NULL, NULL),
(709, 'Hòa Thành', '11 15 31N, 106 08 44E', 72, NULL, NULL),
(710, 'Gò Dầu', '11 09 39N, 106 14 42E', 72, NULL, NULL),
(711, 'Bến Cầu', '11 07 50N, 106 08 25E', 72, NULL, NULL),
(712, 'Trảng Bàng', '11 06 18N, 106 23 12E', 72, NULL, NULL),
(718, 'Thủ Dầu Một', '11 00 01N, 106 38 56E', 74, NULL, NULL),
(720, 'Dầu Tiếng', '11 19 07N, 106 26 59E', 74, NULL, NULL),
(721, 'Bến Cát', '11 12 42N, 106 36 28E', 74, NULL, NULL),
(722, 'Phú Giáo', '11 20 21N, 106 47 48E', 74, NULL, NULL),
(723, 'Tân Uyên', '11 06 31N, 106 49 02E', 74, NULL, NULL),
(724, 'Dĩ An', '10 55 03N, 106 47 09E', 74, NULL, NULL),
(725, 'Thuận An', '10 55 58N, 106 41 59E', 74, NULL, NULL),
(731, 'Biên Hòa', '10 56 31N, 106 50 50E', 75, NULL, NULL),
(732, 'Long Khánh', '10 56 24N, 107 14 29E', 75, NULL, NULL),
(734, 'Tân Phú', '11 22 51N, 107 21 35E', 75, NULL, NULL),
(735, 'Vĩnh Cửu', '11 14 31N, 107 00 06E', 75, NULL, NULL),
(736, 'Định Quán', '11 12 41N, 107 17 03E', 75, NULL, NULL),
(737, 'Trảng Bom', '10 58 39N, 107 00 52E', 75, NULL, NULL),
(738, 'Thống Nhất', '10 58 29N, 107 09 26E', 75, NULL, NULL),
(739, 'Cẩm Mỹ', '10 47 05N, 107 14 36E', 75, NULL, NULL),
(740, 'Long Thành', '10 47 38N, 106 59 42E', 75, NULL, NULL),
(741, 'Xuân Lộc', '10 55 39N, 107 24 21E', 75, NULL, NULL),
(742, 'Nhơn Trạch', '10 39 18N, 106 53 18E', 75, NULL, NULL),
(747, 'Vũng Tầu', '10 24 08N, 107 07 05E', 77, NULL, NULL),
(748, 'Bà Rịa', '10 30 33N, 107 10 47E', 77, NULL, NULL),
(750, 'Châu Đức', '10 39 23N, 107 15 08E', 77, NULL, NULL),
(751, 'Xuyên Mộc', '10 37 46N, 107 29 39E', 77, NULL, NULL),
(752, 'Long Điền', '10 26 47N, 107 12 53E', 77, NULL, NULL),
(753, 'Đất Đỏ', '10 28 40N, 107 18 27E', 77, NULL, NULL),
(754, 'Tân Thành', '10 34 50N, 107 05 06E', 77, NULL, NULL),
(755, 'Côn Đảo', '8 42 25N, 106 36 05E', 77, NULL, NULL),
(760, '1', '10 46 34N, 106 41 45E', 79, NULL, NULL),
(761, '12', '10 51 43N, 106 39 32E', 79, NULL, NULL),
(762, 'Thủ Đức', '10 51 20N, 106 45 05E', 79, NULL, NULL),
(763, '9', '10 49 49N, 106 49 03E', 79, NULL, NULL),
(764, 'Gò Vấp', '10 50 12N, 106 39 52E', 79, NULL, NULL),
(765, 'Bình Thạnh', '10 48 46N, 106 42 57E', 79, NULL, NULL),
(766, 'Tân Bình', '10 48 13N, 106 39 03E', 79, NULL, NULL),
(767, 'Tân Phú', '10 47 32N, 106 37 31E', 79, NULL, NULL),
(768, 'Phú Nhuận', '10 48 06N, 106 40 39E', 79, NULL, NULL),
(769, '2', '10 46 51N, 106 45 25E', 79, NULL, NULL),
(770, '3', '10 46 48N, 106 40 46E', 79, NULL, NULL),
(771, '10', '10 46 25N, 106 40 02E', 79, NULL, NULL),
(772, '11', '10 46 01N, 106 38 44E', 79, NULL, NULL),
(773, '4', '10 45 42N, 106 42 09E', 79, NULL, NULL),
(774, '5', '10 45 24N, 106 40 00E', 79, NULL, NULL),
(775, '6', '10 44 46N, 106 38 10E', 79, NULL, NULL),
(776, '8', '10 43 24N, 106 37 40E', 79, NULL, NULL),
(777, 'Bình Tân', '10 46 16N, 106 35 26E', 79, NULL, NULL),
(778, '7', '10 44 19N, 106 43 35E', 79, NULL, NULL),
(783, 'Củ Chi', '11 02 17N, 106 30 20E', 79, NULL, NULL),
(784, 'Hóc Môn', '10 52 42N, 106 35 33E', 79, NULL, NULL),
(785, 'Bình Chánh', '10 45 01N, 106 30 45E', 79, NULL, NULL),
(786, 'Nhà Bè', '10 39 06N, 106 43 35E', 79, NULL, NULL),
(787, 'Cần Giờ', '10 30 43N, 106 52 50E', 79, NULL, NULL),
(794, 'Tân An', '10 31 36N, 106 24 06E', 80, NULL, NULL),
(796, 'Tân Hưng', '10 49 05N, 105 39 26E', 80, NULL, NULL),
(797, 'Vĩnh Hưng', '10 52 54N, 105 45 58E', 80, NULL, NULL),
(798, 'Mộc Hóa', '10 47 09N, 105 57 56E', 80, NULL, NULL),
(799, 'Tân Thạnh', '10 37 44N, 105 57 07E', 80, NULL, NULL),
(800, 'Thạnh Hóa', '10 41 37N, 106 11 08E', 80, NULL, NULL),
(801, 'Đức Huệ', '10 51 57N, 106 15 48E', 80, NULL, NULL),
(802, 'Đức Hòa', '10 53 04N, 106 23 58E', 80, NULL, NULL),
(803, 'Bến Lức', '10 41 40N, 106 26 28E', 80, NULL, NULL),
(804, 'Thủ Thừa', '10 39 41N, 106 20 12E', 80, NULL, NULL),
(805, 'Tân Trụ', '10 31 47N, 106 30 06E', 80, NULL, NULL),
(806, 'Cần Đước', '10 32 21N, 106 36 33E', 80, NULL, NULL),
(807, 'Cần Giuộc', '10 34 43N, 106 38 35E', 80, NULL, NULL),
(808, 'Châu Thành', '10 27 52N, 106 30 00E', 80, NULL, NULL),
(815, 'Mỹ Tho', '10 22 14N, 106 21 52E', 82, NULL, NULL),
(816, 'Gò Công', '10 21 55N, 106 40 24E', 82, NULL, NULL),
(818, 'Tân Phước', '10 30 36N, 106 13 02E', 82, NULL, NULL),
(819, 'Cái Bè', '10 24 21N, 105 56 01E', 82, NULL, NULL),
(820, 'Cai Lậy', '10 24 20N, 106 06 05E', 82, NULL, NULL),
(821, 'Châu Thành', '20 25 21N, 106 16 57E', 82, NULL, NULL),
(822, 'Chợ Gạo', '10 23 45N, 106 26 53E', 82, NULL, NULL),
(823, 'Gò Công Tây', '10 19 55N, 106 35 02E', 82, NULL, NULL),
(824, 'Gò Công Đông', '10 20 42N, 106 42 54E', 82, NULL, NULL),
(825, 'Tân Phú Đông', '', 82, NULL, NULL),
(829, 'Bến Tre', '10 14 17N, 106 22 26E', 83, NULL, NULL),
(831, 'Châu Thành', '10 17 24N, 106 17 45E', 83, NULL, NULL),
(832, 'Chợ Lách', '10 13 26N, 106 09 08E', 83, NULL, NULL),
(833, 'Mỏ Cày Nam', '10 06 56N, 106 19 40E', 83, NULL, NULL),
(834, 'Giồng Trôm', '10 08 46N, 106 28 12E', 83, NULL, NULL),
(835, 'Bình Đại', '10 09 56N, 106 37 46E', 83, NULL, NULL),
(836, 'Ba Tri', '10 04 08N, 106 35 10E', 83, NULL, NULL),
(837, 'Thạnh Phú', '9 55 53N, 106 32 45E', 83, NULL, NULL),
(838, 'Mỏ Cày Bắc', '', 83, NULL, NULL),
(842, 'Trà Vinh', '9 57 09N, 106 20 39E', 84, NULL, NULL),
(844, 'Càng Long', '9 58 18N, 106 12 52E', 84, NULL, NULL),
(845, 'Cầu Kè', '9 51 23N, 106 03 33E', 84, NULL, NULL),
(846, 'Tiểu Cần', '9 48 37N, 106 12 06E', 84, NULL, NULL),
(847, 'Châu Thành', '9 52 57N, 106 24 12E', 84, NULL, NULL),
(848, 'Cầu Ngang', '9 47 14N, 106 29 19E', 84, NULL, NULL),
(849, 'Trà Cú', '9 42 06N, 106 16 24E', 84, NULL, NULL),
(850, 'Duyên Hải', '9 39 58N, 106 26 23E', 84, NULL, NULL),
(855, 'Vĩnh Long', '10 15 09N, 105 56 08E', 86, NULL, NULL),
(857, 'Long Hồ', '10 13 58N, 105 55 47E', 86, NULL, NULL),
(858, 'Mang Thít', '10 10 58N, 106 05 13E', 86, NULL, NULL),
(859, 'Vũng Liêm', '10 03 32N, 106 10 35E', 86, NULL, NULL),
(860, 'Tam Bình', '10 03 58N, 105 58 03E', 86, NULL, NULL),
(861, 'Bình Minh', '10 05 45N, 105 47 21E', 86, NULL, NULL),
(862, 'Trà Ôn', '9 59 20N, 105 57 56E', 86, NULL, NULL),
(863, 'Bình Tân', '', 86, NULL, NULL),
(866, 'Cao Lãnh', '10 27 38N, 105 37 21E', 87, NULL, NULL),
(867, 'Sa Đéc', '10 19 22N, 105 44 31E', 87, NULL, NULL),
(868, 'Hồng Ngự', '', 87, NULL, NULL),
(869, 'Tân Hồng', '10 52 48N, 105 29 21E', 87, NULL, NULL),
(870, 'Hồng Ngự', '10 48 13N, 105 19 00E', 87, NULL, NULL),
(871, 'Tam Nông', '10 44 06N, 105 30 58E', 87, NULL, NULL),
(872, 'Tháp Mười', '10 33 36N, 105 47 13E', 87, NULL, NULL),
(873, 'Cao Lãnh', '10 29 03N, 105 41 40E', 87, NULL, NULL),
(874, 'Thanh Bình', '10 36 38N, 105 28 51E', 87, NULL, NULL),
(875, 'Lấp Vò', '10 21 27N, 105 36 06E', 87, NULL, NULL),
(876, 'Lai Vung', '10 14 43N, 105 38 40E', 87, NULL, NULL),
(877, 'Châu Thành', '10 13 27N, 105 48 38E', 87, NULL, NULL),
(883, 'Long Xuyên', '10 22 22N, 105 25 33E', 89, NULL, NULL),
(884, 'Châu Đốc', '10 41 20N, 105 05 15E', 89, NULL, NULL),
(886, 'An Phú', '10 50 12N, 105 05 33E', 89, NULL, NULL),
(887, 'Tân Châu', '10 49 11N, 105 11 18E', 89, NULL, NULL),
(888, 'Phú Tân', '10 40 26N, 105 14 40E', 89, NULL, NULL),
(889, 'Châu Phú', '10 34 12N, 105 12 13E', 89, NULL, NULL),
(890, 'Tịnh Biên', '10 33 30N, 105 00 17E', 89, NULL, NULL),
(891, 'Tri Tôn', '10 24 41N, 104 56 58E', 89, NULL, NULL),
(892, 'Châu Thành', '10 25 39N, 105 15 31E', 89, NULL, NULL),
(893, 'Chợ Mới', '10 27 23N, 105 26 57E', 89, NULL, NULL),
(894, 'Thoại Sơn', '10 16 45N, 105 15 59E', 89, NULL, NULL),
(899, 'Rạch Giá', '10 01 35N, 105 06 20E', 91, NULL, NULL),
(900, 'Hà Tiên', '10 22 54N, 104 30 10E', 91, NULL, NULL),
(902, 'Kiên Lương', '10 20 21N, 104 39 46E', 91, NULL, NULL),
(903, 'Hòn Đất', '10 14 20N, 104 55 57E', 91, NULL, NULL),
(904, 'Tân Hiệp', '10 05 18N, 105 14 04E', 91, NULL, NULL),
(905, 'Châu Thành', '9 57 37N, 105 10 16E', 91, NULL, NULL),
(906, 'Giồng Giềng', '9 56 05N, 105 22 06E', 91, NULL, NULL),
(907, 'Gò Quao', '9 43 17N, 105 17 06E', 91, NULL, NULL),
(908, 'An Biên', '9 48 37N, 105 03 18E', 91, NULL, NULL),
(909, 'An Minh', '9 40 24N, 104 59 05E', 91, NULL, NULL),
(910, 'Vĩnh Thuận', '9 33 25N, 105 11 30E', 91, NULL, NULL),
(911, 'Phú Quốc', '10 13 44N, 103 57 25E', 91, NULL, NULL),
(912, 'Kiên Hải', '9 48 31N, 104 37 48E', 91, NULL, NULL),
(913, 'U Minh Thượng', '', 91, NULL, NULL),
(914, 'Giang Thành', '', 91, NULL, NULL),
(916, 'Ninh Kiều', '10 01 58N, 105 45 34E', 92, NULL, NULL),
(917, 'Ô Môn', '10 07 28N, 105 37 51E', 92, NULL, NULL),
(918, 'Bình Thuỷ', '10 03 42N, 105 43 17E', 92, NULL, NULL),
(919, 'Cái Răng', '9 59 57N, 105 46 56E', 92, NULL, NULL),
(923, 'Thốt Nốt', '10 14 23N, 105 32 02E', 92, NULL, NULL),
(924, 'Vĩnh Thạnh', '10 11 35N, 105 22 45E', 92, NULL, NULL),
(925, 'Cờ Đỏ', '10 02 48N, 105 29 46E', 92, NULL, NULL),
(926, 'Phong Điền', '9 59 57N, 105 39 35E', 92, NULL, NULL),
(927, 'Thới Lai', '', 92, NULL, NULL),
(930, 'Vị Thanh', '9 45 15N, 105 24 50E', 93, NULL, NULL),
(931, 'Ngã Bảy', '', 93, NULL, NULL),
(932, 'Châu Thành A', '9 55 50N, 105 38 31E', 93, NULL, NULL),
(933, 'Châu Thành', '9 55 22N, 105 48 37E', 93, NULL, NULL),
(934, 'Phụng Hiệp', '9 47 20N, 105 43 29E', 93, NULL, NULL),
(935, 'Vị Thuỷ', '9 48 05N, 105 32 13E', 93, NULL, NULL),
(936, 'Long Mỹ', '9 40 47N, 105 30 53E', 93, NULL, NULL),
(941, 'Sóc Trăng', '9 36 39N, 105 59 00E', 94, NULL, NULL),
(942, 'Châu Thành', '', 94, NULL, NULL),
(943, 'Kế Sách', '9 49 30N, 105 57 25E', 94, NULL, NULL),
(944, 'Mỹ Tú', '9 38 21N, 105 49 52E', 94, NULL, NULL),
(945, 'Cù Lao Dung', '9 37 36N, 106 12 13E', 94, NULL, NULL),
(946, 'Long Phú', '9 34 38N, 106 06 07E', 94, NULL, NULL),
(947, 'Mỹ Xuyên', '9 28 16N, 105 55 51E', 94, NULL, NULL),
(948, 'Ngã Năm', '9 31 38N, 105 37 22E', 94, NULL, NULL),
(949, 'Thạnh Trị', '9 28 05N, 105 43 02E', 94, NULL, NULL),
(950, 'Vĩnh Châu', '9 20 50N, 105 59 58E', 94, NULL, NULL),
(951, 'Trần Đề', '', 94, NULL, NULL),
(954, 'Bạc Liêu', '9 16 05N, 105 45 08E', 95, NULL, NULL),
(956, 'Hồng Dân', '9 30 37N, 105 24 25E', 95, NULL, NULL),
(957, 'Phước Long', '9 23 13N, 105 24 41E', 95, NULL, NULL),
(958, 'Vĩnh Lợi', '9 16 51N, 105 40 54E', 95, NULL, NULL),
(959, 'Giá Rai', '9 15 51N, 105 23 18E', 95, NULL, NULL),
(960, 'Đông Hải', '9 08 11N, 105 24 42E', 95, NULL, NULL),
(961, 'Hoà Bình', '', 95, NULL, NULL),
(964, 'Cà Mau', '9 10 33N, 105 11 11E', 96, NULL, NULL),
(966, 'U Minh', '9 22 30N, 104 57 00E', 96, NULL, NULL),
(967, 'Thới Bình', '9 22 50N, 105 07 35E', 96, NULL, NULL),
(968, 'Trần Văn Thời', '9 07 36N, 104 57 27E', 96, NULL, NULL),
(969, 'Cái Nước', '9 00 31N, 105 03 23E', 96, NULL, NULL),
(970, 'Đầm Dơi', '8 57 48N, 105 13 56E', 96, NULL, NULL),
(971, 'Năm Căn', '8 46 59N, 104 58 20E', 96, NULL, NULL),
(972, 'Phú Tân', '8 52 47N, 104 53 35E', 96, NULL, NULL),
(973, 'Ngọc Hiển', '8 40 47N, 104 57 58E', 96, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `post_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `images`
--

INSERT INTO `images` (`id`, `image`, `post_id`) VALUES
(1, 'admin/images/1.jpg', 2),
(11, 'admin/images/nhadat.jpg', 21),
(12, 'admin/images/1.jpg', 22),
(21, 'admin/images/Softech.jpg', 33),
(23, 'admin/images/8.jpg', 36),
(26, 'librarys/images/posts/2018/07/06/dam-den-ren-cham-bi-1527566054 - Copy.jpg', 40),
(29, 'librarys/images/posts/2018/07/06/dam-be-kt-phoi-luoi-1525668064.jpg', 43),
(30, 'admin/images/2.jpg', 45),
(31, 'admin/images/33524985_2115577418657423_3298649258307092480_o.jpg', 47),
(32, 'admin/images/nhadat.jpg', 50),
(33, 'admin/images/33524985_2115577418657423_3298649258307092480_o.jpg', 51),
(34, 'librarys/images/posts/2018/07/09/dam-den-beo-xep-ly-1527656272.jpg', 52);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `title` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `acreage` float NOT NULL,
  `city_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` int(11) NOT NULL,
  `unit` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `lay` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `balcony` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `license` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `posts`
--

INSERT INTO `posts` (`id`, `title`, `acreage`, `city_id`, `district_id`, `address`, `price`, `unit`, `description`, `lay`, `balcony`, `license`, `category_id`, `created_at`, `updated_at`) VALUES
(21, 'Mua bán nhà đất', 1000, 22, 0, '', 500000, '', 'Nhà đầy đủ tiện nghi, gần chợ và thuận tiện đi lại.', 'Đông', 'Đông Nam', 'Có', 1, '2018-07-06 02:42:26', NULL),
(33, 'Mua bán nhà đất', 1000, 1, 0, '', 5000, '', 'Nhà đầy đủ tiện nghi, gần chợ và thuận tiện đi lại.', 'Đông', 'Đông Nam', 'Có', 1, '2018-07-06 03:58:27', NULL),
(36, 'Mua bán nhà đất', 1000, 24, 0, '', 5000, '', 'Nhà đầy đủ tiện nghi, gần chợ và thuận tiện đi lại.', 'Đông', 'Đông Nam', 'Có', 1, '2018-07-06 04:23:33', NULL),
(40, 'Cho thuê nhà trọ sin viên', 30, 4, 43, '101B, Le Huu Trac, Son Tra, Da Nang', 1, 'Triệu/tháng', 'hiền nguyễn 98', 'Tây', 'Gần chợ/siêu thị', 'Chưa có cửa sổ', 3, '2018-07-06 06:50:06', '2018-07-06 06:50:06'),
(43, 'Bán căn hộ thương mại ngay trung tâm thanh ph', 100, 60, 596, '101 pham cu luong', 30, 'Triệu/tháng', 'Nằm ngay trung t&acirc;m du lịch dich vụ, c&aacute;ch kh&ocirc;ng xa c&aacute;c trung t&acirc;m vui chơi giải tr&iacute; v&agrave; c&aacute;c điểm du lịch nổi tiếng ở Nha Trang.', 'Tây Nam', 'Gần chợ/siêu thị', 'Chưa có cửa sổ', 2, '2018-07-06 06:59:06', '2018-07-06 06:59:06'),
(44, 'Mua bán xe ô tô 7 chỗ', 100, 89, 889, NULL, 5000, NULL, 'Nhà đầy đủ tiện nghi, gần chợ và thuận tiện đi lại.', 'Đông', 'Đông Nam', 'Có', 1, '2018-07-06 07:13:52', NULL),
(45, 'Mua bán xe ô tô 7 chỗ', 100, 89, 889, NULL, 5000, NULL, 'Nhà đầy đủ tiện nghi, gần chợ và thuận tiện đi lại.', 'Đông', 'Đông Nam', 'Có', 1, '2018-07-06 07:14:06', NULL),
(46, 'Mua bán nhà đất', 1000, 42, 446, NULL, 5000, NULL, 'Nhà đầy đủ tiện nghi, gần chợ và thuận tiện đi lại.', 'Đông', 'Đông Nam', 'Có', 1, '2018-07-06 08:10:49', NULL),
(47, 'Mua bán nhà đất', 1000, 42, 446, NULL, 5000, NULL, 'Nhà đầy đủ tiện nghi, gần chợ và thuận tiện đi lại.', 'Đông', 'Đông Nam', 'Có', 1, '2018-07-06 08:11:42', NULL),
(48, 'Bán nhà', 1000, 42, 446, NULL, 5000, NULL, 'Nhà đầy đủ tiện nghi, gần chợ và thuận tiện đi lại.', 'Đông', 'Đông Nam', 'Có', 1, '2018-07-06 08:36:46', NULL),
(49, 'Mua bán nhà đất', 1000, 77, 748, NULL, 454, 'Tỷ', 'Nhà đầy đủ tiện nghi, gần chợ và thuận tiện đi lại.', 'Đông', 'Đông Nam', 'Có', 1, '2018-07-06 08:39:13', NULL),
(50, 'Mua bán nhà đất', 1000, 77, 748, NULL, 454, 'Tỷ', 'Nhà đầy đủ tiện nghi, gần chợ và thuận tiện đi lại.', 'Đông', 'Đông Nam', 'Có', 1, '2018-07-06 08:39:29', NULL),
(51, 'Bán máy sấy tóc Nhật bản', 1000, 42, 446, 'Da Nang', 5, 'Triệu', 'Nhà đầy đủ tiện nghi, gần chợ và thuận tiện đi lại.', 'Đông', 'Đông Nam', 'Có', 13, '2018-07-06 08:47:13', NULL),
(52, 'mua bán chung cư giá rẻ', 200, 27, 264, '112 Trường Sa, quận Ngọc Ngà', 120, 'Triệu/tháng', 'mua b&aacute;n chung cư&nbsp;', 'Tây', 'Gần chợ/siêu thị', 'Chưa có cửa sổ', 2, '2018-07-09 02:35:22', '2018-07-09 02:35:22');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `post_contacts`
--

CREATE TABLE `post_contacts` (
  `id` int(11) NOT NULL,
  `phone` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `post_contacts`
--

INSERT INTO `post_contacts` (`id`, `phone`, `email`, `fullname`, `post_id`, `user_id`) VALUES
(17, '1678408880', 'thuthuong.016@gmail.com', 'Thương Nguyễn', 21, NULL),
(32, '01678408880', 'thuong.nguyen@student.passerellesnumerique', 'Thương Nguyễn', 33, NULL),
(34, '01678408880', 'thuong.nguyen@student.passerellesnumeriques.o', 'Thương Nguyễn', 36, NULL),
(37, '01678408880', 'thica@gmail.com', 'Arthor', 0, NULL),
(40, '0972616834', 'hien.nguyen@student.passerellesnumeriques.org', 'Nguyen Thi Thien Thanh', 40, 1),
(44, '01668765234', 'thuy.le@gmai.com', 'Nguyen Thi Thu Trang', 43, 1),
(46, '01678408880', 'thuong@gmail.com', 'Arthor', 45, NULL),
(50, '01678408880', 'thuong@gmail.com', 'Arthor', 47, NULL),
(52, '01678408880', 'thuong@gmail.com', 'Arthor', 50, NULL),
(53, '01678408880', 'thuong@gmail.com', 'tyty', 51, NULL),
(54, '0972616834', 'thuy.le@gmai.com', 'Nguyen Thi Hien', 52, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `provinces`
--

CREATE TABLE `provinces` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `provinces`
--

INSERT INTO `provinces` (`id`, `name`, `alias`, `created_at`, `updated_at`) VALUES
(1, 'Hà Nội', 'Ha-Noi', NULL, 1498447671),
(2, 'Hà Giang', 'Ha-Giang', NULL, 1498447671),
(4, 'Cao Bằng', 'Cao-Bang', NULL, 1498447671),
(6, 'Bắc Kạn', 'Bac-Kan', NULL, 1498447671),
(8, 'Tuyên Quang', 'Tuyen-Quang', NULL, 1498447671),
(10, 'Lào Cai', 'Lao-Cai', NULL, 1498447671),
(11, 'Điện Biên', 'Dien-Bien', NULL, 1498447671),
(12, 'Lai Châu', 'Lai-Chau', NULL, 1498447671),
(14, 'Sơn La', 'Son-La', NULL, 1498447671),
(15, 'Yên Bái', 'Yen-Bai', NULL, 1498447671),
(17, 'Hòa Bình', 'Hoa-Binh', NULL, 1498447671),
(19, 'Thái Nguyên', 'Thai-Nguyen', NULL, 1498447671),
(20, 'Lạng Sơn', 'Lang-Son', NULL, 1498447671),
(22, 'Quảng Ninh', 'Quang-Ninh', NULL, 1498447671),
(24, 'Bắc Giang', 'Bac-Giang', NULL, 1498447671),
(25, 'Phú Thọ', 'Phu-Tho', NULL, 1498447671),
(26, 'Vĩnh Phúc', 'Vinh-Phuc', NULL, 1498447671),
(27, 'Bắc Ninh', 'Bac-Ninh', NULL, 1498447671),
(30, 'Hải Dương', 'Hai-Duong', NULL, 1498447671),
(31, 'Hải Phòng', 'Hai-Phong', NULL, 1498447672),
(33, 'Hưng Yên', 'Hung-Yen', NULL, 1498447672),
(34, 'Thái Bình', 'Thai-Binh', NULL, 1498447672),
(35, 'Hà Nam', 'Ha-Nam', NULL, 1498447672),
(36, 'Nam Định', 'Nam-Dinh', NULL, 1498447672),
(37, 'Ninh Bình', 'Ninh-Binh', NULL, 1498447672),
(38, 'Thanh Hóa', 'Thanh-Hoa', NULL, 1498447672),
(40, 'Nghệ An', 'Nghe-An', NULL, 1498447672),
(42, 'Hà Tĩnh', 'Ha-Tinh', NULL, 1498447672),
(44, 'Quảng Bình', 'Quang-Binh', NULL, 1498447672),
(45, 'Quảng Trị', 'Quang-Tri', NULL, 1498447672),
(46, 'Thừa Thiên Huế', 'Thua-Thien-Hue', NULL, 1498447672),
(48, 'Đà Nẵng', 'Da-Nang', NULL, 1498447672),
(49, 'Quảng Nam', 'Quang-Nam', NULL, 1498447672),
(51, 'Quảng Ngãi', 'Quang-Ngai', NULL, 1498447672),
(52, 'Bình Định', 'Binh-Dinh', NULL, 1498447672),
(54, 'Phú Yên', 'Phu-Yen', NULL, 1498447672),
(56, 'Khánh Hòa', 'Khanh-Hoa', NULL, 1498447672),
(58, 'Ninh Thuận', 'Ninh-Thuan', NULL, 1498447672),
(60, 'Bình Thuận', 'Binh-Thuan', NULL, 1498447672),
(62, 'Kon Tum', 'Kon-Tum', NULL, 1498447672),
(64, 'Gia Lai', 'Gia-Lai', NULL, 1498447673),
(66, 'Đắk Lắk', 'Dak-Lak', NULL, 1498447673),
(67, 'Đắk Nông', 'Dak-Nong', NULL, 1498447673),
(68, 'Lâm Đồng', 'Lam-Dong', NULL, 1498447673),
(70, 'Bình Phước', 'Binh-Phuoc', NULL, 1498447673),
(72, 'Tây Ninh', 'Tay-Ninh', NULL, 1498447673),
(74, 'Bình Dương', 'Binh-Duong', NULL, 1498447673),
(75, 'Đồng Nai', 'Dong-Nai', NULL, 1498447673),
(77, 'Bà Rịa - Vũng Tàu', 'Ba-Ria-Vung-Tau', NULL, 1498447673),
(79, 'Hồ Chí Minh', 'Ho-Chi-Minh', NULL, 1498447673),
(80, 'Long An', 'Long-An', NULL, 1498447673),
(82, 'Tiền Giang', 'Tien-Giang', NULL, 1498447673),
(83, 'Bến Tre', 'Ben-Tre', NULL, 1498447673),
(84, 'Trà Vinh', 'Tra-Vinh', NULL, 1498447673),
(86, 'Vĩnh Long', 'Vinh-Long', NULL, 1498447673),
(87, 'Đồng Tháp', 'Dong-Thap', NULL, 1498447673),
(89, 'An Giang', 'An-Giang', NULL, 1498447673),
(91, 'Kiên Giang', 'Kien-Giang', NULL, 1498447673),
(92, 'Cần Thơ', 'Can-Tho', NULL, 1498447673),
(93, 'Hậu Giang', 'Hau-Giang', NULL, 1498447673),
(94, 'Sóc Trăng', 'Soc-Trang', NULL, 1498447673),
(95, 'Bạc Liêu', 'Bac-Lieu', NULL, 1498447673),
(96, 'Cà Mau', 'Ca-Mau', NULL, 1498447673);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` int(2) NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `email`, `role`, `password`, `facebook`, `status`) VALUES
(28, 'taonguyen991@gmail.com', 0, '894c925e9616baf4484f6fccbf9013c0', '123', 0),
(30, 'thuongnguyen996@gmail.com', 0, '894c925e9616baf4484f6fccbf9013c0', '123', 0),
(38, 'thica@gmail.com', 0, '894c925e9616baf4484f6fccbf9013c0', '0', 0),
(64, 'van1@gmail.com', 0, '25f9e794323b453885f5181f1b624d0b', '01245484146', 0);


-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user_infos`
--

CREATE TABLE `user_infos` (
  `id` int(11) NOT NULL,
  `phone` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `img` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `user_infos`
--

INSERT INTO `user_infos` (`id`, `phone`, `gender`, `fullname`, `address`, `user_id`, `img`, `created_at`, `updated_at`) VALUES
(6, '12345678978', 'Nam', 'Nguyen Van Tao', 'Quảng Nam', 28, '../../assets/img/avatar.png', '0000-00-00 00:00:00', '2018-07-04 03:23:25'),
(7, '01626252159', 'Nữ', 'Nguyen Thi Thu Thuong', 'Quảng Trị', 30, '../../assets/img/avatar.png', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, '01678408880', 'Nam', 'Thi Ca', 'Da Nang', 38, 'ghghghg', '2018-07-04 10:10:04', NULL),
(35, '12345678978', '1', 'Adfasdfasdf', 'Quảng Bình', 64, '../../assets/img/avatar.png', '2018-07-06 10:11:58', NULL);


--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `post_contacts`
--
ALTER TABLE `post_contacts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `post_id_UNIQUE` (`post_id`);

--
-- Chỉ mục cho bảng `provinces`
--
ALTER TABLE `provinces`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `user_infos`
--
ALTER TABLE `user_infos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT cho bảng `districts`
--
ALTER TABLE `districts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=974;

--
-- AUTO_INCREMENT cho bảng `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT cho bảng `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT cho bảng `post_contacts`
--
ALTER TABLE `post_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT cho bảng `provinces`
--
ALTER TABLE `provinces`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT cho bảng `user_infos`
--
ALTER TABLE `user_infos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
