$(document).ready(function() {

    // Delete 
    $('.delete').click(function() {
        var el = this;
        var id = this.id;
        var splitid = id.split();

        // Delete id
        var deleteid = splitid[1];

        // AJAX Request
        $.ajax({
            url: 'controllers/customers/delete_item.php',
            type: 'POST',
            data: {
                id: deleteid
            },
            success: function(response) {

                // Removing row from HTML Table
                $(el).closest('history-post').css('background', 'tomato');
                $(el).closest('history-post').echo "<script>alert('Delete user success');</script>";
                $(el).closest('history-post').fadeOut(800, function() {
                    $(this).remove();
                });

            }
        });

    });

});