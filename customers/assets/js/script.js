$(document).ready(function (e) {
	$("#uploadimage").on('submit',(function(e) {
		e.preventDefault();
		$("#message").empty();
		$.ajax({
			url: "controllers/customers/inforcus.php", // tro den file xu li
			type: "POST",
			data: new FormData(this),
			contentType: false,
			cache:false,
			processData:false,
			success:function(data){
				$("#message").html(data);
			}
		});
	}));

	// function to preview image after validation
	$(function() {
		$("#file").change(function() {
			$("#message").empty();
			var file = this.files[0];
			var imagefile = file.type;
			var match =["image/jpeg", "image/png", "image/jpg"];
			if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
				$('#previewing').attr('src','assets/img/');
				$("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
				return false;
			} else {
				var reader = new FileReader();
				reader.onload = imageIsLoaded;
				reader.readAsDataURL(this.files[0]);
			}
		});
	});

	function imageIsLoaded(e) {
		$("#file").css("color","green");
		$('#image_preview').css("display", "block");
		$('#previewing').attr('src', e.target.result);
		$('#previewing').attr('width', '250px');
		$('#previewing').attr('height', '230px');
	};

	$('.category').change(function(){
		var id = $(this).val();
		if(id!=''){
			$.ajax({
				url: 'direct.php?controller=customer&action=post',
				type: 'POST',
				dataType: "json",
				data: {
					'cate_id':id
				},
				success: function (data) {
					console.log(data);
					JSON.stringify(data);
					if(data == "true"){
						$(".add-info").hide();
						$(".checkbox").hide();
						$("#acreage").hide();
					} else {
						$(".add-info").show();
						$(".checkbox").show();
						$("#acreage").show();
					}  
				}
			});
		}
	});
});