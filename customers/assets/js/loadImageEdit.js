var fileCollection = new Array();
$('#file').on('change', function (e) {

    if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
    {

        var data = $(this)[0].files; //this file data
        var FileType;
        var count = 0;
        $.each(data, function (index, file) { //loop though each file
            if (/(\.|\/)(gif|jpeg|png)$/i.test(file.type)) { //check supported file type
                var fRead = new FileReader(); //new filereader
                fRead.onload = (function (file) { //trigger function on successful read
                    return function (e) {
                        fileCollection.push(file);
                        count = fileCollection.length;
                        var html = '<div class="product-img">';
                        html += '<img src="' + e.target.result + '" class="thumb"/>'; //create image element 
                        html += '</div>';
                        var FileType = data[index].type;
                        // get file extension..
                        var fileExtension = FileType.substr((FileType.lastIndexOf('/') + 1));

                        var Extension = fileExtension.toUpperCase();

                        if (fileCollection.length == 1) {
                            $('.picture').empty();
                        }
                        $('.picture').append(html);
                    };
                })(file);
                fRead.readAsDataURL(file);
            }
        });
    } else {
        alert("Your browser doesn't support File API!"); //if File API is absent
    }
});

$(".picture").on("click", ".rimg", function () {
    var rimg = $(this).attr('data');
    fileCollection.splice(fileCollection.indexOf(fileCollection[rimg]), 1);
    $(this).parent().parent().remove();
    console.log(fileCollection);
    return false;
});

$('#submit').submit(function (e) {
    e.preventDefault();
    var form =$(this);
    var formData = new FormData(form[0]);
    console.log(fileCollection);
    $.each(fileCollection, function(i, obj) {
        formData.append('file['+i+']', obj);
        
    });
//    
$.ajax({
    url: '/direct.php?controller=customer&action=edit',
    type: 'POST',
    data: formData,
    processData: false,
    contentType : false,
    cache: false,
    success: function (data) {
        // console.log(data);
        if(data==1){
            alert('Chỉnh sửa thành công!!!');
            window.location.assign("http://localhost/rongbay/customers/index.php?controller=customer&action=edit");
        }else{
            alert('Lỗi, Vui lòng thử lại!!!');
        }
    }
});

});