<?php

$phone = $fullname = $address = "";
$name = $parent = $acreage = $city = $district = $price = $description = $lay = $balcony = $parent = $license = $unit = "";

if (isset($_GET['id'])) {

    $post_ID  = $_GET['id'];
    $edit     = new Post();
    $result   = $edit->getPostById($post_ID);
    $countImg = $edit->getImg($post_ID);
	
	$row = $result->fetch_assoc();
	$par = $row['category_id'];
	
	$cate = new Category();
	$parent_id = $cate->getParent($par);
	$par1 = $parent_id->fetch_assoc();
	
}

if (isset($_POST["post_ID"])) {
    $model = new Post();
	$post_ID  = $_POST["post_ID"];
    $edit     = new Post();
    $result   = $edit->getPostById($post_ID);
    $countImg = $edit->getImg($post_ID);
	
	$row = $result->fetch_assoc();
	$par = $row['category_id'];
	
	$cate = new Category();
	$parent_id = $cate->getParent($par);
	$par1 = $parent_id->fetch_assoc();
	
    $post_ID     = $_POST["post_ID"];
    $name        = $_POST['name'];
    $city        = $_POST['city'];
    $district    = $_POST['district'];
    $price       = $_POST['price'];
    $description = $_POST['description'];
    $parent      = $_POST['parent'];
    $phone       = $_POST['phone'];
    $fullname    = $_POST['fullname'];
    $unit        = $_POST['unit'];
    $address     = $_POST['address'];
	
	if($par1['parent'] != 1 && $par1['parent'] != 2 && $par1['parent'] != 5){
			$lay = "" ;
			$balcony = "" ;
			$license = "" ;
			$acreage = 0 ;
		} else {
			 $acreage     = $_POST['acreage'];
			$lay         = $_POST['lay'];
			$balcony     = $_POST['balcony'];
			$license     = $_POST['license'];
			if ($acreage == '') {
				echo 'Vui lòng nhập trường Diện Tích!';
				$error = 1;
			} elseif (!is_numeric($acreage)) {
				echo 'Vui lòng nhập số, không nhập chữ cái tại trường Diện Tích!';
				$error = 1;
			}else{
				$acreage = $_POST['acreage'];
			}
		}
   
		
    $error       = 0;

    //VALIDATE
    if ($name == '') {
        echo 'Vui lòng nhập trường Tên Bài Đăng!<br>';
         $error = 1;
    }
    if ($parent == '') {
        echo 'Vui lòng nhập trường Danh Mục!<br>';
        $error=1;
    }
   
    if ($city == '') {
        echo 'Vui lòng chọn trường Thành Phố!<br>';
        $error=1;
    }
    if ($district == '') {
        echo 'Vui lòng nhập trường Quận/Huyện!<br>';
        $error=1;
    }
    if ($price == '') {
        echo 'Vui lòng nhập trường Giá!<br>';
        $error=1;
    } elseif (!is_numeric($price)) {
       echo 'Vui lòng nhập số, không nhập chữ cái tại trường Giá thành!<br>';
       $error=1;
    }
    if ($description == '') {
        echo 'Vui lòng nhập trường Mô tả!<br>';
        $error=1;
    }
    
    if ($phone == '') {
        echo 'Vui lòng nhập trường Số Điện Thoại!<br>';
        $error=1;
    } elseif (!is_numeric($phone)) {
        echo 'Vui lòng nhập số, không nhập chữ cái tại trường Số Điện Thoại!<br>';
        $error=1;
    } elseif (strlen($phone) < 10 || strlen($phone) > 11) {
        echo  'Vui lòng nhập 10 hoặc 11 chữ số tại trường Số Điện Thoại!<br>';
        $error=1;
    }
    if ($fullname == '') {
       echo  'Vui lòng nhập trường Tên Người Đăng Tin!<br>';
       $error=1;
    }
    if ($address == '') {
       echo 'Vui lòng nhập trường Địa chỉ!<br>';
       $error=1;
    }
if($error==1){
    die;
}
        if (isset($_FILES["file"]['name'][0]) && !empty($_FILES["file"]['name'][0])) {
            $groupFile = $_FILES["file"];
            
            $sub_folder = '/' . date('Y') . '/' . date('m') . '/' . date('d');

            $update = new Post();
            $result = $update->updatePost($post_ID, $address, $name, $acreage, $city, $district, $price, $description, $lay, $balcony, $parent, $license, $unit);
            $result2 = $update->updatePostContact($post_ID, $phone, $fullname);
            $result1 = $update->deleteImg($post_ID);
            for ($i = 0; $i < count($groupFile['name']); $i++) {
                $duoi = explode('.', $groupFile['name'][$i]); // tách chuỗi khi gặp dấu .
                
                $duoi = $duoi[(count($duoi) - 1)]; //lấy ra đuôi file
                if ($duoi === 'jpg' || $duoi === 'png' || $duoi === 'gif' || $duoi === 'jpeg') {
                    if (!is_dir("../libraries/images/posts" . $sub_folder))
                        if (!mkdir("../libraries/images/posts" . $sub_folder, 0777, true))
                            return FALSE;
                    $upload_url = SITE_ROOT . '/libraries/images/posts' . $sub_folder;
                    $img = $upload_url . '/' . $groupFile['name'][$i];
                    $image = 'libraries/images/posts' . $sub_folder . '/' . $groupFile['name'][$i];
                    $check = move_uploaded_file($groupFile['tmp_name'][$i], $img);
                    if ($check) {
                        $result3 = $update->addImg($post_ID, $image);
                    } else {
//                        echo "<script>alert('Lỗi trong quá trình update file, vui lòng thử lại!');window.location='index.php?controller=post&action=edit&id=$post_ID'</script>";
                        echo 'Lỗi trong quá trình update file, vui lòng thử lại!';
                        die;
                    }
                } else {
                    echo 'Sai định dạng file! Vui lòng thử lại!!';die;
//                    echo "<script>alert('Sai định dạng file! Vui lòng thử lại');window.location='index.php?controller=post&action=edit&id=$post_ID'</script>";
                }
            }
            echo 1;die;
//            echo "<script>alert('Chỉnh sửa thành công!');window.location='index.php?controller=post&action=listed'</script>";die;
        } else {
            $result = $model->updatePost($post_ID, $address, $name, $acreage, $city, $district, $price, $description, $lay, $balcony, $parent, $license, $unit);
            $result2 = $model->updatePostContact($post_ID, $phone, $fullname);
            if($result2){
                echo 1;die;
            }else{
                echo 'Lỗi, Vui lòng thử lại !!!';die;
            }
        }
    }
	$model = new Post();

//print_r($row)->fetch_assoc();die;
$list_parent = $model->getlistparent();
$list_province = $model->getListProvince();
$list_district = $model->getListDistrict($row['city_id']); //get district với city_id
include 'views/posts/edit_post_view.php';

?>
