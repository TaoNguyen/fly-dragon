<?php

if (isset($_POST['id'])) {
    $id_parent = $_POST['id'];
    $getP = new Category();
    $return = $getP->getParent($id_parent);
    $row = $return->fetch_assoc();
    if ($row['parent'] != 1 && $row['parent'] != 2 && $row['parent'] != 5) {
        $return = "true";
    } else {
        $return = 0;
    }
    echo json_encode($return); die;
}
    if (isset($_POST["name"])) {
        $name = $_POST['name'];
        $city = $_POST['city'];
        $district = $_POST['district'];
        $price = $_POST['price'];
        $description = $_POST['description'];
        $parent = $_POST['parent'];
        $phone = $_POST['phone'];
        $email = $_POST['email'];
        $fullname = $_POST['fullname'];
        $unit = $_POST['unit'];
        $address = $_POST['address'];
        $lay = $_POST['lay'];
        $balcony = $_POST['balcony'];
        $license = $_POST['license'];
		
		$cate = new Category();
		$parent_id = $cate->getParent($parent);
		$par = $parent_id->fetch_assoc();
		
		if($par['parent'] != 1 && $par['parent'] != 2 && $par['parent'] != 5){
			$lay = "" ;
			$balcony = "" ;
			$license = "" ;
			$acreage = 0 ;
		} else {
			if ($acreage == '') {
				echo 'Vui lòng nhập trường Diện Tích!';
				$error = 1;
			} elseif (!is_numeric($acreage)) {
				echo 'Vui lòng nhập số, không nhập chữ cái tại trường Diện Tích!';
				$error = 1;
			}else{
				$acreage = $_POST['acreage'];
			}
		}
		
        $error = 0;
        //VALIDATE
        if ($name == '') {
            echo 'Vui lòng nhập trường Tên Bài Đăng!';
            $error = 1;
        }
        if ($parent == '') {
            echo 'Vui lòng nhập trường Danh Mục!';
            $error = 1;
        }
        
        if ($city == '') {
            echo 'Vui lòng chọn trường Thành Phố!';
            $error = 1;
        }
        if ($district == '') {
            echo 'Vui lòng nhập trường Quận/Huyện!';
            $error = 1;
        }
        if ($price == '') {
            echo 'Vui lòng nhập trường Giá!';
            $error = 1;
        } elseif (!is_numeric($price)) {
            echo 'Vui lòng nhập số, không nhập chữ cái tại trường Giá thành!';
            $error = 1;
        }
        if ($description == '') {
            echo 'Vui lòng nhập trường Mô tả!';
            $error = 1;
        }
        if ($phone == '') {
            echo 'Vui lòng nhập trường Số Điện Thoại!';
            $error = 1;
        } elseif (!is_numeric($phone)) {
            echo 'Vui lòng nhập số, không nhập chữ cái tại trường Số Điện Thoại!';
            $error = 1;
        } elseif (strlen($phone) < 10 || strlen($phone) > 11) {
            echo 'Vui lòng nhập 10 hoặc 11 chữ số tại trường Số Điện Thoại!';
            $error = 1;
        }
        if ($email == '') {
            echo 'Vui lòng nhập trường Email!';
            $error = 1;
        }
        if ($fullname == '') {
            echo 'Vui lòng nhập trường Tên Người Đăng Tin!';
            $error = 1;
        } elseif (is_numeric($fullname)) {
            echo 'Vui lòng nhập chữ, không nhập số tại trường Tên người đăng tin!';
            $error = 1;
        }
        if ($address == '') {
            echo 'Vui lòng nhập trường Địa chỉ!';
            $error = 1;
        }
        if ($error == 1) {
            die;
        }

        if (!is_dir("../libraries/images/posts"))
            mkdir("../libraries/images/posts", 0777, true);
        if (isset($_FILES["file"]['name'][0]) && !empty($_FILES["file"]['name'][0])) {
            $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
            $sub_folder = '/' . date('Y') . '/' . date('m') . '/' . date('d');
            $groupFile = $_FILES["file"];
            $model = new Post();
            $post_id = $model->addPost($name, $acreage, $city, $district, $price, $description, $lay, $balcony, $parent, $license, $unit, $address);
            $addPostCont = $model->addPostContact($phone, $email, $fullname, $post_id);
            for ($i = 0; $i < count($groupFile['name']); $i++) {
                $ext = pathinfo($groupFile['name'][$i], PATHINFO_EXTENSION);

                if (!array_key_exists($ext, $allowed)) {
                    $_SESSION['error'][] = 'Vui lòng chọn đúng định dạng file!';
                }
                if ($groupFile['size'][$i] > 51048576) {
                    $_SESSION['error'][] = 'Kích thước file lớn hơn giới hạn cho phép, vui lòng chọn lại!';
                }
                if (in_array($groupFile['type'][$i], $allowed)) {
                    if (!is_dir("../libraries/images/posts" . $sub_folder))
                        if (!mkdir("../libraries/images/posts" . $sub_folder, 0777, true))
                            return FALSE;
                    $upload_url = SITE_ROOT . '/libraries/images/posts' . $sub_folder;
                    $img = $upload_url . '/' . $groupFile['name'][$i];
                    $image = 'libraries/images/posts' . $sub_folder . '/' . $groupFile['name'][$i];
                    $check = move_uploaded_file($groupFile['tmp_name'][$i], $img);
                    if ($check) {
                        $addImg = $model->addImage($image, $post_id);
                    } else {
                        echo 'Lỗi : Có vấn đề xảy ra khi upload file';
                        die;
                    }
                } else {
                    echo 'Lỗi : Có vấn đề xảy ra khi upload file';
                    die;
                }
            }echo 1;
            die;
        } else {
            $post_id = $model->addPost($name, $acreage, $city, $district, $price, $description, $lay, $balcony, $parent, $license, $unit, $address);
            $addPostCont = $model->addPostContact($phone, $email, $fullname, $post_id);
            if ($addPostCont) {
                echo 1;
                die;
            } else {
                echo 'Lỗi, vui lòng thử lại!';
                die;
            }
        }
    }

$model = new Post();
$list_parent = $model->getlistparent();
$list_province = $model->getListProvince();
include 'views/posts/add_post_view.php';
?>