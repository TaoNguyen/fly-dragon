<?php
$name = $address = $phone= $gender  = $password = "";

if(isset($_GET['id'])){
    
    $userID = $_GET['id'];
//    echo $userID;die;
    $edit = new User();
    $result = $edit->getUserbyId($userID);
  }
if(isset($_POST["update"])) // Kiểm tra xem đã click vào nút button hay chưa
{
    // Set các biến thành cho các giá trị bên form
    $name = $_POST['name'];
    $phone = $_POST['phone'];
    $address = $_POST['address'];
    $gender = $_POST['gender'];
    $password = $_POST['password'];
    //VALIDATE
    if ($name == '') {
        $_SESSION['error'][] = 'Vui lòng nhập trường tên!';
    }
    if ($phone == '') {
        $_SESSION['error'][] = 'Vui lòng nhập trường số điện thoại!';
    } elseif (!is_numeric($phone)) {
        $_SESSION['error'][] = 'Vui lòng nhập số, không nhập chữ cái tại trường Số Điện Thoại!';
    } elseif (strlen($phone) < 10 || strlen($phone) > 11) {
        $_SESSION['error'][] = 'Vui lòng nhập 10 hoặc 11 chữ số tại trường Số Điện Thoại!';
    }
    if ($address == '') {
        $_SESSION['error'][] = 'Vui lòng nhập trường địa chỉ!';
    }
    if ($gender == '') {
        $_SESSION['error'][] = 'Vui lòng nhập trường giới tính!';
    }
    if ($password == '') {
        $_SESSION['error'][] = 'Vui lòng nhập trường mật khẩu!';
    } elseif (strlen($password) < 6) {
        $_SESSION['error'][] = 'Mật khẩu ít nhất 6 kí tự!';
    }
    
    
    if (!isset($_SESSION['error']) && empty($_SESSION['error'])) {
        if (!is_dir("../libraries/images/users"))
            mkdir("../libraries/images/users", 0777, true);
        if (isset($_FILES["file"]["name"]) && !empty($_FILES["file"]["name"])) {
            $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
            $filename = $_FILES["file"]["name"];
            $filetype = $_FILES["file"]["type"];
            $filesize = $_FILES["file"]["size"];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if (!array_key_exists($ext, $allowed)) {
                echo "<script>alert('Vui lòng chọn đúng định dạng file!');window.location='index.php?controller=user&action=edit&id=$userID'</script>";
            }
            if ($filesize > 51048576) {
                echo "<script>alert('Kích thước file lớn hơn giới hạn cho phép!');window.location='index.php?controller=user&action=edit&id=$userID'</script>";
            }
            if (in_array($filetype, $allowed)) {
                $year = date('Y');
                $month = date('m');
                $day = date('d');
                $sub_folder = '/' . $year . '/' . $month . '/' . $day;
                if (!is_dir("../libraries/images/users" . $sub_folder))
                    if (!mkdir("../libraries/images/users" . $sub_folder, 0777, true))
                        return FALSE;
                $upload_url = SITE_ROOT . '/libraries/images/users' . $sub_folder;
                $img = $upload_url . '/' . $filename;
                $image = 'libraries/images/users' . $sub_folder . '/' . $filename;
                $check = move_uploaded_file($_FILES["file"]["tmp_name"], $img);
                if (!$check) {
                    echo "<script>alert('Chỉnh sửa ảnh lỗi. Vui lòng thử lại!!!');window.location='index.php?controller=user&action=edit&id=$userID'</script>";
                    die;
                }
            } else {
                echo "<script>alert('Lỗi: Có vấn đề xảy ra khi upload file. Vui lòng thử lại!!!');window.location='index.php?controller=user&action=edit&id=$userID'</script>";
            }
        } else {
            $image = '';
        }
        $update = new User();
        $result = $update->updateUser($userID, $password);
        $result2 = $update->updateInfo($userID, $name, $phone, $address, $gender, $image);
        echo "<script>alert('Chỉnh sửa thông tin người dùng thành công!');window.location='index.php?controller=user&action=listed'</script>";
        die;
    }
}

include 'views/users/edit_view.php';
?>

