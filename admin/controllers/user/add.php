<?php

$name = $phone = $address = $email = $gender = $password = $repass = "";
if (isset($_POST["add"])) { // Kiểm tra xem đã click vào nút button hay chưa
    // Set các biến thành cho các giá trị bên form
    $name = $_POST['name'];
    $phone = $_POST['phone'];
    $email = $_POST['email'];
    $address = $_POST['address'];
    $gender = $_POST['gender'];
    $password = $_POST['password'];
    $repass = $_POST['repass'];
    //VALIDATE
    if ($name == '') {
        $_SESSION['error'][] = 'Vui lòng nhập trường Tên!';
    }
    if ($phone == '') {
        $_SESSION['error'][] = 'Vui lòng nhập trường SĐT!';
    } elseif (!is_numeric($phone)) {
        $_SESSION['error'][] = 'Vui lòng nhập số, không nhập chữ cái tại trường Số Điện Thoại!';
    } elseif (strlen($phone) < 10 || strlen($phone) > 11) {
        $_SESSION['error'][] = 'Vui lòng nhập 10 hoặc 11 chữ số tại trường Số Điện Thoại!';
    }
    if ($email == '') {
        $_SESSION['error'][] = 'Vui lòng nhập trường email!';
    }
    if ($address == '') {
        $_SESSION['error'][] = 'Vui lòng nhập trường Địa chỉ!';
    }
    if ($gender == '') {
        $_SESSION['error'][] = 'Vui lòng nhập trường Giới Tính!';
    }
    if ($password == '') {
        $_SESSION['error'][] = 'Vui lòng nhập trường Mật Khẩu!';
    } elseif (strlen($password) < 6) {
        $_SESSION['error'][] = 'Mật khẩu ít nhất 6 kí tự!';
    }
    if ($password != $repass) {
        $_SESSION['error'][] = 'Mật khẩu và nhập lại Mật khẩu không khớp!';
    }
    if (!isset($_SESSION['error']) && empty($_SESSION['error'])) {
        if (!is_dir("../libraries/images/users"))
            mkdir("../libraries/images/users", 0777, true);
        if (isset($_FILES["file"]) && !empty($_FILES["file"])) {
            
            $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
            $filename = $_FILES["file"]["name"];
            $filetype = $_FILES["file"]["type"];
            $filesize = $_FILES["file"]["size"];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if (!array_key_exists($ext, $allowed)) {
                $_SESSION['add'] = "Vui lòng chọn đúng định dạng file!";
            }
            if ($filesize > 5242880) {
                $_SESSION['add'] = "Kích thước file lớn hơn giới hạn cho phép!";
            }
            if (in_array($filetype, $allowed)) {
                $year = date('Y');
                $month = date('m');
                $day = date('d');
                $sub_folder = '/' . $year . '/' . $month . '/' . $day;
                if (!is_dir("../libraries/images/users" . $sub_folder))
                    if (!mkdir("../libraries/images/users" . $sub_folder, 0777, true))
                        return FALSE;
                $upload_url = SITE_ROOT . '/libraries/images/users' . $sub_folder;
                $img = $upload_url . '/' . $filename;
                $image = 'libraries/images/users' . $sub_folder . '/' . $filename;
                $check = move_uploaded_file($_FILES["file"]["tmp_name"], $img);
                if (!$check) {
                    $_SESSION['add'] = "Lỗi";
                    header("Location:index.php?controller=user&action=listed");
                }
            } else {
                $_SESSION['add'] = "Lỗi: Có vấn đề xảy ra khi upload file";
            }
        } else {
            $image = '';
        }
        $checkEmail = new User();
        $result = $checkEmail->checkEmail($email);
        $count = mysqli_num_rows($result);
        if ($count == 0) {
            $add = new User();
            $user_id = $add->addUser($email, $password);
            $result2 = $add->addUserInfo($name, $phone, $address, $gender, $user_id, $image);
            $_SESSION['add'] = "Thêm mới thành công!";
            header("Location:index.php?controller=user&action=listed");
            die;
        } else {
            $_SESSION['error']['$email'] = 'Email đã tồn tại, vui lòng chọn Email khác!';
            header("Location:index.php?controller=user&action=add");
        }
    }
}
include 'views/users/add_view.php';
?>