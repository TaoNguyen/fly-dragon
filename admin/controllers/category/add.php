<?php
$category_model = new Category();
$name = $parent = $images = "";
if(isset($_POST["add"])) { // Kiểm tra xem đã click vào nút button hay chưa
    $name = $_POST['name'];
    $parent = $_POST['parent'];

    //VALIDATE
    if ($name == '') {
        $_SESSION['error'][] = 'Vui lòng nhập trường Tên!';
    }
    if ($parent == '') {
        $_SESSION['error'][] = 'Vui lòng nhập trường Danh Mục!';
    }
    if (!isset($_SESSION['error']) && empty($_SESSION['error'])) {
        if (!is_dir("../libraries/images/categories"))
            mkdir("../libraries/images/categories", 0777, true);
        if (isset($_FILES["file"]) && !empty($_FILES["file"])) {
            $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
            $filename = $_FILES["file"]["name"];
            $filetype = $_FILES["file"]["type"];
            $filesize = $_FILES["file"]["size"];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if (!array_key_exists($ext, $allowed)) {
                $_SESSION['add'] = "Vui lòng chọn đúng định dạng file!";
            }
            if ($filesize > 51048576) {
                $_SESSION['add'] = "Kích thước file lớn hơn giới hạn cho phép!";
            }
            if (in_array($filetype, $allowed)) {
                $year = date('Y');
                $month = date('m');
                $day = date('d');
                $sub_folder = '/' . $year . '/' . $month . '/' . $day;
                if (!is_dir("../libraries/images/categories" . $sub_folder))
                    if (!mkdir("../libraries/images/categories" . $sub_folder, 0777, true))
                        return FALSE;
                $upload_url = SITE_ROOT . '/libraries/images/categories' . $sub_folder;
                $img = $upload_url . '/' . $filename;
                $image = 'libraries/images/categories' . $sub_folder . '/' . $filename;
                
                $check = move_uploaded_file($_FILES["file"]["tmp_name"], $img);
                if (!$check) {
                    $_SESSION['add'] = "Lỗi";
                    header("Location:index.php?controller=category&action=listed");
                }
            } else {
                $_SESSION['add'] = "Lỗi: Có vấn đề xảy ra khi upload file";
            }
        } else {
            $image = '';
        }
        
        $checkName = new Category();
        $result = $checkName->checkNameP($name);
        $count = mysqli_num_rows($result);
        
        if ($count == 0) {
            $add = new Category();
            $addCate = $add->addCategory($name, $parent, $image);
            $_SESSION['add'] = "Thêm mới danh mục thành công!";
            header("Location:index.php?controller=category&action=listed");
            die;
        } else {
            $_SESSION['error']['$email'] = 'Danh mục đã TỒN TẠI, vui lòng chọn Email khác!';
            header("Location:index.php?controller=category&action=add");
        }
    }   
}
$list_parent=$category_model->getlistparent();
include 'views/categories/add_view.php';
?>