<?php

$category_model = new Category();
$name = $parent = "";

if (isset($_GET['id'])) {

    $cate_ID = $_GET['id'];
//    $sql = "SELECT * FROM admin WHERE id = $id_ad";
//    $edit = $conn->query($sql);
    $edit = new Category();
    $result = $edit->getCategorybyId($cate_ID);
}

if (isset($_POST["update"])) {
    $name = $_POST['name'];
    $parent = $_POST['parent'];

    //VALIDATE
    if ($name == '') {
        $_SESSION['error'][] = 'Vui lòng nhập trường Tên danh mục!';
    }
    if ($parent == '') {
        $_SESSION['error'][] = 'Vui lòng nhập trường Danh Mục!';
    }
    if (!isset($_SESSION['error']) && empty($_SESSION['error'])) {
        
        if (!is_dir("../libraries/images/categories"))
            mkdir("../libraries/images/categories", 0777, true);
        if (isset($_FILES["file"]) && !empty($_FILES["file"])) {
            $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
            $filename = $_FILES["file"]["name"];
            $filetype = $_FILES["file"]["type"];
            $filesize = $_FILES["file"]["size"];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if (!array_key_exists($ext, $allowed)) {
                echo "<script>alert('Vui lòng chọn đúng định dạng file!');window.location='index.php?controller=category&action=edit&id=$cate_ID'</script>";
            }
            if ($filesize > 51048576) {
                echo "<script>alert('Kích thước file lớn hơn giới hạn cho phép!');window.location='index.php?controller=category&action=edit&id=$cate_ID'</script>";
            }
            if (in_array($filetype, $allowed)) {
                $year = date('Y');
                $month = date('m');
                $day = date('d');
                $sub_folder = '/' . $year . '/' . $month . '/' . $day;
                if (!is_dir("../libraries/images/categories" . $sub_folder))
                    if (!mkdir("../libraries/images/categories" . $sub_folder, 0777, true))
                        return FALSE;
                $upload_url = SITE_ROOT . '/libraries/images/categories' . $sub_folder;
                $img = $upload_url . '/' . $filename;
                $image = 'libraries/images/categories' . $sub_folder . '/' . $filename;
                $check = move_uploaded_file($_FILES["file"]["tmp_name"], $img);
                if (!$check) {
                    echo "<script>alert('Chỉnh sửa ảnh lỗi. Vui lòng thử lại!!!');window.location='index.php?controller=category&action=edit&id=$cate_ID'</script>";
                    die;
                }
            } else {
                echo "<script>alert('Lỗi: Có vấn đề xảy ra khi upload file. Vui lòng thử lại!!!');window.location='index.php?controller=category&action=edit&id=$cate_ID'</script>";
            }
        } else {
            $image = '';
        }
        
        $update = new Category();
        $result = $update->checkNameCategory($name, $cate_ID);
        $count = mysqli_num_rows($result);
        if ($count== 0) {
            $update = new Category();
            $result = $update->updateCategory($cate_ID, $name, $parent, $image);
            if ($result) {
                echo "<script>alert('Chỉnh sửa thông tin thành công!');window.location='index.php?controller=category&action=listed'</script>";
            } else {
                echo "<script>alert('Chỉnh sửa KHÔNG THÀNH CÔNG. Vui lòng thử lại!');window.location='index.php?controller=category&action=edit&id=$cate_ID'</script>";
            }
        } else {
            echo "<script>alert('Tên danh mục TỒN TẠI. Vui lòng thử lại!');window.location='index.php?controller=category&action=edit&id=$cate_ID'</script>";
            die;
        }
    }
}

$list_parent = $category_model->getlistparent();
include 'views/categories/edit_view.php';
?>
