<?php if ($_SESSION['user'] && $_SESSION['pass']) {
            
    ?>
    <div class="page">
        <header class="header">
            <nav class="navbar">
         
                <div class="container-fluid">
                    <div class="navbar-holder d-flex align-items-center justify-content-between">
                        <div class="navbar-header">
                            <a href="#" class="navbar-brand"></a>
                            <div class="brand-text brand-big"><strong >Quản Lí Bài Đăng</strong></div>
                        </div>

                        <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                            <li class="nav-item d-flex align-items-center"><a id="search" href="#"></a></li>
                            <li class="nav-item dropdown"> </li>
                            <li class="nav-item dropdown">
                                <a id="messages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="fa fa-envelope-o"></i><span class="badge bg-orange">10</span></a>
                                <ul aria-labelledby="notifications" class="dropdown-menu">
                                    <li><a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                                        <div class="msg-profile"></div>

                                        <div class="msg-body">
                                            <h3 class="h5">Jason Doe</h3><span>Sent You Message</span>
                                        </div></a>
                                    </li>

                                    <li><a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                                        <div class="msg-profile"></div>
                                        <div class="msg-body">
                                            <h3 class="h5">Frank Williams</h3><span>Sent You Message</span>
                                        </div></a>
                                    </li>

                                    <li><a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                                        <div class="msg-profile"></div>
                                        <div class="msg-body">
                                            <h3 class="h5">Ashley Wood</h3><span>Sent You Message</span>
                                        </div></a>
                                    </li>

                                    <li><a rel="nofollow" href="#" class="dropdown-item all-notifications text-center"> <strong>Read all messages</strong></a></li>
                                </ul>
                            </li>
                            
                            <li class="nav-item"><a href="index.php?controller=user&action=logout" class="nav-link logout">Đăng Xuất<i class="fa fa-sign-out"></i></a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        
        <div class="page-content d-flex align-items-stretch"> 
            <nav class="side-navbar">
                <div class="sidebar-header d-flex align-items-center">
                    <div class="avatar"></div>
                    <div class="title"> <h1 class="h4"><?php echo ucwords( $_SESSION["user"])?></h1> <p>Administrator</p></div>
                </div>
                
                <span class="heading">Quản Lí Danh Mục</span>
                <ul class="list-unstyled">
                    <li><a href="index.php?controller=category&action=listed"> <i class="icon-website"></i>Quản Lí Danh Mục</a></li>
                    <li class="active"><a href="index.php?controller=post&action=listed"> <i class="icon-website"></i>Quản Lí Bài Đăng</a></li>
                    <li><a href="index.php?controller=user&action=listed"> <i class="icon-grid"></i>Quản Lí Người Dùng</a></li>
                    <li><a href="index.php?controller=admin&action=listed"> <i class="icon-user"></i>Quản Trị Viên</a></li>
                    <li><a href="index.php?controller=user&action=login"> <i class="icon-interface-windows"></i>Đăng Nhập </a></li>
                </ul>
                <span class="heading">Extras</span>
            </nav>
            
            <div class="content-inner">
                <header class="page-header">
                    <div class="container-fluid">
                        <h2 class="no-margin-bottom">Quản Lí Bài Đăng</h2>
                    </div>
                </header>
                
                <div class="breadcrumb-holder container-fluid">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Quản Lí Bài Đăng</a></li>
                        <li class="breadcrumb-item active"><a href="index.php?controller=post&action=listed">Danh Sách Bài Đăng</a></li>              
                        <li class="breadcrumb-item active">Thêm mới Bài Đăng</li>
                    </ul>
                </div>
                <section class="tables">   
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="msg-error" style="color: red; margin-left: 80px;">
                                        <p style="color: red;">*Vui lòng không để trường trống...</p>
                                    <?php
                                    if(isset($_SESSION['error'])){
                                        $error=$_SESSION['error'];
//                                        echo '<pre>';
//                                        print_r($error);
//                                        echo '</pre>';
                                        foreach ($error as $msg){
                                            echo $msg.'<br>';
                                        }
                                        unset($_SESSION['error']);
                                    }
                                   
                                        if (isset($_SESSION['add'])) {
                                            $mess = $_SESSION['add'];
                                            echo $mess;
                                        }unset($_SESSION['add']);
                                    ?>
                                    </div>
                                    <form action="" id="addpost" method="POST" role="form" enctype="multipart/form-data" style="margin-left: 50px;" >
                                        <div class="form-group" style="margin-top: 20px;">
                                            <label class="control-label col-sm-5" for="name">Tên Bài Đăng</label>
                                            <div class="col-sm-10">                                         
                                                <input type="text" class="form-control" id="name" placeholder="Tên bài đăng" name="name">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group" style="margin-top: 20px;">
                                            <label class="control-label col-sm-5" for="fullname">Tên Người Đăng</label>
                                            <div class="col-sm-10">                                         
                                                <input type="text" class="form-control" id="fullname" placeholder="Tên người đăng" name="fullname">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group" style="margin-top: 20px;">
                                            <label class="control-label col-sm-5" for="email">Email</label>
                                            <div class="col-sm-10">                                         
                                                <input type="email" required class="form-control" id="email" placeholder="Nhập Email" name="email">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group" style="margin-top: 20px;">
                                            <label class="control-label col-sm-5" for="phone">Số Điện thoại Liên Hệ</label>
                                            <div class="col-sm-10">                                         
                                                <input type="text" class="form-control" id="phone" placeholder="Nhập số điện thoại" name="phone">
                                            </div>
                                        </div>
                                        
										<div class="form-group">
                                            <label class="control-label col-sm-2" for="">Thuộc Danh Mục</label>
                                            <div class="col-sm-10">
                                                <select class="form-control category" name="parent">
                                                    
                                                    <?php
                                                    
                                                    if($list_parent->num_rows!=0){
                                                        while ($row_list = $list_parent->fetch_array()) {
                                                            echo '<option value="'.$row_list['id'].'">'.$row_list['category_name'].'</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
										
                                        <div class="form-group acreage" style="margin-top: 20px;">
                                            <label class="control-label col-sm-5" for="acreage">Diện Tích</label>
                                            <div class="col-sm-10">                                         
                                                <input type="text" class="form-control" id="acreage" placeholder="Nhập Diện Tích (m2)" name="acreage">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group" style="margin-top: 20px;">
                                            <label class="control-label col-sm-5" for="city">Thành Phố</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" name="city" id="city">
                                                    <option value=""> -- Chọn tên thành phố -- </option>
                                                    <?php
                                                        while ($row_list_province = $list_province->fetch_array()) {
                                                            echo '<option value="'.$row_list_province['id'].'">'.$row_list_province['name'].'</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group" style="margin-top: 20px;">
                                            <label class="control-label col-sm-5" for="district">Quận/Huyện</label>
                                            <div class="col-sm-10" id="district_select">
                                                <select class="form-control" name="district">
                                                    <option>NONE</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group" style="margin-top: 20px;">
                                            <label class="control-label col-sm-5" for="address">Địa chỉ</label>
                                            <div class="col-sm-10">                                         
                                                <input type="text" class="form-control" placeholder="Nhập địa chỉ" name="address">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group" style="margin-top: 20px;">
                                            <label class="control-label col-sm-5" for="price">Giá thành</label>
                                            <div class="col-sm-10">                                         
                                                <input type="text" class="form-control" id="price" placeholder="Nhập giá" name="price">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group" style="margin-top: 20px;">
                                            <label class="control-label col-sm-5" for="unit">Đơn vị</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" name="unit">
                                                    <option value="Tỷ">Tỷ</option>
                                                    <option value="Triệu">Triệu</option>
                                                    <option value="Triệu/m2">Triệu/m2</option>
                                                    <option value="Triệu/tháng">Triệu/tháng</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group" style="margin-top: 20px;">
                                            <label class="control-label col-sm-5" for="description">Mô Tả</label>
                                            <div class="col-sm-10">
                                                <textarea type="text" class="form-control" id="description" placeholder="Mô tả" name="description"></textarea>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group choose-one" style="margin-top: 20px;">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label class="control-label col-sm-5" for="lay">Hướng Cửa Chính</label>
                                                    <div class="col-sm-10">
                                                        <label class="laya">Đông
                                                            <input type="radio" checked="checked" name="lay" value="Đông">
                                                            <span class="lay"></span>
                                                        </label>
                                                        <label class="laya">Tây Bắc
                                                            <input type="radio" name="lay" value="Tây Bắc">
                                                            <span class="lay"></span>
                                                        </label>
                                                        <label class="laya">Tây Nam
                                                            <input type="radio" name="lay" value="Tây Nam">
                                                            <span class="lay"></span>
                                                        </label>
                                                        <label class="laya">Tây
                                                            <input type="radio" name="lay" value="Tây">
                                                            <span class="lay"></span>
                                                        </label>
                                                        <label class="laya">Nam
                                                            <input type="radio" name="lay" value="Nam">
                                                            <span class="lay"></span>
                                                        </label>
                                                        <label class="laya">Đông Bắc
                                                            <input type="radio" name="lay" value="Đông Bắc">
                                                            <span class="lay"></span>
                                                        </label>
                                                        <label class="laya">Bắc
                                                            <input type="radio" name="lay" value="Bắc">
                                                            <span class="lay"></span>
                                                        </label>
                                                        <label class="laya">Đông Nam
                                                            <input type="radio" name="lay" value="Đông Nam">
                                                            <span class="lay"></span>
                                                        </label>
                                                        <!--<input type="text" class="form-control" id="lay" placeholder="Nhập Hướng chính" name="lay">-->
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <label class="control-label col-sm-5" for="balcony">Lợi ích</label>
                                                    <div class="col-sm-10">
                                                        <label class="laya">Gần trường
                                                            <input type="radio" checked="checked" name="balcony" value="Gần trường">
                                                            <span class="lay"></span>
                                                        </label>
                                                        <label class="laya">Gần bệnh viện
                                                            <input type="radio" name="balcony" value="Gần bệnh viện">
                                                            <span class="lay"></span>
                                                        </label>
                                                        <label class="laya">Gần chợ/siêu thị
                                                            <input type="radio" name="balcony" value="Gần chợ/siêu thị">
                                                            <span class="lay"></span>
                                                        </label>
                                                        <label class="laya">Gần bến tàu/xe
                                                            <input type="radio" name="balcony" value="Gần bến tàu/xe">
                                                            <span class="lay"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group choose-one" style="margin-top: 20px;">
                                            <label class="control-label col-sm-5" for="license">Giấy tờ pháp lý</label>
                                            <div class="col-sm-10">
                                                <label class="laya">Có sổ hồng
                                                    <input type="radio" checked="checked" name="license" value="Có sổ hồng">
                                                    <span class="lay"></span>
                                                </label>
                                                <label class="laya">Hợp đồng mua bán
                                                    <input type="radio" name="license" value="Hợp đồng mua bán">
                                                    <span class="lay"></span>
                                                </label>
                                                <label class="laya">Văn phòng chuyển nhượng
                                                    <input type="radio" name="license" value="Văn phòng chuyển nhượng">
                                                    <span class="lay"></span>
                                                </label>
                                                <label class="laya">Khác
                                                    <input type="radio" name="license" value="Khác">
                                                    <span class="lay"></span>
                                                </label>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="picture"></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="">Hình Ảnh</label>
                                            <div class="col-sm-10">
                                                <input type="file" class="form-control" id="file" name="file[]" multiple="">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group"> 
                                            <div class="row">
                                                <div class="col-sm-offset-2 col-sm-2">
                                                    <button type="submit" class="btn btn-default" name="add">Thêm Bài Đăng</button>
                                                </div>
                                                <div class="col-sm-10">
                                                    <a href="index.php?controller=post&action=listed"><button type="button" class="btn btn-default">Quay Lại </button></a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                
                <!-- Page Footer-->
                <footer class="main-footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6">  <p><a href="">Quản Lí Danh Mục</a></p></div>
                            <div class="col-sm-6 text-right">
                               <p><?php 
                                    $now = new DateTime();
                                    echo $now->format('Y-m-d H:i');
                                ?></p>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </div>
<?php
} else {
    header('location: index.php?controller=user&action=login'); //bỏ lại link chuẩn ở đây
}
?>