<?php
if ($_SESSION['user'] && $_SESSION['pass']) {
    ?>
    <div class="page">
        <header class="header">
            <nav class="navbar">
                <div class="container-fluid">
                    <div class="navbar-holder d-flex align-items-center justify-content-between">
                        <div class="navbar-header">
                            <a href="#" class="navbar-brand"></a>
                            <div class="brand-text brand-big"><strong >Quản Lí Bài Đăng</strong></div>
                        </div>

                        <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                            <li class="nav-item d-flex align-items-center"><a id="search" href="#"></a></li>
                            <li class="nav-item dropdown">  </li>

                            <li class="nav-item dropdown"> <a id="messages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="fa fa-envelope-o"></i><span class="badge bg-orange">10</span></a>
                                <ul aria-labelledby="notifications" class="dropdown-menu">
                                    <li><a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                                            <div class="msg-profile"></div>
                                            <div class="msg-body"><h3 class="h5">Jason Doe</h3><span>Sent You Message</span></div></a>
                                    </li>

                                    <li><a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                                            <div class="msg-profile"></div>
                                            <div class="msg-body">
                                                <h3 class="h5">Frank Williams</h3><span>Sent You Message</span>
                                            </div></a>
                                    </li>

                                    <li><a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                                            <div class="msg-profile"></div>
                                            <div class="msg-body">
                                                <h3 class="h5">Ashley Wood</h3><span>Sent You Message</span>
                                            </div></a>
                                    </li>

                                    <li><a rel="nofollow" href="#" class="dropdown-item all-notifications text-center"> <strong>Read all messages    </strong></a></li>
                                </ul>
                            </li>
                            <li class="nav-item"><a href="/rongbayproject/admin/index.php?controller=user&action=logout" class="nav-link logout">Đăng Xuất<i class="fa fa-sign-out"></i></a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>

        <div class="page-content d-flex align-items-stretch"> 
            <nav class="side-navbar">
                <div class="sidebar-header d-flex align-items-center">
                    <div class="avatar"></div>
                    <div class="title">
                        <h1 class="h4"><?php echo ucwords( $_SESSION["user"])?></h1>
                        <p>Administrator</p>
                    </div>
                </div>

                <span class="heading">Quản Lí Bài Đăng</span>
                <ul class="list-unstyled">
                    <li><a href="index.php?controller=category&action=listed"> <i class="icon-website"></i>Quản Lí Danh Mục</a></li>
                    <li class="active"><a href="index.php?controller=post&action=listed"> <i class="icon-website"></i>Quản Lí Bài Đăng</a></li>
                    <li><a href="index.php?controller=user&action=listed"> <i class="icon-grid"></i>Quản Lí Người Dùng</a></li>
                    <li><a href="index.php?controller=admin&action=listed"> <i class="icon-user"></i>Quản Trị Viên</a></li>
					<li><a href="../index.php?controller=home"> <i class="icon-interface-windows"></i>Trang Chủ </a></li>
                    <li><a href="/rongbayproject/admin//index.php?controller=user&action=login"> <i class="icon-interface-windows"></i>Đăng Nhập </a></li>
                </ul>
            </nav>

            <div class="content-inner">
                <header class="page-header">
                    <div class="container-fluid"> <h2 class="no-margin-bottom">Quản Lí Bài Đăng</h2> </div>
                </header>

                <div class="breadcrumb-holder container-fluid">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Quản Lí Bài Đăng</a></li>
                        <li class="breadcrumb-item active">Danh Sách Bài Đăng</li>
                    </ul>
                </div>

                <section class="tables">   
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header d-flex align-items-center">
                                        <h3 class="h4">Quản Lí Bài Đăng</h3> 
                                        <button class="btn-outline-primary"  style="margin-left: 870px;width: 100px;padding: 2.5rem -2rem;border-radius: 4.3rem;"><a href="index.php?controller=post&action=add">Thêm</a></button>  
                                    </div>
                                    <div class="msg" style="color: red; font-size: 12px; font-weight: initial; margin-left: 20px">
                                        <?php
                                        if (isset($_SESSION['add'])) {
                                            $mess = $_SESSION['add'];
                                            echo $mess;
                                        }unset($_SESSION['add']);

                                        if (isset($_SESSION['edit'])) {
                                            $mess = $_SESSION['edit'];
                                            echo $mess;
                                        }unset($_SESSION['edit']);
                                        ?>
                                    </div>

                                    <div class="card-body"> 
                                        <?php
                                        $result = $result3;
                                        ?>
                                    </div>
                                    <div>
                                        <table class='table table-striped table-hover'> 
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Tên Bài Đăng</th>
                                                    <th>Người Đăng</th>
                                                    <th>SĐT</th>
                                                    <th>Danh Mục</th>
                                                    <th>Thành Phố</th>
                                                    <th>Giá</th>
                                                    <th>Ảnh</th>
                                                    <th>Hành Động</th>
                                                </tr>
                                            </thead>
                                            <?php
                                            while ($rows = $result->fetch_array()) {
                                                ?>
                                                <tbody>
                                                    <tr id="id<?php echo $rows['id']; ?>">
                                                        <td><?php echo $rows['id']; ?></td>
                                                        <td><a href="index.php?controller=post&action=postdetail&id=<?php echo $rows['id']; ?>"><?php echo $rows['title']; ?></a></td>
                                                        <td><?php echo $rows['fullname']; ?></td>
                                                        <td><?php echo $rows['phone']; ?></td>
                                                        <td><?php echo $rows['category_name']; ?></td>
                                                        <td><?php echo $rows['pname']; ?></td>
                                                        <td><?php echo $rows['price']; ?> <?php echo $rows['unit']; ?></td>
                                                        <td><img src="/rongbayproject/<?php echo $rows['image'] ?>" width="70" height="70"></td>
                                                        <td><a  href="index.php?controller=post&action=edit&id=<?php echo $rows['id']; ?>">Sửa</a>
                                                        <span>|</span> <button type="button" class="delete_post" idP="<?php echo $rows['id']; ?>"><span style="color:#796AEE"> Xóa </span></button></td>
                                                    </tr>
                                                </tbody>
                                            <?php } ?>
                                        </table> 
                                        <div class="pagination">
                                            <?php
                                            if ($current_page > 1 && $total_page > 1) {
                                                echo '<a href="index.php?controller=post&action=listed&page=' . ($current_page - 1) . '">Prev</a> | ';
                                            }
                                            for ($i = 1; $i <= $total_page; $i++) {
                                                if ($i == $current_page) {
                                                    echo '<span>' . $i . '</span> | ';
                                                } else {
                                                    echo '<a href="index.php?controller=post&action=listed&page=' . $i . '">' . $i . '</a> | ';
                                                }
                                            }
                                            if ($current_page < $total_page && $total_page > 1) {
                                                echo '<a href="index.php?controller=post&action=listed&page=' . ($current_page + 1) . '">Next</a> | ';
                                            }
                                            ?>                       
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            

                <footer class="main-footer">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-sm-6"> <p><a href="">Quản Lí Bài Đăng</a></p> </div>
                                <div class="col-sm-6 text-right">
                                    <p><?php
                                        $now = new DateTime();
                                        echo $now->format('Y-m-d H:i');
                                        ?></p>
                                </div>
                            </div>
                        </div>
                </footer>
            </div>
        </div>
    </div>
    <?php
} else {
    header('location: index.php?controller=user&action=login');
}
?>