<?php if ($_SESSION['user'] && $_SESSION['pass']) {
    ?>
    <div class="page">
        <header class="header">
            <nav class="navbar">
                <div class="container-fluid">
                    <div class="navbar-holder d-flex align-items-center justify-content-between">
                        <div class="navbar-header">
                            <a href="#" class="navbar-brand"></a>
                            <div class="brand-text brand-big"><strong >Quản Lí Bài Đăng</strong></div>
                        </div>

                        <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                            <li class="nav-item d-flex align-items-center"><a id="search" href="#"></a></li>
                            <li class="nav-item dropdown"> </li>
                            <li class="nav-item dropdown">
                                <a id="messages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="fa fa-envelope-o"></i><span class="badge bg-orange">10</span></a>
                                <ul aria-labelledby="notifications" class="dropdown-menu">
                                    <li><a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                                            <div class="msg-profile"></div>

                                            <div class="msg-body">
                                                <h3 class="h5">Jason Doe</h3><span>Sent You Message</span>
                                            </div></a>
                                    </li>

                                    <li><a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                                            <div class="msg-profile"></div>
                                            <div class="msg-body">
                                                <h3 class="h5">Frank Williams</h3><span>Sent You Message</span>
                                            </div></a>
                                    </li>

                                    <li><a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                                            <div class="msg-profile"></div>
                                            <div class="msg-body">
                                                <h3 class="h5">Ashley Wood</h3><span>Sent You Message</span>
                                            </div></a>
                                    </li>

                                    <li><a rel="nofollow" href="#" class="dropdown-item all-notifications text-center"> <strong>Read all messages</strong></a></li>
                                </ul>
                            </li>
                            <!-- Logout    -->
                            <li class="nav-item"><a href="index.php?controller=user&action=logout" class="nav-link logout">Đăng Xuất<i class="fa fa-sign-out"></i></a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>

        <div class="page-content d-flex align-items-stretch">
            <nav class="side-navbar">
                <div class="sidebar-header d-flex align-items-center">
                    <div class="avatar"></div>
                    <div class="title"> <h1 class="h4"><?php echo ucwords( $_SESSION["user"])?></h1> <p>Administrator</p></div>
                </div>

                <span class="heading">Quản Lí Bài Đăng</span>
                <ul class="list-unstyled">
                    <li><a href="index.php?controller=category&action=listed"> <i class="icon-website"></i>Quản Lí Danh Mục</a></li>
                    <li class="active"><a href="index.php?controller=post&action=listed"> <i class="icon-website"></i>Quản Lí Bài Đăng</a></li>
                    <li><a href="index.php?controller=user&action=listed"> <i class="icon-grid"></i>Quản Lí Người Dùng</a></li>
                    <li><a href="index.php?controller=admin&action=listed"> <i class="icon-user"></i>Quản Trị Viên</a></li>
                    <li><a href="index.php?controller=user&action=login"> <i class="icon-interface-windows"></i>Đăng Nhập</a></li>
                </ul>
                <span class="heading">Extras</span>
            </nav>

            <div class="content-inner">
                <header class="page-header">
                    <div class="container-fluid">
                        <h2 class="no-margin-bottom">Quản Lí Bài Đăng</h2>
                    </div>
                </header>

                <div class="breadcrumb-holder container-fluid">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Quản Lí Bài Đăng</a></li>
                        <li class="breadcrumb-item active"><a href="index.php?controller=post&action=listed">Danh Sách Bài Đăng</a></li>              
                        <li class="breadcrumb-item active">Chỉnh Sửa Bài Đăng </li>
                    </ul>
                </div>
                <section class="tables">   
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="msg-error" style="color: red; margin-left: 80px;">
                                        <?php
                                        if (isset($_SESSION['error'])) {
                                            $error = $_SESSION['error'];
//                                        echo '<pre>';
//                                        print_r($error);
//                                        echo '</pre>';
                                            foreach ($error as $msg) {
                                                echo $msg . '<br>';
                                            }
                                            unset($_SESSION['error']);
                                        }
                                        ?>
                                    </div>
                                    <form action="" id="submit" method="POST" role="form" enctype="multipart/form-data" style="margin-left: 50px;" >
                                        <div class="form-group" style="margin-top: 20px;">
                                            <label class="control-label col-sm-5" for="name">Tên Bài Đăng</label>
                                            <div class="col-sm-10">                                         
                                                <input type="text" class="form-control" id="name" name="name" value="<?php echo $row['title']; ?>">
                                                <input type="hidden" name="post_ID" value="<?= $_GET['id'] ?>"/>
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-top: 20px;">
                                            <label class="control-label col-sm-5" for="fullname">Tên Người Đăng</label>
                                            <div class="col-sm-10">                                         
                                                <input type="text" class="form-control" id="fullname" name="fullname" value="<?php echo $row['fullname']; ?>">
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-top: 20px;">
                                            <label class="control-label col-sm-5" for="email">Email</label>
                                            <div class="col-sm-10">                                         
                                                <input type="email" disabled class="form-control" id="email" name="email" value="<?php echo $row['email']; ?>">
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-top: 20px;">
                                            <label class="control-label col-sm-5" for="phone">Số Điện thoại Liên Hệ</label>
                                            <div class="col-sm-10">                                         
                                                <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $row['phone']; ?>">
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label class="control-label col-sm-2" for="parent">Thuộc Danh Mục</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" name="parent">
                                                    <?php
													
                                                    while ($row_list = $list_parent->fetch_array()) {
                                                        ?>
                                                        <option value="<?= $row_list['id'] ?>" <?= ($row['category_id'] == $row_list['id']) ? ' selected="selected"' : ''; ?>><?= $row_list['category_name'] ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-top: 20px;">
                                            <label class="control-label col-sm-5" for="city">Thành Phố</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" name="city" id="city">
                                                    <?php
                                                    while ($row_list_province = $list_province->fetch_array()) {
                                                        ?>
                                                        <option value="<?= $row_list_province['id'] ?>" <?= ($row['city_id'] == $row_list_province['id']) ? ' selected="selected"' : ''; ?>><?= $row_list_province['name'] ?></option>;
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-top: 20px;">
                                            <label class="control-label col-sm-5" for="district">Quận/Huyện</label>
                                            <div class="col-sm-10" id="district_select">
                                                <select class="form-control" name="district">
                                                    <?php
                                                    while ($row_list_district = $list_district->fetch_array()) {
//                                                            
                                                        ?>
                                                        <option value="<?= $row_list_district['id'] ?>" <?= ($row['district_id'] == $row_list_district['id']) ? ' selected="selected"' : ''; ?>><?= $row_list_district['name'] ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-top: 20px;">
                                            <label class="control-label col-sm-5" for="address">Địa chỉ</label>
                                            <div class="col-sm-10">                                         
                                                <input type="text" class="form-control" name="address" value="<?php echo $row['address']; ?>">
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-top: 20px;">
                                            <label class="control-label col-sm-5" for="price">Giá thành</label>
                                            <div class="col-sm-10">                                         
                                                <input type="text" class="form-control" name="price" value="<?php echo $row['price']; ?>">
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-top: 20px;">
                                            <label class="control-label col-sm-5" for="unit">Đơn vị</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" name="unit">
                                                    <option value="Tỷ"<?php echo ($row['unit'] == 'Tỷ') ? ' selected="selected"' : ''; ?>>Tỷ</option>
                                                    <option value="Triệu"<?php echo ($row['unit'] == 'Triệu') ? ' selected="selected"' : ''; ?>>Triệu</option>
                                                    <option value="Triệu/m2"<?php echo ($row['unit'] == 'Triệu/m2') ? ' selected="selected"' : ''; ?>>Triệu/m2</option>
                                                    <option value="Triệu/tháng"<?php echo ($row['unit'] == 'Triệu/tháng') ? ' selected="selected"' : ''; ?>>Triệu/tháng</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-top: 20px;">
                                            <label class="control-label col-sm-5" for="description">Mô Tả</label>
                                            <div class="col-sm-10">
                                                <textarea type="text" class="form-control" id="description" name="description"><?php echo $row['description']; ?></textarea>
                                            </div>
                                        </div>
									<?php
										
										if($par1['parent'] == 1 || $par1['parent'] == 2 || $par1['parent'] == 5){
									?>
                                        <div class="form-group choose-one" style="margin-top: 20px;">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label class="control-label col-sm-5" for="lay">Hướng Cửa Chính</label>
                                                    <div class="col-sm-10">
                                                        <label class="laya">Đông
                                                            <input type="radio" name="lay" value="Đông"<?php echo ($row['lay'] == 'Đông') ? ' checked="checked"' : ''; ?>>
                                                            <span class="lay"></span>
                                                        </label>
                                                        <label class="laya">Tây Bắc
                                                            <input type="radio" name="lay" value="Tây Bắc"<?php echo ($row['lay'] == 'Tây Bắc') ? ' checked="checked"' : ''; ?>>
                                                            <span class="lay"></span>
                                                        </label>
                                                        <label class="laya">Tây Nam
                                                            <input type="radio" name="lay" value="Tây Nam"<?php echo ($row['lay'] == 'Tây Nam') ? ' checked="checked"' : ''; ?>>
                                                            <span class="lay"></span>
                                                        </label>
                                                        <label class="laya">Tây
                                                            <input type="radio" name="lay" value="Tây"<?php echo ($row['lay'] == 'Tây') ? ' checked="checked"' : ''; ?>>
                                                            <span class="lay"></span>
                                                        </label>
                                                        <label class="laya">Nam
                                                            <input type="radio" name="lay" value="Nam"<?php echo ($row['lay'] == 'Nam') ? ' checked="checked"' : ''; ?>>
                                                            <span class="lay"></span>
                                                        </label>
                                                        <label class="laya">Đông Bắc
                                                            <input type="radio" name="lay" value="Đông Bắc"<?php echo ($row['lay'] == 'Đông Bắc') ? ' checked="checked"' : ''; ?>>
                                                            <span class="lay"></span>
                                                        </label>
                                                        <label class="laya">Bắc
                                                            <input type="radio" name="lay" value="Bắc"<?php echo ($row['lay'] == 'Bắc') ? ' checked="checked"' : ''; ?>>
                                                            <span class="lay"></span>
                                                        </label>
                                                        <label class="laya">Đông Nam
                                                            <input type="radio" name="lay" value="Đông Nam"<?php echo ($row['lay'] == 'Đông Nam') ? ' checked="checked"' : ''; ?>>
                                                            <span class="lay"></span>
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group" style="margin-top: 20px;">
                                                        <label class="control-label col-sm-5" for="balcony">Lợi ích</label>
                                                        <div class="col-sm-10">
                                                            <label class="laya">Gần trường
                                                                <input type="radio" checked="checked" name="balcony" value="Gần trường"<?php echo ($row['balcony'] == 'Gần trường') ? ' checked="checked"' : ''; ?>>
                                                                <span class="lay"></span>
                                                            </label>
                                                            <label class="laya">Gần bệnh viện
                                                                <input type="radio" name="balcony" value="Gần bệnh viện"<?php echo ($row['balcony'] == 'Gần bệnh viện') ? ' checked="checked"' : ''; ?>>
                                                                <span class="lay"></span>
                                                            </label>
                                                            <label class="laya">Gần chợ/siêu thị
                                                                <input type="radio" name="balcony" value="Gần chợ/siêu thị"<?php echo ($row['balcony'] == 'Gần chợ/siêu thị') ? ' checked="checked"' : ''; ?>>
                                                                <span class="lay"></span>
                                                            </label>
                                                            <label class="laya">Gần bến tàu/xe
                                                                <input type="radio" name="balcony" value="Gần bến tàu/xe"<?php echo ($row['balcony'] == 'Gần bến tàu/xe') ? ' checked="checked"' : ''; ?>>
                                                                <span class="lay"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										
										<div class="form-group acreage" style="margin-top: 20px;">
                                            <label class="control-label col-sm-5" for="acreage">Diện Tích</label>
                                            <div class="col-sm-10">                                         
                                                <input type="text" class="form-control" id="acreage" name="acreage" value="<?php echo $row['acreage']; ?>">
                                            </div>
                                        </div>

                                        <div class="form-group choose-one" style="margin-top: 20px;">
                                            <label class="control-label col-sm-5" for="license">Giấy Tờ Pháp Lý</label>
                                            <div class="col-sm-10">
                                                <label class="laya">Có sổ hồng
                                                    <input type="radio" name="license" value="Có sổ hồng"<?php echo ($row['license'] == 'Có sổ hồng') ? ' checked="checked"' : ''; ?>>
                                                    <span class="lay"></span>
                                                </label>
                                                <label class="laya">Hợp đồng mua bán
                                                    <input type="radio" name="license" value="Hợp đồng mua bán"<?php echo ($row['license'] == 'Hợp đồng mua bán') ? ' checked="checked"' : ''; ?>>
                                                    <span class="lay"></span>
                                                </label>
                                                <label class="laya">Văn phòng chuyển nhượng
                                                    <input type="radio" name="license" value="Văn phòng chuyển nhượng"<?php echo ($row['license'] == 'Văn phòng chuyển nhượng') ? ' checked="checked"' : ''; ?>>
                                                    <span class="lay"></span>
                                                </label>
                                                <label class="laya">Khác
                                                    <input type="radio" name="license" value="Khác"<?php echo ($row['license'] == 'Khác') ? ' checked="checked"' : ''; ?>>
                                                    <span class="lay"></span>
                                                </label>
                                            </div>
                                        </div>
									<?php
										}
										?>
                                        <div class="form-group">
                                            <div class="col-sm-10">
                                                <div class="picture">
                                                    <?php
                                                    while ($a = $countImg->fetch_array()) {
                                                        ?>
                                                        <div class="product-img">
                                                            <div class="del-icon">
                                                                <a title="Xóa ảnh này" class="rm-img"><img src="./library/images/delete.png" width="20" height="20" class="img-responsive"></a>
                                                            </div>
                                                        </div>
                                                        <img src="/<?php echo $a['image'] ?>" width="170px" height="170px">
                                                        <?php }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="">Chọn ảnh</label>
                                            <div class="col-sm-10">
                                                <input type="file" class="form-control" id="file" name="file[]" multiple="">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-10">
                                                <div class="row">
                                                    <div class="col-sm-offset-2 col-sm-2">
                                                        <button type="submit" class="btn btn-primary" name="update">Update</button>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <a href="index.php?controller=post&action=listed"><button type="button" class="btn btn-primary">Quay Lại </button></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- Page Footer-->
                <footer class="main-footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6">  <p><a href="">Quản Lí Bài Đăng</a></p></div>
                            <div class="col-sm-6 text-right">
                                <p><?php
                                    $now = new DateTime();
                                    echo $now->format('Y-m-d H:i');
                                    ?></p>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </div>
    <?php
} else {
    header('location: index.php?controller=user&action=login'); //bỏ lại link chuẩn ở đây
}
?>