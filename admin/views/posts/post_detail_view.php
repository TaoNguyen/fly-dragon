<?php if ($_SESSION['user'] && $_SESSION['pass']) {
    ?>
<div class="page">
    <header class="header">
        <nav class="navbar">
            <div class="container-fluid">
                <div class="navbar-holder d-flex align-items-center justify-content-between">
                    <div class="navbar-header">
                        <a href="#" class="navbar-brand"></a>
                        <div class="brand-text brand-big"><strong >Quản Lí Bài Đăng</strong></div>
                    </div>

                    <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                        <li class="nav-item d-flex align-items-center"><a id="search" href="#"></a></li>
                        <li class="nav-item dropdown">  </li>

                        <li class="nav-item dropdown"> <a id="messages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="fa fa-envelope-o"></i><span class="badge bg-orange">10</span></a>
                            <ul aria-labelledby="notifications" class="dropdown-menu">
                                <li><a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                                        <div class="msg-profile"></div>
                                        <div class="msg-body"><h3 class="h5">Jason Doe</h3><span>Sent You Message</span></div></a>
                                </li>

                                <li><a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                                        <div class="msg-profile"></div>
                                        <div class="msg-body">
                                            <h3 class="h5">Frank Williams</h3><span>Sent You Message</span>
                                        </div>
                                    </a></li>

                                <li><a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                                        <div class="msg-profile"></div>
                                        <div class="msg-body">
                                            <h3 class="h5">Ashley Wood</h3><span>Sent You Message</span></div></a>
                                </li>

                                <li><a rel="nofollow" href="#" class="dropdown-item all-notifications text-center"> <strong>Read all messages    </strong></a></li>
                            </ul>
                        </li>

                        <li class="nav-item"><a href="index.php?controller=user&action=logout" class="nav-link logout">Đăng Xuất<i class="fa fa-sign-out"></i></a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <div class="page-content d-flex align-items-stretch"> 
        <nav class="side-navbar">
            <div class="sidebar-header d-flex align-items-center">
                <div class="avatar"></div>
                <div class="title">
                    <h1 class="h4"><?php echo ucwords( $_SESSION["user"])?></h1>
                    <p>Administrator</p>
                </div>
            </div>

            <span class="heading">Quản Lí Bài Đăng</span>
            <ul class="list-unstyled">
                <li><a href="index.php?controller=category&action=listed"> <i class="icon-website"></i>Quản Lí Danh Mục</a></li>
                <li class="active"><a href="index.php?controller=post&action=listed"> <i class="icon-website"></i>Quản Lí Bài Đăng</a></li>
                <li><a href="index.php?controller=user&action=listed"> <i class="icon-grid"></i>Quản Lí Người Dùng</a></li>
                <li><a href="index.php?controller=admin&action=listed"> <i class="icon-user"></i>Quản Trị Viên</a></li>
                <li><a href="index.php?controller=user&action=login"> <i class="icon-interface-windows"></i>Đăng Nhập </a></li>
            </ul>
        </nav>

        <div class="content-inner">
            <header class="page-header">
                <div class="container-fluid"> <h2 class="no-margin-bottom">Quản Lí Bài Đăng</h2> </div>
            </header>

            <div class="breadcrumb-holder container-fluid">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Quản Lí Bài Đăng</a></li>
                    <li class="breadcrumb-item active">Danh Sách Bài Đăng</li>
                    <li class="breadcrumb-item active">Chi tiết Bài Đăng </li>
                </ul>
            </div>

            <section class="tables">   
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header d-flex align-items-center">
                                    <h3 class="h4">Quản Lí Bài Đăng</h3> 
                                </div>

                                <div class="card-body">
                                    <div style="padding-top:5px;">
                                        <h5>Thông tin người đăng:</h5>
                                    </div>
                                    <table class='table table-striped table-hover'> 
                                        <thead>
                                            <tr>
                                                <th>Người Đăng</th>
                                                <th>SĐT</th>
                                                <th>Email</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <tr id="id<?php echo $rows['id']; ?>">
                                                <td><?php echo $rows['fullname']; ?></td>
                                                <td><?php echo $rows['phone']; ?></td>
                                                <td><?php echo $rows['email']; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <div style="padding-top:10px;">
                                        <h5>Thông tin Bài Đăng:</h5>
                                    </div>
                                    
                                    <div class="detail">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <p>ID bài đăng:</p>
                                                <p>Tên Bài Đăng:</p>
                                                <p>Diện tích: </p>
                                                <p>Thuộc Danh Mục:</p>
                                                <p>Thành Phố: </p>
                                                <p>Quận/Huyện: </p>
                                                <p>Địa chỉ: </p>
                                                <p>Giá: </p>
                                                <p>Ngày đăng tin:</p>
                                                <p>Mô tả:</p>
                                                <p>Hướng chính:</p>
                                                <p>Lợi ích:</p>
                                                <p>Giấy tờ pháp lý:</p>
                                            </div>
                                            
                                            <div class="col-sm-9" style="font-weight:bold;">
                                                <p><?php echo $rows['id']; ?></p>
                                                <p><?php echo $rows['title']; ?></p>
                                                <p><?php echo $rows['acreage']; ?> m2</p>
                                                <p><?php echo $rows['category_name']; ?></p>
                                                <p><?php echo $rows['pname']; ?></p>
                                                <p><?php echo $rows['disname']; ?></p>
                                                <p><?php echo $rows['address']; ?></p>
                                                <p><?php echo $rows['price']; ?> <?php echo $rows['unit']; ?></p>
                                                <p><?php echo $rows['date']; ?></p>
                                                <p><?php echo $rows['description']; ?></p>
                                                <p><?php echo $rows['lay']; ?></p>
                                                <p><?php echo $rows['balcony']; ?></p>
                                                <p><?php echo $rows['license']; ?></p>
                                            </div>
                                        </div>
                                        
                                        <p>Ảnh: <br>
                                            <div class="picture">
                                                <?php
                                                while ($a = $countImg->fetch_array()) {
                                                    ?>
                                                    <img src="/<?php echo $a['image'] ?>" width="200" height="200">
                                                    <?php }
                                                ?>
                                            </div>
                                            </p>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-10">
                                            <div class="col-sm-8">
                                                <a href="index.php?controller=post&action=listed"><button type="button" class="btn btn-primary">Quay Lại </button></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Page Footer-->
            <footer class="main-footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6"> <p><a href="">Quản Lí Bài Đăng</a></p> </div>
                        <div class="col-sm-6 text-right">
                            <p><?php
                                $now = new DateTime();
                                echo $now->format('Y-m-d H:i');
                                ?></p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>
<?php
        } else {
            header('location: index.php?controller=user&action=login');
        }
    ?>