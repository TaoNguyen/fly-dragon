<?php if ($_SESSION['user'] && $_SESSION['pass']) {
            $row = $result->fetch_assoc();
    ?>
    <div class="page">
        <header class="header">
            <nav class="navbar">
                <div class="container-fluid">
                    <div class="navbar-holder d-flex align-items-center justify-content-between">
                        <div class="navbar-header">
                            <a href="#" class="navbar-brand"></a>
                            <div class="brand-text brand-big"><strong >Quản Lí Danh Mục</strong></div>
                        </div>
                        
                        <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                            <li class="nav-item d-flex align-items-center"><a id="search" href="#"></a></li>
                            <li class="nav-item dropdown"> </li>
                            <li class="nav-item dropdown">
                                <a id="messages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="fa fa-envelope-o"></i><span class="badge bg-orange">10</span></a>
                                <ul aria-labelledby="notifications" class="dropdown-menu">
                                    <li><a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                                        <div class="msg-profile"></div>

                                        <div class="msg-body">
                                            <h3 class="h5">Jason Doe</h3><span>Sent You Message</span>
                                        </div></a>
                                    </li>

                                    <li><a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                                        <div class="msg-profile"></div>
                                        <div class="msg-body">
                                            <h3 class="h5">Frank Williams</h3><span>Sent You Message</span>
                                        </div></a>
                                    </li>

                                    <li><a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                                        <div class="msg-profile"></div>
                                        <div class="msg-body">
                                            <h3 class="h5">Ashley Wood</h3><span>Sent You Message</span>
                                        </div></a>
                                    </li>

                                    <li><a rel="nofollow" href="#" class="dropdown-item all-notifications text-center"> <strong>Read all messages</strong></a></li>
                                </ul>
                            </li>
                            <!-- Logout    -->
                            <li class="nav-item"><a href="index.php?controller=user&action=logout" class="nav-link logout">Đăng Xuất<i class="fa fa-sign-out"></i></a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        
        <div class="page-content d-flex align-items-stretch">
            <nav class="side-navbar">
                <div class="sidebar-header d-flex align-items-center">
                    <div class="avatar"></div>
                    <div class="title"> <h1 class="h4"><?php echo ucwords( $_SESSION["user"])?></h1> <p>Administrator</p></div>
                </div>
                
                <span class="heading">Quản Lí Danh Mục</span>
                <ul class="list-unstyled">
                    <li class="active"><a href="index.php?controller=category&action=listed"> <i class="icon-website"></i>Quản Lí Danh Mục</a></li>
					<li><a href="index.php?controller=post&action=listed"> <i class="icon-website"></i>Quản Lí Bài Đăng</a></li>
                    <li><a href="index.php?controller=user&action=listed"> <i class="icon-grid"></i>Quản Lí Người Dùng</a></li>
                    <li><a href="index.php?controller=admin&action=listed"> <i class="icon-user"></i>Quản Trị Viên</a></li>
                    <li><a href="index.php?controller=user&action=login"> <i class="icon-interface-windows"></i>Đăng Nhập</a></li>
                </ul>
                <span class="heading">Extras</span>
            </nav>
            
            <div class="content-inner">
                <!-- Page Header-->
                <header class="page-header">
                    <div class="container-fluid">
                        <h2 class="no-margin-bottom">Quản Lí Danh Mục</h2>
                    </div>
                </header>
                
                <!-- Breadcrumb-->
                <div class="breadcrumb-holder container-fluid">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Quản Lí Danh Mục</a></li>
                        <li class="breadcrumb-item active"><a href="index.php?controller=category&action=listed">Danh Sách Danh Mục</a></li>              
                        <li class="breadcrumb-item active">Chỉnh Sửa Danh Mục </li>
                    </ul>
                </div>
                <section class="tables">   
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="msg-error" style="color: red; margin-left: 80px;">
                                    
                                    <?php
                                    if(isset($_SESSION['error'])){
                                        $error=$_SESSION['error'];
//                                        echo '<pre>';
//                                        print_r($error);
//                                        echo '</pre>';
                                        foreach ($error as $msg){
                                            echo $msg.'<br>';
                                        }
                                        unset($_SESSION['error']);
                                    }
                                    ?>
                                    </div>
                                    <form action="" method="POST" role="form" enctype="multipart/form-data" style="margin-left: 50px;" >
                                        <div class="form-group" style="margin-top: 20px;">
                                            <label class="control-label col-sm-5" for="name">Tên Danh Mục</label>
                                            <div class="col-sm-10">                                         
                                                <input type="text" class="form-control" id="nameproduct" name="name" value="<?php echo $row['category_name']; ?>">
                                                <input type="hidden" name="cate_ID" value="<?=$_GET['id']?>"/>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="parent">Thư Mục Cha</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" name="parent">
                                                    <option value="0" <?= ($row['parent'] ==  '0')?' selected="selected"':'';?>>None</option>
                                                    <?php
                                                    
                                                    if($list_parent->num_rows!=0){
                                                        while ($row_list = $list_parent->fetch_array()) {
                                                            ?>
                                                            <option value="<?= $row_list['id'] ?>" <?= ($row['parent']==$row_list['id']) ? ' selected="selected"' : '';?>><?= $row_list['category_name'] ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="col-sm-10"> 
                                                <img src="/<?php echo $row['img']?>" width="200" height="200">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="">Ảnh</label>
                                            <div class="col-sm-10">
                                                <input type="file" class="form-control" id="file" name="file">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="col-sm-10">
                                                <div class="row">
                                                    <div class="col-sm-offset-2 col-sm-2">
                                                        <button type="submit" class="btn btn-default" name="update">Update</button>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <a href="index.php?controller=category&action=listed"><button type="button" class="btn btn-default">Quay Lại </button></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                
                <!-- Page Footer-->
                <footer class="main-footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6">  <p><a href="">Quản Lí Danh Mục</a></p></div>
                            <div class="col-sm-6 text-right">
                               <p><?php 
                                    $now = new DateTime();
                                    echo $now->format('Y-m-d H:i');
                                ?></p>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </div>
<?php
        } else {
            header('location: index.php?controller=user&action=login');
        }
    ?>