
        <div class="page login-page">
            <div class="container d-flex align-items-center">
                <div class="form-holder has-shadow">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="info d-flex align-items-center">
                                <div class="content" style="text-align: center">
                                    <div class="logo">
                                        <h1>Admin Management</h1>
                                    </div>
                                    <p>Welcome to our service!!!</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 bg-white">
                            <div class="form d-flex align-items-center">
                                <div class="content">
                                    <div style="color:red; font-style: italic; font-size: 12px;">
                                        <?php 
                                            if(isset($errormsg)){
                                                echo $errormsg; 
                                            }
                                        ?>
                                    </div>

                                    <form id="login-form" method="post">
                                        <div class="form-group">
                                            <input id="login-username" type="email" name="username" required class="input-material">
                                            <label for="login-username" class="label-material">Admin email</label>
                                        </div>

                                        <div class="form-group">
                                            <input id="login-password" type="password" name="password" required="" class="input-material">
                                            <label for="login-password" class="label-material">Password</label>
                                        </div>

                                        <input name="submit" type="submit" value=" Login ">
                                    </form>
                                    <br><small>Do not want to go to </small><a href="#">Rongbay.com?</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="copyrights text-center">
                <p>Design by<a href="#" class="external"> Thu Thuong Nguyen!</a></p>
            </div>
        </div>