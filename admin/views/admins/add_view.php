
<?php if ($_SESSION['user'] && $_SESSION['pass']) {
            
    ?>
    <div class="page">
        <header class="header">
            <nav class="navbar">
         
                <div class="container-fluid">
                    <div class="navbar-holder d-flex align-items-center justify-content-between">
                        <div class="navbar-header">
                            <a href="#" class="navbar-brand"></a>
                            <div class="brand-text brand-big"><strong >User Management</strong></div>
                        </div>

                        <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                            <li class="nav-item d-flex align-items-center"><a id="search" href="#"></a></li>
                            <li class="nav-item dropdown"> </li>
                            <li class="nav-item dropdown">
                                <a id="messages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="fa fa-envelope-o"></i><span class="badge bg-orange">10</span></a>
                                <ul aria-labelledby="notifications" class="dropdown-menu">
                                    <li><a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                                        <div class="msg-profile"></div>

                                        <div class="msg-body">
                                            <h3 class="h5">Jason Doe</h3><span>Sent You Message</span>
                                        </div></a>
                                    </li>

                                    <li><a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                                        <div class="msg-profile"></div>
                                        <div class="msg-body">
                                            <h3 class="h5">Frank Williams</h3><span>Sent You Message</span>
                                        </div></a>
                                    </li>

                                    <li><a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                                        <div class="msg-profile"></div>
                                        <div class="msg-body">
                                            <h3 class="h5">Ashley Wood</h3><span>Sent You Message</span>
                                        </div></a>
                                    </li>

                                    <li><a rel="nofollow" href="#" class="dropdown-item all-notifications text-center"> <strong>Read all messages</strong></a></li>
                                </ul>
                            </li>
                            <!-- Logout    -->
                            <li class="nav-item"><a href="index.php?controller=user&action=logout" class="nav-link logout">Đăng Xuất<i class="fa fa-sign-out"></i></a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        
        <div class="page-content d-flex align-items-stretch">
            <nav class="side-navbar">
                <div class="sidebar-header d-flex align-items-center">
                    <div class="avatar"></div>
                    <div class="title"> <h1 class="h4"><?php echo ucwords( $_SESSION["user"])?></h1> <p>Administratior</p></div>
                </div>
                
                <span class="heading">Admin Management</span>
                <ul class="list-unstyled">
                    <li><a href="index.php?controller=category&action=listed"> <i class="icon-website"></i>Quản lí danh mục</a></li>
                    <li><a href="index.php?controller=post&action=listed"> <i class="icon-website"></i>Quản Lí Bài Đăng</a></li>
                    <li><a href="index.php?controller=user&action=listed"> <i class="icon-grid"></i>Quản lí người dùng</a></li>
                    <li class="active"><a href="index.php?controller=admin&action=listed"> <i class="icon-user"></i>Quản trị viên</a></li>
                    <li><a href="index.php?controller=user&action=login"> <i class="icon-interface-windows"></i>Đăng nhập </a></li>
                </ul>
                <span class="heading">Extras</span>
            </nav>
            
            <div class="content-inner">
                <!-- Page Header-->
                <header class="page-header">
                    <div class="container-fluid">
                        <h2 class="no-margin-bottom">Quản trị viên</h2>
                    </div>
                </header>
                
                <!-- Breadcrumb-->
                <div class="breadcrumb-holder container-fluid">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Quản trị viên</a></li>
                        <li class="breadcrumb-item active"><a href="index.php?controller=user&action=listed">Quản trị viên</a></li>              
                        <li class="breadcrumb-item active">Thêm Quản trị viên </li>
                    </ul>
                </div>
                <section class="tables">   
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="msg-error" style="color: red; margin-left: 80px;">
                                    
                                    <?php
                                    if(isset($_SESSION['error'])){
                                        $error=$_SESSION['error'];
//                                        echo '<pre>';
//                                        print_r($error);
//                                        echo '</pre>';
                                        foreach ($error as $msg){
                                            echo $msg.'<br>';
                                        }
                                        unset($_SESSION['error']);
                                    }
                                    ?>
                                    </div>
                                    <form method="POST" role="form" enctype="multipart/form-data" style="margin-left: 50px;" >
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="">Hình Ảnh</label>
                                            <div class="col-sm-10">
                                                <input type="file" class="form-control" id="file" name="file">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group" style="margin-top: 20px;">
                                            <label class="control-label col-sm-5" for="name">Tên quản trị viên</label>
                                            <div class="col-sm-10">                                         
                                                <input type="text" class="form-control" id="name" placeholder="Enter name" name="name">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="">Email</label>
                                            <div class="col-sm-10">
                                                <input required type="email" class="form-control" id="address" placeholder="Enter email" name="email">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="">Mật khẩu</label>
                                            <div class="col-sm-10">
                                                <input type="password" class="form-control" id="address" placeholder="Enter password" name="password">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="">Kiểm tra mật khẩu</label>
                                            <div class="col-sm-10">
                                                <input type="password" class="form-control" id="address" placeholder="Enter re-password" name="repass">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="">Số điện thoại</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="phone" placeholder="Enter number phone" name="phone">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="">Giới tính</label>
                                            <div class="col-sm-10">
                                                <select name="gender">
                                                    <option value="Nam">Nam</option>
                                                    <option value="Nữ">Nữ</option>
                                                    <option value="Khác">Khác</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="">Địa chỉ</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="address" placeholder="Nhập địa chỉ" name="address">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group"> 
                                            <div class="row">
                                                <div class="col-sm-offset-2 col-sm-2">
                                                    <button type="submit" class="btn btn-default" name="add">Thêm Admin</button>
                                                </div>
                                                <div class="col-sm-10">
                                                    <a href="index.php?controller=admin&action=listed"><button type="button" class="btn btn-default">Quay Lại </button></a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                
                <!-- Page Footer-->
                <footer class="main-footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6">  <p><a href="">Quản Trị Viên</a></p></div>
                            <div class="col-sm-6 text-right">
                               <p><?php 
                                    $now = new DateTime();
                                    echo $now->format('Y-m-d H:i');
                                ?></p>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </div>
        <?php
    } else {
        header('location: index.php?controller=user&action=login');
    }
    ?>