<?php

class Category {

    public $conn;

    public function __construct() {
        $conn = $GLOBALS['conn'];
    }

    //show ra all category
    public function getListCategory() {
        $sql = "SELECT * FROM categories";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
	public function getParent($id) {
        $sql = "SELECT parent FROM categories WHERE id = $id LIMIT 1";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
	
    //check email existed or not
    public function checkNameP($name) {
        $sql = "SELECT * FROM categories WHERE category_name='$name' LIMIT 1";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    public function checkNameCategory($name, $cate_ID) {
        $sql = "SELECT * FROM categories WHERE category_name='$name' AND id <> $cate_ID LIMIT 1";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    // thêm mới một user
    public function addCategory($name, $parent, $image) {
        if ($image != '') {
        $sql = "INSERT INTO categories (category_name, parent, img)
                    VALUES ('$name', '$parent', '$image')";
        $result = $GLOBALS['conn']->query($sql);
        }  else {
            $sql = "INSERT INTO categories (category_name, parent)
                    VALUES ('$name', '$parent')";
        $result = $GLOBALS['conn']->query($sql);
        }
        return $result;
    }

    public function getCategorybyId($cate_ID) {
        $sql = "SELECT * FROM categories WHERE id = $cate_ID";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }

    //update lại một category đã có trongDB
    public function updateCategory($cate_ID,$name, $parent, $image) {
        if ($image != '') {
            $sql = "UPDATE categories SET category_name = '$name', parent = $parent, img ='$image' WHERE id=$cate_ID";
            $result = $GLOBALS['conn']->query($sql);
        } else {
            $sql = "UPDATE categories SET category_name = '$name', parent = $parent WHERE id=$cate_ID";
            $result = $GLOBALS['conn']->query($sql);
        }
        return $result;
    }

    //delete a user in DB
    public function deleteCategory($id) {
        $sql = "DELETE FROM categories WHERE id = $id";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }

    //count all category in DB
    public function countCategory() {
        $sql = " SELECT COUNT(*) AS total FROM categories";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }

//    //get user limit
    public function getCategoryWithLimit($start, $limit) {
        $sql = "SELECT * FROM categories ORDER BY id DESC LIMIT $start, $limit";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }

    public function getlistparent() {
        $sql = 'SELECT id,category_name FROM categories WHERE parent = 0';
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    public function deleteImg($id) {
        $sql = "DELETE FROM images WHERE category_id = $id";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }

}

?>
