<?php

class Post {
    
    public $conn;

    public function __construct() {
        $conn = $GLOBALS['conn'];
    }
    
    //---------------------------------------- show ra all post --------------------------------------------------------------------
    public function getListPost() {
        $sql = "SELECT * FROM posts AS p LEFT JOIN categories AS c ON p.category_id = c.id LEFT JOIN post_contacts as p_contact ON p.id = p_contact.post_id LEFT JOIN images AS i ON p.id = i.post_id LEFT JOIN provinces as pro ON p.city_id = pro.id GROUP BY i.post_id";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    public function getlistparent() {
        $sql = 'SELECT id,category_name FROM categories WHERE parent <> 0';
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    public function getImg($post_id) {
        $sql = "SELECT image FROM images WHERE post_id = $post_id";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    // -----------------Phân trang -----------------
    
    public function countPost() {
        $sql = "SELECT COUNT(*) AS total FROM posts";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    public function getPostWithLimit($start, $limit) {
        $sql = "SELECT p.id,p.title,p.unit,p.description,p.acreage,p.address,p.price,p.lay,p.balcony,p.license,p.created_at AS date,c.category_name,p_contact.email,p_contact.fullname,p_contact.phone,pro.name AS pname,i.image FROM posts AS p LEFT JOIN categories AS c ON p.category_id = c.id LEFT JOIN post_contacts as p_contact ON p.id = p_contact.post_id LEFT JOIN images i ON p.id = i.post_id LEFT JOIN provinces as pro ON p.city_id = pro.id GROUP BY i.post_id ORDER BY p.id DESC LIMIT $limit OFFSET $start ";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    //----------------------------------------------- Thêm mới 1 bài đăng -----------------------------------------------------------
    
    public function addPost($name,$acreage,$city,$district,$price,$description,$lay,$balcony,$parent,$license,$unit,$address) {
        $sql = "INSERT INTO posts (title, acreage, city_id, district_id, price, description, lay, balcony, category_id, license,created_at,unit,address)
            VALUES ('$name','$acreage','$city','$district','$price','$description','$lay','$balcony','$parent','$license', NOW(),'$unit','$address')";
        $result = $GLOBALS['conn']->query($sql);
        
        if ($result) {
            $post_id = $GLOBALS['conn']->insert_id;
            return $post_id;
        } else {
            return false;
        }
    }
    
    public function addPostContact($phone, $email, $fullname, $post_id) {
        $sql = "INSERT INTO post_contacts (phone, email, fullname, post_id)
            VALUES ('$phone', '$email', '$fullname','$post_id')";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    public function addImage($image, $post_id) {
        $sql = "INSERT INTO images (image, post_id)
            VALUES ('$image', $post_id)";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    public function addImg($post_ID, $image) {
        $sql = "INSERT INTO images (image, post_id)
            VALUES ('$image', $post_ID)";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    //---------------------------------Xóa một bài đăng --------------------------------------
    public function deletePost($id) {
        $sql = "DELETE FROM posts WHERE id = $id";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    public function deletePostCont($id) {
        $sql = "DELETE FROM post_contacts WHERE id = $id";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    public function getListProvince() {
        $sql = 'SELECT id,name FROM provinces ORDER BY name ASC';
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    public function getDistrict($id) {
        $sql = "SELECT id, name FROM districts WHERE province_id = $id ORDER BY name ASC";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    public function getListDistrict($province_id) {
        $sql = "SELECT id, name FROM districts WHERE id = $province_id ORDER BY name ASC";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
//----------------------------------------- Edit một bài đăng ----------------------------------------------------------

    public function getPostById($post_ID) {
        $sql = "SELECT * FROM posts AS p LEFT JOIN categories AS c ON p.category_id = c.id LEFT JOIN post_contacts as p_contact ON p.id = p_contact.post_id LEFT JOIN images i ON p.id = i.post_id LEFT JOIN provinces as pro ON p.city_id = pro.id WHERE p.id = $post_ID";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    public function updatePost($post_ID,$address,$name,$acreage,$city,$district,$price,$description,$lay,$balcony,$parent,$license,$unit) {
        $sql = "UPDATE posts SET title = '$name',acreage ='$acreage',city_id='$city',district_id='$district',address='$address',price='$price',unit='$unit',description='$description',lay='$lay',balcony='$balcony',license='$license',category_id='$parent' WHERE id = $post_ID";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    public function updatePostContact($post_ID,$phone,$fullname) {
        $sql = "UPDATE post_contacts SET fullname = '$fullname',phone = '$phone' WHERE post_id = $post_ID";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    public function deleteImg($post_ID) {
         $sql = "DELETE FROM images WHERE post_id = $post_ID";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    
    public function getAPost($post_ID) {
        $sql = "SELECT p.id,p.title,p.unit,p.description,p.acreage,p.address,p.price,p.lay,p.balcony,p.license,p.created_at AS date,c.category_name,p_contact.email,p_contact.fullname,p_contact.phone,pro.name AS pname,dis.name AS disname FROM posts AS p LEFT JOIN categories AS c ON p.category_id = c.id LEFT JOIN post_contacts as p_contact ON p.id = p_contact.post_id LEFT JOIN images i ON p.id = i.post_id LEFT JOIN provinces as pro ON p.city_id = pro.id LEFT JOIN districts as dis ON p.district_id = dis.id WHERE p.id = $post_ID";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
	
	

}
?>