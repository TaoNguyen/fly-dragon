$(function () {
    $(".delete_user").click(function () {
        var element = $(this);
        var id = element.attr("idU");
//        alert(id); die;
        var p = confirm("Bạn có chắc chắn muốn xóa người này khỏi hệ thống?");
        if (p == true) {
            $.ajax({
                type: "GET",
                url: "direct.php?controller=user&action=delete&id=" + id,
                success: function (html) {
                    jQuery("#id" + id).remove();
                    alert('Bạn đã xóa người dùng thành công!');
                }
            });
        }
    });
});


$(function () {
    $(".delete_admin").click(function () {
        var element = $(this);
        var id = element.attr("idA");
        var p = confirm("Chỉ có duy nhất 1 quản trị viên. Bạn có thực sự muốn xóa?");
        if (p == true) {
            $.ajax({
                type: "GET",
                url: "direct.php?controller=admin&action=delete&id=" + id,
                success: function (html) {
                    jQuery("#id" + id).remove();
                    alert('Xóa quản trị viên thành công!');
                }
            });
        }
    });
});


$(function () {
    $(".delete_category").click(function () {
        var element = $(this);
        var id = element.attr("idC");
        var p = confirm("Bạn có chắc chắn muốn xóa danh mục này?");
        if (p == true) {
            $.ajax({
                type: "GET",
                url: "direct.php?controller=category&action=delete&id=" + id,
                success: function (html) {
                    jQuery("#id" + id).remove();
                    alert('Xóa Danh mục thành công!');
                }
            });
        }
    });
});

$(function () {
    $(".delete_post").click(function () {
        var element = $(this);
        var id = element.attr("idP");
//        alert(id); die;
        var p = confirm("Bạn có chắc chắn muốn xóa bài đăng này?");
        if (p == true) {
            $.ajax({
                type: "GET",
                url: "direct.php?controller=post&action=delete&id=" + id,
                success: function (html) {
                    jQuery("#id" + id).remove();
                    alert('Xóa bài đăng thành công!');
                }
            });
        }
    });
});

$(document).ready(function ($) {
    $("#city").change(function (event) {
        var id = $(this).val();
//        alert(id);die;
        if (id != '') {
            $.ajax({
                url: 'direct.php?controller=post&action=getprovince',
                type: 'GET',
                dataType: "json",
                data: {
                    'id': id
                },
                success: function (data) {
                    if (data != '') {
                        var select = '<select class="form-control" name="district">';
                        $.each(data, function (key, value) {
                            select += '<option value="' + value.id + '">' + value.name + '</option>';
                        });
                        select += '</select>';
                        $('#district_select').html(select);
                    }
                }
            });
        } else {
            alert('Vui lòng chọn thành phố!!!');
        }
    });
});

//---------------------- ----------- hiển thị hình ảnh --------------------------------
var fileCollection = new Array();
$('#file').on('change', function (e) {

    if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
    {

        var data = $(this)[0].files; //this file data
        var FileType;
        var count = 0;
        $.each(data, function (index, file) { //loop tnhough each file
            if (/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)) { //check supported file type
                var fRead = new FileReader(); //new filereader
                fRead.onload = (function (file) { //trigger function on successful read
                    return function (e) {
                        fileCollection.push(file);
                        count = fileCollection.length;
                        var html = '<div class="product-img">';
                        html += '<div class="del-item">\n\
                                <a title="Xóa ảnh này" class="rimg" data="' + (count - 1) + '">\n\
                                <img src="./library/images/delete.png" width="20" height="20" class="img-responsive" alt="Image">\n\
                                </a>\n\
                                </div>';
                        html += '<img src="' + e.target.result + '" class="thumb"/>'; //create image element 
                        html += '</div>';
                        var FileType = data[index].type;
                        // get file extension..
                        var fileExtension = FileType.substr((FileType.lastIndexOf('/') + 1));

                        var Extension = fileExtension.toUpperCase();

                        if (fileCollection.length == 1) {
                            $('.picture').empty();
                        }
                        $('.picture').append(html);
                    };
                })(file);
                fRead.readAsDataURL(file);
            }
        });
    } else {
        alert("Your browser doesn't support File API!"); //if File API is absent
    }
});

$(".picture").on("click", ".rimg", function () {
    var rimg = $(this).attr('data');
    fileCollection.splice(fileCollection.indexOf(fileCollection[rimg]), 1);
    $(this).parent().parent().remove();
    console.log(fileCollection);
    return false;
});

$('#submit').submit(function (e) {
    e.preventDefault();
    var form = $(this);
    var formData = new FormData(form[0]);
    console.log(fileCollection);
    $.each(fileCollection, function (i, obj) {
        formData.append('file[' + i + ']', obj);

    });
//    
    $.ajax({
        url: '/rongbay/admin/direct.php?controller=post&action=edit',
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
            console.log(data);
            if (data == 1) {
                alert('Chỉnh sửa thành công!!!');
                window.location.assign("http://localhost/rongbay/admin/index.php?controller=post&action=listed");
            } else {
                $('.msg-error').html(data);
                $('html, body').animate({
                    scrollTop: parseInt($(".msg-error").offset().top)
                }, 500);
            }
        }
    });

});


$('#addpost').submit(function (e) {
    e.preventDefault();
    var form = $(this);
    var formData = new FormData(form[0]);
    console.log(fileCollection);
    $.each(fileCollection, function (i, obj) {
        if (i < fileCollection.length) {
            formData.append('file[' + i + ']', obj);
        }

    });
    $.ajax({
        url: '/rongbay/admin/direct.php?controller=post&action=add',
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
            console.log(data);
            if (data == 1) {
//                console.log(data);
                alert('Thêm mới thành công!!!');
                window.location.assign("http://localhost/rongbay/admin/index.php?controller=post&action=listed");
            } else {
                $('.msg-error').html(data);
                $('html, body').animate({
                    scrollTop: parseInt($(".msg-error").offset().top)
                }, 500);
            }
        }
    });

});

//--------------------------------------------------------------------
//$('#upload').on('click', function () {
//        //Lấy ra files
//        var file_data = $('#file').prop('files')[0];
//        //lấy ra kiểu file
//        var type = file_data.type;
//        //Xét kiểu file được upload
//        var match = ["image/gif", "image/png", "image/jpg",];
//        //kiểm tra kiểu file
//        if (type == match[0] || type == match[1] || type == match[2]) {
//            //khởi tạo đối tượng form data
//            var form_data = new FormData();
//            //thêm files vào trong form data
//            form_data.append('file', file_data);
//            //sử dụng ajax post
//            $.ajax({
//                url: 'direct.php?controller=post&action=edit', // gửi đến file upload.php 
//                dataType: 'text',
//                cache: false,
//                contentType: false,
//                processData: false,
//                data: form_data,
//                type: 'post',
//                success: function (res) {
//                    $('.status').text(res);
//                    $('#file').val('');
//                }
//            });
//        } else {
//            $('.status').text('Chỉ được upload file ảnh');
//            $('#file').val('');
//        }
//        return false;
//    });