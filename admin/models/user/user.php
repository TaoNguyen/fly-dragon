<?php
class User{
	
    public $conn;
    public function __construct() {
        $conn = $GLOBALS['conn'];
    }
    
    //show ra all user
    public function getListUser() {
        $sql = "SELECT * FROM users AS u JOIN user_infos AS u_info ON u.id = u_info.user_id WHERE u.role = 0 ORDER BY u.id DESC";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    //lấy user đã trong DB để show ra (by ID) trong giai đoạn edit
    public function getUserbyId($userID){
        $sql = "SELECT * FROM users AS u JOIN user_infos AS u_info ON u.id = u_info.user_id WHERE u.id = $userID";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    //check email existed or not
    public function checkEmail($email){
        $sql = "SELECT * FROM users WHERE email = '$email'";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    // thêm mới một user tại bảng users
    public function addUser($email,$password) {
        $sql = "INSERT INTO users (email, password, role, facebook)
                    VALUES ('$email', '". md5($password)."', 0, 0)";
        $result = $GLOBALS['conn']->query($sql);
        if ($result) {
            $user_id = $GLOBALS['conn']->insert_id;
            return $user_id;
        } else {
            return false;
        }
    }
    
    // thêm mới một user tại bảng user_infos
    public function addUserInfo($name, $phone, $address, $gender, $user_id, $image) {
        if ($image != '') {
            $sql = "INSERT INTO user_infos (fullname, phone, address, gender, user_id, created_at, img)
                    VALUES ('$name', '$phone', '$address', '$gender', '$user_id', NOW(),'$image')";
            $result = $GLOBALS['conn']->query($sql);
        } else {
            $sql = "INSERT INTO user_infos (fullname, phone, address, gender, user_id, created_at)
                    VALUES ('$name', '$phone', '$address', '$gender', '$user_id', NOW())";
            $result = $GLOBALS['conn']->query($sql);
        }
        return $result;
    }
    
    //đăng nhập
    public function getLogin($username, $password){
        $result = "SELECT * FROM users WHERE role = 1 and email = '" . $username. "' and password = '".md5($password)."'";
//        $run_query = mysqli_query($GLOBALS['conn'], $result);
        $run_query = $GLOBALS['conn']->query($result);
        return $run_query;
    }
    
    //update một user đã có trongDB tại bảng users
    public function updateUser($userID, $password)
    {
        $sql = "UPDATE users SET password=  '". md5($password)."' WHERE id = $userID";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    //update một user đã có trongDB tại bảng user_infos
    public function updateInfo($userID, $name, $phone, $address, $gender, $image)
    {
        if ($image != '') {
            $sql = "UPDATE user_infos SET fullname ='$name', phone ='$phone',address ='$address', gender ='$gender',img ='$image', updated_at = NOW() WHERE user_id = $userID";
            $result = $GLOBALS['conn']->query($sql);
        } else {
            $sql = "UPDATE user_infos SET fullname = '$name', phone = '$phone',address ='$address', gender ='$gender', updated_at = NOW() WHERE user_id = $userID";
            $result = $GLOBALS['conn']->query($sql);
        }
        return $result;
    }

    //-----------------------------------------------------------------------------------------------
    
    //delete a user in DB
    public function deleteUser($id)
    {
        $sql = "DELETE FROM users WHERE id = $id";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    public function deleteInfo($id)
    {
        $sql = "DELETE FROM user_infos WHERE user_id = $id";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    //count all account in DB
    public function countUser(){
        $sql = " SELECT COUNT(*) AS total FROM users AS u JOIN user_infos AS u_info ON u.id = u_info.user_id WHERE u.role = 0";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    //get user limit
    public function getUserWithLimit($start, $limit){
        $sql = "SELECT * FROM users AS u JOIN user_infos u_info ON u.id = u_info.user_id WHERE u.role = 0 ORDER BY u.id DESC LIMIT $start, $limit";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
}
?>
