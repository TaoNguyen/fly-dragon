<?php
class Admin{
	
    public $conn;
    public function __construct() {
        $conn = $GLOBALS['conn'];
    }
    
    //get all admin's account in DB
    public function getListAdmin() {
        $sql = "SELECT * FROM users AS u JOIN user_infos AS u_info ON u.id = u_info.user_id WHERE u.role = 1 ORDER BY u.id DESC";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    //get Admin's account by id to show inf on sceen when admin edit
    public function getAdminbyId($adminID){
        $sql = "SELECT * FROM users AS u JOIN user_infos AS u_info ON u.id = u_info.user_id WHERE u.id = $adminID";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    //check email existed or not
    public function checkEmail($email){
        $sql = "SELECT * FROM users WHERE email = '$email'";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
	
	// thêm mới một user tại bảng users
    public function addAdmin($email,$password) {
        $sql = "INSERT INTO users (email, password, role, facebook)
                    VALUES ('$email', '". md5($password)."', 1, 0)";
        $result = $GLOBALS['conn']->query($sql);
        if ($result) {
            $user_id = $GLOBALS['conn']->insert_id;
            return $user_id;
        } else {
            return false;
        }
    }
	
	// thêm mới một user tại bảng user_infos
    public function addAdminInfo($name, $phone, $address, $gender, $user_id, $image) {
        if ($image != '') {
            $sql = "INSERT INTO user_infos (fullname, phone, address, gender, user_id, created_at, img)
                    VALUES ('$name', '$phone', '$address', '$gender', '$user_id', NOW(),'$image')";
            $result = $GLOBALS['conn']->query($sql);
        } else {
            $sql = "INSERT INTO user_infos (fullname, phone, address, gender, user_id, created_at)
                    VALUES ('$name', '$phone', '$address', '$gender', '$user_id', NOW())";
            $result = $GLOBALS['conn']->query($sql);
        }
        return $result;
    }
    
    //update một user đã có trongDB tại bảng users
    public function updateAdmin($userID, $password)
    {
        $sql = "UPDATE users SET password=  '". md5($password)."' WHERE id = $userID";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    //update một user đã có trongDB tại bảng user_infos
    public function updateInfo($userID, $name, $phone, $address, $gender, $image)
    {
        if($image != '') {
            $sql = "UPDATE user_infos SET fullname = '$name', phone = '$phone',address ='$address', gender ='$gender',img = '$image', updated_at = NOW() WHERE user_id = $userID";
            $result = $GLOBALS['conn']->query($sql);
        }
        $sql = "UPDATE user_infos SET fullname = '$name', phone = '$phone',address ='$address', gender ='$gender', updated_at = NOW() WHERE user_id = $userID";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    //del a account
    public function deleteAdmin($id){
        $sql = "DELETE FROM users WHERE id = $id";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    public function deleteInfo($id){
        $sql = "DELETE FROM user_infos WHERE user_id = $id";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
        //----------------------------------------------------------------------------------------------------------
    //count all account in DB
    public function countAdmin(){
        $sql = " SELECT COUNT(id) AS Total FROM users AS u JOIN user_infos AS u_info ON u.id = u_info.user_id WHERE u.role = 1";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
    
    //get user limit
    public function getAdminWithLimit($start, $limit){
        $sql = "SELECT * FROM users AS u JOIN user_infos AS u_info ON u.id = u_info.user_id WHERE u.role = 1 LIMIT $start, $limit";
        $result = $GLOBALS['conn']->query($sql);
        return $result;
    }
}
?>
