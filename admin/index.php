<?php
    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Admin Management</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <link rel="stylesheet" href="views/assets/vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="views/assets/vendor/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
        <link rel="stylesheet" href="views/assets/css/style.default.css" id="theme-stylesheet">
        <link rel="stylesheet" href="views/assets/css/custom.css">
    </head>
    
    <body>
        <?php
            include_once 'direct.php';
        ?>

        <script src="./views/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
        <script src="./views/assets/vendor/popper.js/umd/popper.min.js"></script>
        <script src="./views/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="views/assets/js/ajax.js"></script>
    </body>
</html>