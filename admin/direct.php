<?php
//define('SITE_ROOT', __DIR__);
include_once ('../define.php');
include_once ('library/connectdb.php');
include_once('library/_autoload.php');

if (isset($_GET['controller'])) {
    switch ($_GET['controller']) {

        case 'user': include_once('controllers/user/userController.php');
            break;
        case 'admin': include_once('controllers/admin/adminController.php');
            break;
        case 'category': include_once('controllers/category/categoryController.php');
            break;
        case 'post': include_once('controllers/post/postController.php');
            break;
    }
} else {
    header('location: index.php?controller=user&action=login');
}
?>