<?php
/**
* 
*/
class Post
{
	public $connect;
	function __construct()
	{
		$connect = $GLOBALS['connect'];
	}
	// Create post
	public function createPost($title, $acreage, $city_id, $district_id, $address, $price, $unit, $description, $lay, $balcony, $license, $category_id ){
		$sql = "INSERT INTO posts (title, acreage, city_id, district_id, address, price, unit, description, lay, balcony, license,category_id,
				 created_at, updated_at) 
				VALUES ('$title', $acreage, $city_id, $district_id, '$address', $price, '$unit', '$description', '$lay', '$balcony', '$license', $category_id, now(), now())";
		$result = $GLOBALS['connect'] ->query($sql);
		if ($result) {
			$post_id = $GLOBALS['connect']->insert_id;
			return $post_id;
		}else {
			return false;
		}
	}
	public function createPostContact($phone, $email, $fullname,$post_id,$user_id){
		$sql = "INSERT INTO post_contacts(phone, email, fullname, post_id, user_id)
				VALUES ('$phone', '$email', '$fullname',$post_id, $user_id)";
		$result = $GLOBALS['connect'] ->query($sql);
		return $result;
	}
	public function getCountPost()
	{
		$sql="SELECT count(*) as count FROM posts";
		$result = $GLOBALS['connect'] ->query($sql);
		$count=$result->fetch_assoc();
		return $count['count'];
	}
	public function getPostById($id) {
		$sql = "SELECT p.id AS post_id, p.title AS title, p.acreage AS acreage, p.address AS address, p.description AS description, p.price AS price, p.unit AS unit, p.lay AS lay, p.balcony AS balcony, p.license AS license,p.created_at AS created_at, c.category_name AS category_name, c.id AS category_id, p_contact.fullname AS fullname, p_contact.phone AS phone, p_contact.email AS email, i.image AS image FROM posts AS p
				LEFT JOIN categories AS c ON p.category_id = c.id 
				LEFT JOIN post_contacts AS p_contact ON p.id = p_contact.post_id 
				LEFT JOIN images AS i ON p.id = i.post_id 
				LEFT JOIN provinces AS pro ON p.city_id = pro.id
				WHERE p.id = $id
				GROUP BY i.post_id";
		$result = $GLOBALS['connect'] ->query($sql);
		return $result;
	}

	public function getPostByIdCategory($post_id, $category_id) {
		$sql = "SELECT p.id AS post_id, p.title AS title, p.acreage AS acreage, p.address AS address, p.description AS description, p.price AS price, p.unit AS unit, p.lay AS lay, p.balcony AS balcony, p.license AS license,p.created_at AS created_at, c.category_name AS category_name, p_contact.fullname AS fullname, p_contact.phone AS phone, p_contact.email AS email, i.image AS image FROM posts AS p
				LEFT JOIN categories AS c ON p.category_id = c.id 
				LEFT JOIN post_contacts AS p_contact ON p.id = p_contact.post_id 
				LEFT JOIN images AS i ON p.id = i.post_id 
				LEFT JOIN provinces AS pro ON p.city_id = pro.id
				WHERE p.category_id = $category_id AND p.id != $post_id
				GROUP BY i.post_id
				ORDER BY p.id DESC LIMIT 5";

		$result = $GLOBALS['connect'] ->query($sql);
		return $result;
	}
	// List ra tất cả các bài Post thuộc category đã chọn
	public function getPost($page='', $id) {
		if($page==''){
			$offset=0;
		}else{
			$offset=$page-1;
			$offset *= 5;
		}
		$sql = "SELECT p.id AS post_id, p.title AS title, p.acreage AS acreage, p.address AS address, p.price AS price, p.unit AS unit, p.lay AS lay, p.balcony AS balcony, p.license AS license,p.created_at AS created_at, p_contact.fullname AS fullname, i.image AS image FROM posts AS p
				JOIN categories AS c ON p.category_id = c.id 
				JOIN post_contacts AS p_contact ON p.id = p_contact.post_id 
				JOIN images AS i ON p.id = i.post_id 
				JOIN provinces AS pro ON p.city_id = pro.id 
				WHERE p.category_id = $id
				GROUP BY i.post_id
				ORDER BY p.id DESC LIMIT 5 OFFSET $offset ";
		$result = $GLOBALS['connect'] ->query($sql);
		return $result;
	}
	public function getCountAllPost($id)
	{
		$sql="SELECT count(*) as count FROM posts WHERE category_id= $id";
		$result = $GLOBALS['connect'] ->query($sql);
		$count=$result->fetch_assoc();
		return $count['count'];
	}
	// List ra tất cả các bài Post thuộc category avf tĩnh đã chọn theo 
	public function getPostProvince($page='', $id, $province) {
		if($page==''){
			$offset=0;
		}else{
			$offset=$page-1;
			$offset *= 5;
		}
		$sql = "SELECT p.id AS post_id, p.title AS title, p.acreage AS acreage, p.address AS address, p.price AS price, p.unit AS unit, p.lay AS lay, p.balcony AS balcony, p.license AS license,p.created_at AS created_at, p_contact.fullname AS fullname, i.image AS image FROM posts AS p
				JOIN categories AS c ON p.category_id = c.id 
				JOIN post_contacts AS p_contact ON p.id = p_contact.post_id 
				JOIN images AS i ON p.id = i.post_id 
				JOIN provinces AS pro ON p.city_id = pro.id 
				WHERE p.category_id = $id AND p.city_id = $province
				GROUP BY i.post_id
				ORDER BY p.id DESC LIMIT 5 OFFSET $offset ";
		$result = $GLOBALS['connect'] ->query($sql);
		return $result;
	}
	public function getCountAllPostProvince($id, $province)
	{
		$sql="SELECT count(*) as count FROM posts WHERE category_id = $id AND city_id = $province";
		$result = $GLOBALS['connect'] ->query($sql);
		$count=$result->fetch_assoc();
		return $count['count'];
	}
	// List ra tất cả các Category sắp xếp theo thứ tụ từ lớn đến nhỏ theo số lượng bài Post
	public function getCategoryByPost()
	{
		$sql = "SELECT p.title,p.category_id, COUNT(category_id) AS number, c.category_name AS name , c.img AS image FROM posts AS p
				LEFT JOIN categories AS c ON p.category_id = c.id
				GROUP BY category_id ORDER BY number DESC";
		$result = $GLOBALS['connect']->query($sql);
		return $result;
	}
	// List ra tất cả các Category sắp xếp theo thứ tụ từ lớn đến nhở theo số lượng bài Post tỉnh đã chọn
	public function getPostByProvince($id)
	{
		$sql = "SELECT p.title,p.category_id, COUNT(category_id) AS number, c.category_name AS name , c.img AS image FROM posts AS p
				LEFT JOIN categories AS c ON p.category_id = c.id
				WHERE p.city_id = $id
				GROUP BY category_id ORDER BY number DESC";
		$result = $GLOBALS['connect']->query($sql);
		return $result;
	}
	// List ra tất cả các bài Post thuộc parent đã chọn
	public function getPostByParent($page='', $id)
	{
		if($page==''){
			$offset=0;
		}else{
			$offset=$page-1;
			$offset *= 5;
		}
		$sql = "SELECT p.id AS post_id, p.title AS title, p.acreage AS acreage, p.address AS address, p.price AS price, p.unit AS unit, p.lay AS lay, p.balcony AS balcony, p.license AS license,p.created_at AS created_at, p_contact.fullname AS fullname, i.image AS image FROM posts AS p
				JOIN categories AS c ON p.category_id = c.id 
				JOIN post_contacts AS p_contact ON p.id = p_contact.post_id 
				JOIN images AS i ON p.id = i.post_id 
				JOIN provinces AS pro ON p.city_id = pro.id 
				WHERE c.parent = $id
				GROUP BY p.id
				ORDER BY p.id DESC LIMIT 5 OFFSET $offset ";
		$result = $GLOBALS['connect'] ->query($sql);
		return $result;
	}
	public function getCountAllPostByParent($id)
	{
		$sql="SELECT count(*) as count FROM posts AS p LEFT JOIN categories AS c ON p.category_id = c.id WHERE c.parent = $id";
		$result = $GLOBALS['connect'] ->query($sql);
		$count=$result->fetch_assoc();
		return $count['count'];
	}
	
	// List ra tất cả các bài Post thuộc parent và tỉnh đã chọn
	public function getPostByParentProvince($page='', $id, $province)
	{
		if($page==''){
			$offset=0;
		}else{
			$offset=$page-1;
			$offset *= 5;
		}
		$sql = "SELECT p.id AS post_id, p.title AS title, p.city_id AS city_id , p.acreage AS acreage, p.address AS address, p.price AS price, p.unit AS unit, p.lay AS lay, p.balcony AS balcony, p.license AS license,p.created_at AS created_at, p_contact.fullname AS fullname, i.image AS image FROM posts AS p
				JOIN categories AS c ON p.category_id = c.id 
				JOIN post_contacts AS p_contact ON p.id = p_contact.post_id 
				JOIN images AS i ON p.id = i.post_id 
				JOIN provinces AS pro ON p.city_id = pro.id 
				WHERE c.parent = $id AND pro.id = $province
				GROUP BY p.id
				ORDER BY p.id DESC LIMIT 5 OFFSET $offset";
		$result = $GLOBALS['connect'] ->query($sql);
		return $result;
	}
	public function getCountAllPostByParentProvince($id, $province)
	{
		$sql="SELECT count(*) as count FROM posts AS p LEFT JOIN categories AS c ON p.category_id = c.id WHERE c.parent = $id AND p.city_id = $province";
		$result = $GLOBALS['connect'] ->query($sql);
		$count=$result->fetch_assoc();
		return $count['count'];
	}
	// Chức năng tìm kiếm với parent_id
	public function SearchPost($page='', $id, $search_word)
	{
		if($page==''){
			$offset=0;
		}else{
			$offset=$page-1;
			$offset *= 5;
		}
		$sql = "SELECT p.id AS post_id, p.title AS title, p.acreage AS acreage, p.address AS address, p.price AS price, p.unit AS unit, p.lay AS lay, 
				p.balcony AS balcony, p.license AS license,p.created_at AS created_at, p_contact.fullname AS fullname, i.image AS image FROM posts AS p
				JOIN categories AS c ON p.category_id = c.id 
				JOIN post_contacts AS p_contact ON p.id = p_contact.post_id 
				JOIN images AS i ON p.id = i.post_id 
				JOIN provinces AS pro ON p.city_id = pro.id 
				WHERE c.parent = $id AND MATCH(p.title, p.description)
				AGAINST( '$search_word' IN NATURAL LANGUAGE MODE)
				GROUP BY p.id
				ORDER BY p.id DESC LIMIT 5 OFFSET $offset";
		$result = $GLOBALS['connect'] ->query($sql);
		return $result;
	}
	public function getCountAllPostSearch($id, $search_word)
	{
		$sql="SELECT count(*) as count FROM posts AS p LEFT JOIN categories AS c ON p.category_id = c.id WHERE c.parent = $id AND MATCH(p.title, p.description)
			AGAINST('$search_word' IN NATURAL LANGUAGE MODE)";
		$result = $GLOBALS['connect'] ->query($sql);
		$count=$result->fetch_assoc();
		return $count['count'];
	}

	// Chức năng tìm kiếm khi chọn tỉnh
	public function SearchPostProvince($page='', $id, $search_word, $province_id)
	{
		if($page==''){
			$offset=0;
		}else{
			$offset=$page-1;
			$offset *= 5;
		}
		$sql = "SELECT p.id AS post_id, p.title AS title, p.acreage AS acreage, p.address AS address, p.price AS price, p.unit AS unit, p.lay AS lay, p.balcony AS balcony, p.license AS license,p.created_at AS created_at, p_contact.fullname AS fullname, i.image AS image FROM posts AS p
				JOIN categories AS c ON p.category_id = c.id 
				JOIN post_contacts AS p_contact ON p.id = p_contact.post_id 
				JOIN images AS i ON p.id = i.post_id 
				JOIN provinces AS pro ON p.city_id = pro.id 
				WHERE c.parent = $id AND p.city_id = $province_id AND MATCH(p.title, p.description)
				AGAINST('$search_word' IN NATURAL LANGUAGE MODE)
				GROUP BY p.id
				ORDER BY p.id DESC LIMIT 5 OFFSET $offset";
		$result = $GLOBALS['connect'] ->query($sql);
		return $result;
	}
	public function getCountAllPostSearchProvince($province_id, $id, $search_word)
	{
		$sql="SELECT count(*) as count FROM posts AS p LEFT JOIN categories AS c ON p.category_id = c.id WHERE c.parent = $id AND p.city_id = $province_id AND MATCH(p.title, p.description)
				AGAINST('$search_word' IN NATURAL LANGUAGE MODE)";
		$result = $GLOBALS['connect'] ->query($sql);
		$count=$result->fetch_assoc();
		return $count['count'];
	}
	// Tìm kiếm khi chỉ có key word người dùng nhập vào
	public function SearchKey($page='', $search_word)
	{
		if($page==''){
			$offset=0;
		}else{
			$offset=$page-1;
			$offset *= 5;
		}
		$sql = "SELECT p.id AS post_id, p.title AS title, p.acreage AS acreage, p.address AS address, p.price AS price, p.unit AS unit, p.lay AS lay, 
				p.balcony AS balcony, p.license AS license,p.created_at AS created_at, p_contact.fullname AS fullname, i.image AS image FROM posts AS p
				JOIN categories AS c ON p.category_id = c.id 
				JOIN post_contacts AS p_contact ON p.id = p_contact.post_id 
				JOIN images AS i ON p.id = i.post_id 
				JOIN provinces AS pro ON p.city_id = pro.id 
				WHERE MATCH(p.title, p.description)
				AGAINST( '$search_word' IN NATURAL LANGUAGE MODE)
				GROUP BY p.id
				ORDER BY p.id DESC LIMIT 5 OFFSET $offset";
		$result = $GLOBALS['connect'] ->query($sql);
		return $result;
	}
	public function getCountSearchKey($search_word)
	{
		$sql="SELECT count(*) as count FROM posts AS p LEFT JOIN categories AS c ON p.category_id = c.id WHERE MATCH(p.title, p.description)
			AGAINST('$search_word' IN NATURAL LANGUAGE MODE)";
		$result = $GLOBALS['connect'] ->query($sql);
		$count=$result->fetch_assoc();
		return $count['count'];
	}
	// Tìm kiếm khi chỉ có key word người dùng nhập vào và chọn tỉnh
	public function SearchKeyProvince($page='', $search_word, $province_id)
	{
		if($page==''){
			$offset=0;
		}else{
			$offset=$page-1;
			$offset *= 5;
		}
		$sql = "SELECT p.id AS post_id, p.title AS title, p.acreage AS acreage, p.address AS address, p.price AS price, p.unit AS unit, p.lay AS lay, p.balcony AS balcony, p.license AS license,p.created_at AS created_at, p_contact.fullname AS fullname, i.image AS image FROM posts AS p
				JOIN categories AS c ON p.category_id = c.id 
				JOIN post_contacts AS p_contact ON p.id = p_contact.post_id 
				JOIN images AS i ON p.id = i.post_id 
				JOIN provinces AS pro ON p.city_id = pro.id 
				WHERE p.city_id = $province_id AND MATCH(p.title, p.description)
				AGAINST('$search_word' IN NATURAL LANGUAGE MODE)
				GROUP BY p.id
				ORDER BY p.id DESC LIMIT 5 OFFSET $offset";
		$result = $GLOBALS['connect'] ->query($sql);
		return $result;
	}
	public function getCountSearchKeyProvince($search_word, $province_id)
	{
		$sql="SELECT count(*) as count FROM posts AS p LEFT JOIN categories AS c ON p.category_id = c.id WHERE p.city_id = $province_id AND MATCH(p.title, p.description)
				AGAINST('$search_word' IN NATURAL LANGUAGE MODE)";
		$result = $GLOBALS['connect'] ->query($sql);
		$count=$result->fetch_assoc();
		return $count['count'];
	}

}
?>