 $('.nodal-view-box a.modal-action').click(function () {
    return false;
});
 $('.loginsite').click(function () {
    $('ul.nav-tabs li:first a').addClass('active');
    $('div.indicator').css('right', '196px').css('left', '0px');
    $('ul.nav-tabs li:not(:first) a').removeClass('active');
    $('#test-swipe-2').removeClass('in').removeClass('active').removeClass('show');
    $('#test-swipe-1').addClass('in').addClass('active').addClass('show');
});
 $('.registersite').click(function () {
    $('ul.nav-tabs li:first a').removeClass('active');
    $('ul.nav-tabs li:not(:first) a').addClass('active');
    $('div.indicator').css('right', '75px').css('left', '141px');
   $('#test-swipe-1').removeClass('in').removeClass('active').removeClass('show');
    $('#test-swipe-2').addClass('in').addClass('active').addClass('show');
});
 $('#register-form').validate({
   errorClass: 'error',
   validClass: "valid_email",
   rules: {
    email: {
        required: true,
        email: true,
        remote: {
            url: "customers/direct.php?controller=customer&action=check_register",
            type: "get",
            data: {
              email: function() {
                return $( "#email" ).val();
            },
            complete:function(data){
                $('#email-eror').addClass('error').removeClass('valid_email');
            }
        }
    }

},
password: {
    required: true,
                    //                    minlenght: 8,
                },
                confirm_pass: {
                    required: true,
                    equalTo: '#password',
                },
                fullname: {
                    required: true,
                },
                phone: {
                    required: true,
                    rangelength: [10, 11],
                    number: true,
                },
                address: {
                    required: true,
                }
            },
            messages: {
                email: {
                    required: 'Vui lòng nhập Email.',
                    email: 'Định dạng email không đúng.',
                    remote: 'Email đã tồn tại',
                },
                password: {
                    required: 'Vui lòng nhập Mật khẩu',
                    //                    minlenght: 'Mật khẩu phải nhiều hơn 8 ký tự',
                },
                confirm_pass: {
                    required: 'Vui lòng xác nhận mật khẩu',
                    equalTo: 'Xác nhận mật khẩu không trùng khớp',
                },
                fullname: {
                    required: 'Vui lòng nhập Họ và tên',
                },
                phone: {
                    required: 'Vui lòng nhập Số điện thoại',
                    rangelength: 'Độ dài số điện thoại không chính xác',
                    number: 'Định dạng số điện thoại không chính xác',
                },
                address: {
                    required: 'Vui lòng nhập địa chỉ',
                }
            },
          success: function(label,element) {
            if($(element).attr('id')=='email'){
                label.addClass('valid_email').text("Email có thể sử dụng");
            }
        },
        submitHandler: function (form) {
            form.submit();
        }
    });

 