
$(document).ready(function(){
	$('input.form-control').focus(function(){
		$(this).parent().find('label').css('top','-25px');
	});
	$('input.form-control').blur(function(){
		if($(this).val()==''){
		$(this).parent().find('label').css('top','5px');
	}
	});
	$('.provinces').change(function(){
		var id = $(this).val(); 
		if(id!=''){
			$.ajax({
				url: 'direct.php?controller=posts&action=post',
				type: 'POST',
				dataType: "json",
				data: {
					'id':id
				},
				success: function (data) {
					if(data!=''){
						var select='<select class="form-control" name="district">';
						$.each(data,function(key,value){
							select+='<option value="'+value.id+'">'+value.name+'</option>';
						});
						select+='</select>';
						$('#district_select').html(select);
					}

				}
			});
		}else{
			alert('Vui lòng chọn thành phố!!!');
		}
	});
	
	$('.category').change(function(){
		var id = $(this).val();
		if(id!=''){
			$.ajax({
				url: 'direct.php?controller=posts&action=post',
				type: 'POST',
				dataType: "json",
				data: {
					'cate_id':id
				},
				success: function (data) {
					console.log(data);
					JSON.stringify(data);
					if(data == "true"){
						$(".add-info").hide();
                		$(".list-check-input").hide();
                		$("#acreage").hide();
					} else {
						$(".add-info").show();
                		$(".list-check-input").show();
                		$("#acreage").show();
					}  
            	}
			});
		}
	});
	//hiển thị category theo thứ tự số bài post giảm dần khi chọn TỈNH
	$('.province_header').click(function(){
		var id = $(this).attr('id');
		var name = $(this).attr('name');
		if(id!='' || name !=''){
			$.ajax({
				url: 'direct.php?controller=posts&action=ajax',
				type: 'POST',
				dataType: "json",
				data: {
					'id':id,
					'province' : name,
				},
				success: function (data) {
					if(data!=''){
						var show_product='';
						$.each(data,function(key,value){
							show_product+='<div class="col-md-6 col-lg-4 show-product1">'
										+'<div class="image-thumb">'
										+'<img src="'+ value.image +'" class="img-responsive" alt="">'	
										+'<div class="news">'
										+'<a href="index.php?controller=posts&action=listpost&id='+ value.category_id+ '" title="">'+ value.name +'</a>'
										+'<span>'+ value.number +'</span></div></div></div>';
							$('.list_product').html(show_product);
						});
					}else {
						alert('KHông có bài viết');
					}
					$('#title').html('');
					var name_province = '<a href="#" class="city"  title="danang" id="title">'+name+'</a>';
					$('#title').html(name_province);		
            	}
			});
		}
	});
	// Hiển thị category được chọn
	$('.choose_category').click(function(){
		var parent_id = $(this).attr('category_id');
		var parent_name = $(this).attr('parent_name');
		$('#parent').html('');
		var parent = parent_name;
		$('#parent').html(parent_name);
		$('input[name=parent]').val(parent_id);

	});
	// tìm kiếm bài post theo parent và kết quả người dùng nhập vào
	// $('.search_post').click(function(){
	// 	var parent_id = $('.parent').attr('pa_id');
	// 	var search = $('.search_word').val();
	// 	if (parent_id != '' && search != '') {
	// 		$.ajax ({
	// 			url: 'direct.php?controller=posts&action=ajax',
	// 			type: 'POST',
	// 			dataType: "json",
	// 			data: {
	// 				'id':parent_id,
	// 				'search' : search,
	// 			},
	// 			success: function (data) {
	// 				console.log(data);
	// 				if(data!=''){
	// 					var show_search='';
	// 					$.each(data,function(key,value){
	// 						show_search+='<div class="row">'
	// 									+'<div class="col-4 col-sm-5 col-md-3 col-lg-3">'
	// 									+'<div class="img_post">'
	// 									+'<img src="'+value.image+'" alt=""></div></div>'
	// 									+'<div class="col-8 col-sm-7 col-md-6 col-lg-6">'	
	// 									+'<div class="title_post">'
	// 									+'<a href="index.php?controller=posts&action=detailpost&id='+value.post_id+'" title="">'+value.title+'</a></div>'
	// 									+'<div class="info-post">'
	// 									+'<span><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;<b>Dự án,</b>'+value.address+'</span><br>'
	// 									+'<span>Giá: <strong>'+value.price+' ('+value.unit+')</strong></span><span>- Diện tích:'+value.acreage+' m2</span><br>'
	// 									+'<span>Hướng: '+value.lay+', '+value.balcony+', '+value.license+'</span><br>'
	// 									+'<span>Post date: <b>'+value.created_at+'</b></span></div></div>'
	// 									+'<div class="col-md-3 col-lg-3">'
	// 									+'<div class="avatar text-center">'
	// 									+'<img src="library/images/interface/no_avatar.jpg" alt=""><br>'
	// 									+'<a href="#" title="">'+value.fullname+'</a></div></div></div>'
	// 									+'<hr>';
	// 						$('.show-product').html(show_search);
	// 					});
	// 				}else {
	// 					alert('KHông có bài viết');
	// 				}		
 //            	}
	// 		});
	// 	}
	// });
						
	//Display image before upload
	$('#file-input').on('change', function(){ //on file input change
        if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
        {
            $('#thumb-output').html(''); //clear html of output element
            var data = $(this)[0].files; //this file data
            
            $.each(data, function(index, file){ //loop though each file
                if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){ //check supported file type
                    var fRead = new FileReader(); //new filereader
                    fRead.onload = (function(file){ //trigger function on successful read
                    return function(e) {
                        var img = $('<img/>').addClass('thumb').attr('src', e.target.result);//create image element';
                        $('#thumb-output').append(img); //append image to output element
                    };
                    })(file);
                    fRead.readAsDataURL(file); //URL representing the file's data.
                }
            });
            
        }else{
            alert("Your browser doesn't support File API!"); //if File API is absent
        }
    });


	$('.news-top').click(function(){
		if ($(window).width() < 993) {
			$('.menu-top-child').toggle();
		}
	});
	$('.help').click(function(){
		if ($(window).width() < 993) {
			$('.menu-top-child-1').toggle();
			$('.child-top').show();
		}
	});
	$('.menu1').click(function(){
		if ($(window).width() < 993) {
			$('.menu-left-child').toggle();
			$('.child-child').show();
		}
	});
	$('.menu2').click(function(){
		if ($(window).width() < 993) {
			$('.menu-left-child1').toggle();
			$('.child-child1').show();
		}
	});
});

$('.carousel').carousel('next');