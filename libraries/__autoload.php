<?php
function __autoload($file_name){
	$uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
	$uri_segments = explode('/', $uri_path);
	if(isset($uri_segments)){
		if($uri_segments[2]=='customers'){
			$controller=$_GET['controller'];
			include_once('../customers/models/'.$controller.'/'.$file_name.'.php');
		}elseif ($uri_segments[2]=='admin') {
			include_once('../admin/models/'.$file_name.'.php');
		}else{
			include_once('models/'.$file_name.'.php');
		}
	}else{
		include_once('models/'.$file_name.'.php');
	}
}
?>