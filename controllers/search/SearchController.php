<?php
if (isset($_GET['page'])) {
	$page = $_GET['page'];
} else {
	$page=1;
}
$search = new Post();
$count = $search -> getCountPost();
if (isset($_GET['keysearch'])){
	$parent_id = $_GET['parent'];
	$keysearch = $_GET['keysearch'];
	
	if (empty($keysearch) && !empty($parent_id)) {
		if (isset($_SESSION['province_id'])){
			$province_id = $_SESSION['province_id'];
			$count_all_post = $search->getCountAllPostByParentProvince($parent_id, $province_id);
			$result = $search->getPostByParentProvince($page, $parent_id, $province_id);
		} else {
			$count_all_post=$search->getCountAllPostByParent ($parent_id);
			$result = $search->getPostByParent($page,$parent_id);
		}
	} 
	if (!empty($keysearch) && empty($parent_id)) {
		if (isset($_SESSION['province_id'])){
			$province_id = $_SESSION['province_id'];
			$count_all_post = $search->getCountSearchKeyProvince($keysearch, $province_id);
			$result = $search->SearchKeyProvince($page, $keysearch, $province_id);
		} else {
			$count_all_post=$search->getCountSearchKey ($keysearch);
			$result = $search->SearchKey($page,$keysearch);
		}
	}
	if (!empty($keysearch) && !empty($parent_id)) {
		if (isset($_SESSION['province_id'])){
			$province_id = $_SESSION['province_id'];
			$count_all_post=$search->getCountAllPostSearchProvince($province_id, $parent_id, $keysearch);
			$result = $search ->SearchPostProvince($page, $parent_id, $keysearch, $province_id);
		} else {
			$count_all_post=$search->getCountAllPostSearch($parent_id, $keysearch);
			$result = $search ->SearchPost($page, $parent_id, $keysearch);
		}
	}
	if (empty($keysearch) && empty($parent_id)) {
		header('location:index.php?controller=home');
	}
	include_once('views/home/search.php');
}
?>