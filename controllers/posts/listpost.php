<?php
if (isset($_SESSION['post'])) {
	echo "<script> alert('Chúc mừng bạn đã đăng bài thành công');</script>";
	unset($_SESSION['post']);
	unset($_SESSION['province_id']);
}
if (isset($_GET['parent'])) {
	$id = $_GET['parent'];
}
if (isset($_GET['id'])){
	$id = $_GET['id'];
}

if (isset($_GET['page'])) {
	$page = $_GET['page'];
} else {
	$page=1;
}
$post = new Post();
$count = $post -> getCountPost();
if (isset($_SESSION['province_id'])){
	$province = $_SESSION['province_id'];
	if (isset($_GET['parent'])) {
		$count_all_post=$post->getCountAllPostByParentProvince ($id, $province);
		$result = $post->getPostByParentProvince($page, $id, $province);
	}
	if (isset($_GET['id'])) {
		$count_all_post=$post->getCountAllPostProvince($id, $province);
		$result = $post->getPostProvince($page, $id, $province);
	}
} else {
	if (isset($_GET['parent'])) {
		$count_all_post=$post->getCountAllPostByParent ($id);
		$result = $post->getPostByParent($page,$id);
	}
	if (isset($_GET['id'])) {
		$count_all_post=$post->getCountAllPost($id);
		$result = $post->getPost($page,$id);
	}
}
include_once('views/posts/listpost.php');
?>