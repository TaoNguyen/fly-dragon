<?php
if (isset($_POST['id'])){
    $id = $_POST['id'];
    $district = new District();
    $result = $district->getDistrict($id);
    $arr = array();
    while($row_district=$result->fetch_array()){
        $arr[]=$row_district;
    }
    echo json_encode($arr);die();
}
//xử lí khi bấm chọn category thì hiển thị phần lay,bacony,... hoặc không
if(isset($_POST['cate_id'])){
    $id = $_POST['cate_id'];
    $cate = new Category();
    $result = $cate -> getParent($id);
    $row = $result ->fetch_assoc();
    if($row['parent'] != 1 && $row['parent'] != 2 && $row['parent'] != 5){
        $return = "true";
    }else {
        $return = "false";
    }
    echo json_encode($return);die();
}
$post = new Post();
$province = new Province();
$results = $province->getProvinces();
$cate = new Category();
$categories = $cate->getCategories();
$title_error = $acreage_error = $provice_error = $district_error = $address_error = $price_error = $image_error =
$unit_error = $description_error = $category_error = $name_error = $phonenumber_error = $email_error = $image_blank ="";
$title = $acreage = $province_id = $district_id = $address = $price = $unit = $description =
$category_id = $name = $phonenumber = $email = "";
$hide = false;
if (isset($_POST['sb_post'])){
	// validation title
	$input_title = trim($_POST ['title']);
    if (empty($input_title)) {
        $title_error = "Vui lòng nhập tiêu đề bài viết.";
    } else {
        $title = $input_title;
    }
	// select province
	$input_province_id = $_POST['province'];
    if (empty($input_province_id)) {
        $provice_error = "Vui lòng chọn tỉnh/ thành phố.";
    } else {
		$province_id = $input_province_id;
    }
    if ($province_id != ""){
        $dis = new District();
        $district = $dis->getDistrict($province_id);
    }
	// validation district
	$input_district_id = $_POST['district'];
    if (empty($input_district_id)) {
        $district_error = "Vui lòng chọn quận/ huyện.";
    } else {
        $district_id = $input_district_id;
    }
    // validation address
	$input_address = trim($_POST['address']);
    if (empty($input_address)) {
        $address_error = "Vui lòng nhập địa chỉ.";
    } else {
        $address = $input_address;
    }
    // validation price
    $input_price = $_POST['price'];
    if (empty($input_price)) {
        $price_error = "Vui lòng nhập giá.";
    } elseif (!preg_match("/^([0-9' ]+)$/",$input_price)) {
        $price_error = "Vui lòng nhập giá với giá trị là số có kí tự 0->9";
    } else {
        $price = $input_price;
    }
	// validation unit
	$input_unit = $_POST['unit'];
	if (empty($input_unit)) {
        $unit_error = "Vui lòng chọn đơn vị.";
    } else {
        $unit = $input_unit;
    }
    // validation description
    $input_description = strip_tags($_POST['description']);
	if (empty($input_description)) {
        $description_error = "Vui lòng nhập mô tả.";
    } else {
        $description = $input_description;
    }
	// validation balcony
    $balcony = $_POST['balcony'];
    $lay = $_POST['lay'];
    $license = $_POST['license'];
    // validation category (chuyên mục sản phẩm)
    $input_category = $_POST['category'];
	if (empty($input_category)) {
        $category_error = "Vui lòng chọn chuyên mục của sản phẩm";
    } else {
        $category_id = $input_category;
    }	
	//create post contact
    // validation fullname 
    $input_name = $_POST['name'];
    if (empty($input_name)) {
        $name_error = "Vui lòng nhập họ tên của bạn";
    } elseif (!preg_match("/^([a-zA-Z' ]+)$/",$input_name)) {
        $name_error = "Vui lòng nhập đúng tên của bạn";
    } else {
        $name = $input_name;
    }  
    // validation phonenumber
    $input_phonenumber = $_POST['phonenumber'];
	if (empty($input_phonenumber)) {
        $phonenumber_error = "Vui lòng nhập số điện thoại của bạn";
    } elseif (strlen($input_phonenumber)  > 14 || strlen($input_phonenumber) < 10) {
        $phonenumber_error = "Vui lòng nhập đúng số điện thoại của bạn";
    } elseif (!preg_match("/^([0-9' ]+)$/",$input_phonenumber)) {
        $phonenumber_error = "Vui lòng nhập đúng số điện thoại của bạn";
        
    } else {
        $phonenumber = $input_phonenumber;
    }
    $input_email = $_POST['email'];;
	$email = $_POST['email'];
    if (empty($input_email)) {
        $email_error = "Vui lòng nhập địa chỉ email của bạn";
    } elseif (!preg_match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^',$input_email)){
        $email_error = "Vui lòng nhập đúng định dạng email của bạn";
    } else {
        $email = $input_email;
    }
    if (isset($_SESSION['u_info'])){
        $user = $_SESSION['u_info'];
        $user_id = $user['id'];
    } else {
        $user_id = 0;
    }
    $cate = new Category();
    $parent = $cate -> getParent($category_id);
    $par = $parent ->fetch_assoc();
    if($par['parent'] != 1 && $par['parent'] != 2 && $par['parent'] != 5){
        $lay = "" ;
        $balcony = "" ;
        $license = "" ;
        $acreage = 0 ;
        $hide = true;
    } else {
        $input_acreage = trim($_POST['acreage']);
        if (empty($input_acreage)) {
            $acreage_error = "Vui lòng nhập diện tích.";
        } elseif (!preg_match("/^([0-9' ]+)$/",$input_acreage)) {
            $price_error = "Vui lòng nhập diện tích với giá trị là số có kí tự 0->9";
        } else {
            $acreage = $input_acreage;
        }
    }
    $arrayfile = $_FILES["photo"];
    for ($i=0; $i < count($arrayfile['name']) ; $i++){
        if ($arrayfile['name'][$i] == '') {
            $image_blank= 'Vui lòng upload hình ảnh';  
        }
    }

    if ($title != "" && $province_id != "" && $district_id != "" && $address != "" && $price != "" && 
        $unit != "" && $description != "" &&$category_id != "" && $name != "" && $phonenumber != "" && $email != "" && $image_blank == ""){
        $post_id = $post->createPost($title, $acreage, $province_id, $district_id, $address, $price, $unit, $description, $lay, $balcony, $license, $category_id);
        $post_contact = $post->createPostContact($phonenumber, $email, $name, $post_id, $user_id);
        if (!is_dir("./libraries/images/posts"))
            mkdir("./libraries/images/posts", 0777, true);
        if(isset($_FILES["photo"])){
            $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
            $sub_folder = '/' .  date('Y') . '/' . date('m') . '/' . date('d') ;
            $arrayfile = $_FILES["photo"];
            for ($i=0; $i < count($arrayfile['name']) ; $i++) { 
                $ext = pathinfo($arrayfile['name'][$i], PATHINFO_EXTENSION);
                if(!array_key_exists($ext, $allowed)) {
                    $image_error = "Vui lòng chọn đúng định dạng file";
                }
                if($arrayfile['size'][$i] > 5242880) {
                    $image_error = "Kích thước file lớn hơn giới hạn cho phép";
                }
                if(in_array($arrayfile['type'][$i], $allowed)){
                    
                    if (!is_dir("./libraries/images/posts" . $sub_folder))
                        if (!mkdir("./libraries/images/posts" . $sub_folder, 0777, true))
                            return FALSE;
                        $upload_url = SITE_ROOT.'/libraries/images/posts' . $sub_folder;
                        $img = $upload_url.'/'. $arrayfile['name'][$i];
                        $image = 'libraries/images/posts' . $sub_folder .'/'. $arrayfile['name'][$i];
                        move_uploaded_file($arrayfile['tmp_name'][$i], $img);
                        $image1 = new Image();
                        $photo = $image1->createPhoto($image,$post_id);    
                    } else{
                        $image_error = "Lỗi : Có vấn đề xảy ra khi upload file"; 
                    }
                }
            $_SESSION['post'] = "success";
            header("location:index.php?controller=posts&action=listpost&id=".$category_id);
        }
    }
}
include_once('views/posts/post.php');
?>