<?php
if (isset($_GET['id'])) {
	$id = $_GET['id'];
	$post = new Post();
	$result = $post->getPostById($id);
	$row = $result->fetch_assoc();
	$post_id = $row['post_id'];
	$image = new Image();
	$images = $image->getPhoto($post_id);
	$category_id = $row['category_id'];
	$categories = $post->getPostByIdCategory($id, $category_id);
}
include_once('views/posts/detailpost.php');
?>